window.onload = function(e) {

    var $countdown = $('#countdown');
    if ($countdown.length) {
    
        var eventTime = moment($countdown.data("countdownto"), 'YYYY-MM-DD HH:mm').unix(),
            currentTime = moment().unix(),
            diffTime = eventTime - currentTime,
            duration = moment.duration(diffTime * 1000, 'milliseconds'),
            interval = 1000;

        if (diffTime > 0) {

            var $d = $countdown.find(".days"), $h = $countdown.find(".hours"),
            $m = $countdown.find(".minutes"), $s = $countdown.find(".seconds");

            setInterval(function() {

                duration = moment.duration(duration.asMilliseconds() - interval, 'milliseconds');
                
                var d = Math.floor(moment.duration(duration).asDays()),
                h = moment.duration(duration).hours(),
                m = moment.duration(duration).minutes(),
                s = moment.duration(duration).seconds();

                d = $.trim(d).length === 1 ? '0' + d : d;
                h = $.trim(h).length === 1 ? '0' + h : h;
                m = $.trim(m).length === 1 ? '0' + m : m;
                s = $.trim(s).length === 1 ? '0' + s : s;

                $d.text(d); $h.text(h); $m.text(m); $s.text(s);

            }, interval);
        }
    }
}