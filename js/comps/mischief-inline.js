function MischiefInline($) {

    var elem = $('#mischieflink');
    var canRun = elem.length;
    if (!canRun) {
        return false;
    }

    var qs_config = {
        debug: $('#mischieflink').data('always') || 'false'
    };

    var section = $('.play-hub #copyInner .sfContentBlock').first();

    var config = {
        counter: 0,
        gapInterval: $('#mischieflink').data('gapInterval') || 2,
        link: $('#mischieflink').data('link') || '/'
    }

    if (section.length < 1) {
        return false;
    }

    var mischiefWidget;
    var overlay;
    var mischiefWidgetHolder;
    var timeouts = [];

    init();

    function init() {
        if (storageAvailable('localStorage')) {
            run();
        }
    }

    function run() {

        var lastVisit = getLocalStorage('mis_run_date');
        if (lastVisit) {
            var diff = new Date().getTime() - new Date(lastVisit).getTime();

            var seconds = (qs_config.debug === true) ? 100 : config.gapInterval * 86400;
            if (diff > seconds) {
                localStorage.removeItem('mis_run');
                localStorage.removeItem('mis_run_date');
            }
        }

        if (!getLocalStorage('mis_run')) {
            removeTimeouts();
            setHasRun();
            var markup = buildTemplate();
            mischiefWidget = $(markup);
            mischiefWidgetHolder = mischiefWidget.find('.mischief-widget-holder');
            mischiefWidgetHolder.css({
                background: 'url(/RSC/img/mischief-background_crop.png) no-repeat 50% 50%',
                height: '210px',
                width: '210px',
                position: 'relative',
                display: 'block',
                float: 'right'
            });

            overlay = mischiefWidget.find('.overlay');
            overlay.css({
                background: 'url(/RSC/img/mischief-overlay_crop.png) no-repeat 50% 50%',
                height: '100%',
                width: '100%',
                position: 'absolute',
                display: 'block',
                top: 0,
                left: 0
            });
            section.prepend(mischiefWidget);
            startAnimation();
        }
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function startAnimation() {
        var flickerTime = 50;
        var timer = 0;
        var intervalId = setInterval(function() {

            var valMin = 1;
            var valMax = 100;
            var pauseMin = 300;
            var pauseMax = 400;

            var val = getRandomInt(parseInt(valMin, 10), parseInt(valMax, 10));
            var pause = getRandomInt(parseInt(pauseMin, 10), parseInt(pauseMax, 10));
            if (val < 25) {
                overlay.css({ 'display': 'block' });
                setTimeout(function() {
                    overlay.css({ 'display': 'none' });
                }, pause)
            }

        }, flickerTime);
    }


    function buildTemplate() {
        return [
            '<div class="mischief-widget mischief-widget--inline">',
            '<a href="' + config.link + '">',
            '<div class="mischief-widget-holder">',
            '<div class="mischief-widget__inner">',
            '<div class="overlay"></div>',
            '</div>',
            '</div>',
            '</a>',
            '</div>'
        ].join('');
    }

    function storageAvailable(type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        } catch (e) {
            return false;
        }
    }

    function getLocalStorage(key) {
        var data = localStorage.getItem(key);
        if (data) {
            return data;
        }
        return false;
    }

    function setLocalStorage(key, value) {
        localStorage.setItem(key, value);
    }

    function removeTimeouts() {
        timeouts.forEach(function(timeout) {
            window.clearTimeout(timeout);
        })
        timeouts = [];
    }

    function setHasRun() {
        var today = new Date();
        setLocalStorage('mis_run', 1);
        setLocalStorage('mis_run_date', today.toISOString());
    }

    return {
        init: init
    }


};

var jQuery = jQuery || undefined;

/*
if (jQuery !== undefined ){
  var mcp = new MischiefInline(jQuery);
}
*/