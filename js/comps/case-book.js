$(document).ready(function(){

  var caseFilePage = $('article.case-book');
  if (caseFilePage.length > 0) {
    addEventHandlers();
  }

  function addEventHandlers() {

    $('.js-magnific').on('click', function(e){
      e.preventDefault();
      var content = $(this).closest('.js-magnific-content').html();
      $.magnificPopup.open({
        mainClass : 'mfp-wrap mfp-wrap-case',
        items: {
          src: '<div class="case-book-popout-panel">' + content + '</div>', // can be a HTML string, jQuery object, or CSS selector
          type: 'inline'
        }
      });
    });

    $('.js-magnific-video').on('click', function(e){
      e.preventDefault();
      var title = $(this).data('title');
      var src = $(this).attr('href');

      $.magnificPopup.open({
        type: 'iframe',
        items: {
          src: src
        },
        iframe: {
          markup: '<div class="mfp-iframe-scaler">'+
          '<h1 class="popup-title">' + title + '</h1>' +
          '<div class="mfp-close"></div>'+
          '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
          '</div>',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        }
      });
    })
  }

  // Added by Luke Jones - Remove class from casebook if the gallery is used and is within the casebook template
  if ($(".case-book .media-gallery .dk-gray").length) {
    $("div.gallery-launcher").removeClass("dk-gray");
  }

});
