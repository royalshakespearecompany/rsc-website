var theBody = $("body"),
    thePage = $("body, html"),
    mediaHolder = $("#mediaHolder"),
    largeVideoHolder = $(".large-video-holder"),
    largeIframe = $(".large-video-holder iframe"),
    smlVideoHolder = $(".small-video-holder"),
    smlIframe = $(".small-video-holder iframe"),
    homeVideoHolder = $("#homeVideoHolder"),
    homeVidIframe = $("#homeVideoHolder iframe"),
    homeVideoCover = $("#videoCover img"),
    quickLinksBtn = $(".ql-head"),
    quicklinksCont = $(".ql-body"),
    smallScreen = 480,
    smadiumScreen = 600,
    mediumScreen = 790,
    largeScreen = 1024,
    xlargeScreen = 1440,
    screenWidth = window.innerWidth,
    inCinemas = $("#inCinemasHolder"),
    showHidden = $(".show-hidden"),
    conditForms = [];


    // playhub variables
var introCopy = $("#playIntroCopy p"),
    prBooking = $("#prBooking"),
    copyInner = $("#copyInner"),
    moveIntro,
    movePRBooking,
    quoteHolder = $(".quote-holder:last-of-type"),
    floatNavOn = $("#floatNavOnBtn"),
    floatNavOff = $("#floatNavOffBtn"),
    floatNav = $("#floatNavList"),
    floatSubNav = $("#floatingMenu .sub-nav"),
    nextPerf = $("#nextPerf"),
    videoCover = $("#videoCover"),
    playVideo = $("#playVideo"),
    mainImage = $(".play-hub .main-image"),
    sponLogo = $("#spHolder"),
    moveSponsor,
    plFullList = $(".performance-list-item"),
    plChecks = $(".pl-filter .k-checkbox"),
    plActiveFilters = {
        where: ["stratford", "london"],
        time: ["matinee", "evening"],
        when: []
    },
    plFilterGroups = $(".pl-filter:not(.pl-calendar):not(.pl-promocode)"),
    perfList = $("#perfList"),
    plWarning = $("#pl-warning"),
    plLoader = $(".loader-outer"),
    apHeaders = $(".pl-list h2");



    // whatson variables
var theWOHeader = $(".whatson header"),
    theWOHeaderOrigHeight = theWOHeader.height(),
    woBtns = $(".wo-q-item"),
    woOpenTransport = $(".wo-transport-lrg .more-button"),
    woCloseTransport = $(".wo-transport-lrg .less-button"),
    woSearchOpen = $("#woSearchOpen"),
    woSearchClose = woSearchOpen.find(".less-button"),
    woSearchForm = woSearchOpen.find(".form-holder"),
    woSearchSubmit = $("#woSearchBtn"),
    woSearchBar = $("#woSearchBar"),
    questionHolder = $(".wo-questions"),
    woHeader = $(".wo-header"),
    woQCloseBtns = $(".wo-q-item .less-button"),
    woLeft = $(".wo-left"),
    woRight = $(".wo-right"),
    woActiveQ,
    selects = $(".selection input"),
    ieCalDropdown = $(".cal-ie-fallback #ie-month-select"),
    goButtons = $(".go-button"),
    resetButtons = $(".reset-filters"),
    selectedChoices = [],
    selectedChoicesObj = {what: [], where: [], when: ["any", "any"], view: "grid"},
    woShowingDefault = ["Plays, Events, Activities&hellip;", "Stratford-upon-Avon, London, On Tour&hellip;", "This Week, Next Week, Next Month&hellip;"],
    woActiveItem,
    gridItems = $(".wo-grid-item"),
    gridFocusOuter,
    rightNow = moment().format('LL'),
    yesterday = moment().subtract(1, 'days').format('LL'),
    nextWeek = moment().add(7, 'days').format('LL'),
    lastDay = moment().add(4, 'months').format('LL'),
    startDateSel,
    endDateSel,
    dsOffset,
    woChangeSearch = $("#woChangeSearch");



    // base template variables
var breakpoint = {},
    globalHeader = $("#globalHeader"),
    globalNav = $("#globalNav"),
    globalHeadertop = $(".header-top"),
    loginSearch = $(".login-search"),
    topLevelLogin = $("#MobileTopLevelLogin .login-link"),
    theMenuButton = $("#menuButton"),
    theBurger = $("#burgerHolder"),
    mainNav = $("nav"),
    mainNavLis = $(".top-level li"),
    mainNavBtns = $(".top-level li a.top-level-link:not(.nodrop)"),
    subNavItems = $(".subnav-item"),
    subNavBkgrnd = $(".subnav-bkgrnd"),
    subNavClose = $("#subNavCloseBtn"),
    didScroll,
    lastScrollTop = 0,
    delta = 5,
    headerHeight = globalNav.outerHeight() + globalHeadertop.outerHeight(),
    fullHeaderHeight = (headerHeight + $(".play-hub header").outerHeight()) - 40,
    gFooterOffset = $(".footer-container").offset(),
    gFooterTop = gFooterOffset.top,
    windowHeight = window.innerHeight,
    floatingMenu = $("#floatingMenu"),
    globalSearch = $(".global-search-box"),
    globalSearchOpen = $(".search-open"),
    globalSearchClose = $(".global-search-box .close-btn"),
    globalSearchInput = $("#globalSearchInput"),
    globalSearchBtn = $("#globalSearchButton"),
    shopSearch = $(".shop-header input[type=text]"),
    shopSearchGo = $(".shop-header #shopSearch"),
    //cookieBar = $("#cookieBar"),
    infoPopUp = $(".info-popup"),
    //cookieClose = $("#cookieBar .close-btn"),
    infoClose =  $(".info-popup .close-btn"),
    sendMessage = $("#send-message"),
    contactFormHolder = $("#contact-form-holder"),
    galleryIframe = $(".gallery-photo-container iframe"),
    searchAgain = $(".search-again"),
    resourceForm = $(".resource-form"),
    resourcePlay = $(".resource-form #resPlaySelect"),
    resourceType = $(".resource-form #resType input[type=checkbox]"),
    resourceAge = $(".resource-form #resAge input[type=checkbox]"),
    resBankFilters = {
        play: [],
        type: [],
        age: []
    };

    // module variables
var allExMods = $(".ex-mod"),
    expandedModule,
    horzScollers = $(".hs-outer"),
    hsTransport = $(".hs-transport"),
    horzScollerInners = [];



$(function() {

    //topLevelLogin.clone().prependTo(loginSearch);

    /*---------- RESIZE FUNCTIONS ----------*/

    $(window).resize(function () {
        screenWidth = window.innerWidth;

        // reset menu items

        if (screenWidth < largeScreen) {
            headerHeight = globalHeadertop.outerHeight();

            // reset menu items
            if (theBurger.hasClass("active")){
                theBurger.removeClass("active");
                mainNavBtns.removeClass("vis");
                globalNav.removeClass("nav-active").addClass("nav-hide");
                subNavBkgrnd.removeClass("vis");
                subNavItems.removeClass("vis");
                theBody.removeClass("noscroll");
            }

            $(".timer-outer").removeClass("timer-hidden");
        } else {
            headerHeight = globalNav.outerHeight() + globalHeadertop.outerHeight();

            // reset menu items
            if (theBurger.hasClass("active")){
                //globalNav.removeClass("nav-active");
                //theBurger.removeClass("active");
            }
        }
        videoResize(largeVideoHolder, smlVideoHolder);

        // adding/removing Intro copy
        if (typeof introCopy !== 'undefined' && introCopy.length > 0){

            // if priority booking period is active
            if (typeof prBooking !== 'undefined' && prBooking.length > 0){

                // move the intro copy for anything above the smaller sizes
                if ((window.innerWidth >= mediumScreen && window.innerWidth < xlargeScreen) && $("#moveIntro").length < 1) {

                    moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop('id', 'moveIntro').prependTo(copyInner);
                    introCopy.css({display: "none"});
                    introCopy.parent(".play-intro-copy").css({position: "", bottom: ""});

                } else if (window.innerWidth < mediumScreen && $("#moveIntro").length > 0){

                    moveIntro.remove();
                    introCopy.css({display: ""});
                    introCopy.parent(".play-intro-copy").css({position: "", bottom: ""});

                } else if (window.innerWidth >= xlargeScreen && $("#moveIntro").length > 0){

                    moveIntro.remove();
                    introCopy.css({display: ""});
                    introCopy.parent(".play-intro-copy").css({position: "absolute", bottom: "32px"});

                } else if (window.innerWidth >= xlargeScreen){

                    introCopy.parent(".play-intro-copy").css({position: "absolute", bottom: "32px"});

                }

                // move priority booking message for medium screens
                if ((window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#movePRBooking").length < 1){

                    movePRBooking = prBooking.clone().removeClass("ongray-intro").addClass("intro-copy").prop('id', 'movePRBooking').prependTo(copyInner);

                    // when screen is small or wider than large, get rid of cloned prBooking
                } else if (!(window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#movePRBooking").length > 0){
                    movePRBooking.remove();
                }

            } else {
                // if the screen is between medium and large and introCopy hasn't already been moved
                if ((window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#moveIntro").length < 1){
                    // if there are quotes on the page, move intro copy below - NOT USING THIS NOW
                    //if (quoteHolder.length > 0){
                    //    moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop('id', 'moveIntro').insertAfter(quoteHolder);
                    //} else {
                        moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop('id', 'moveIntro').prependTo(copyInner);
                    //}

                    // when screen is small or wider than large, get rid of cloned introCopy
                } else if (!(window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#moveIntro").length > 0){
                    moveIntro.remove();
                }
            }
        }

        // moving sponsors logo for Top Level
        if((typeof mainImage !== 'undefined' && mainImage.length > 0) && (typeof sponLogo !== 'undefined' && sponLogo.length > 0)){
            if (window.innerWidth < mediumScreen && sponLogo.hasClass("first-level") && $(".move-sponsor").length < 1){
                moveSponsor = sponLogo.clone().addClass("move-sponsor").insertAfter(mainImage);
            } else if (window.innerWidth >= mediumScreen && $(".move-sponsor").length > 0) {
                moveSponsor.remove();
            }
        }

        // bottom menu for play hubs
        var playHubHeaderHeight = $(".play-hub header").outerHeight();

        windowHeight = window.innerHeight,
            gFooterOffset = $(".footer-container").offset(),
            gFooterTop = gFooterOffset.top,
            fullHeaderHeight = (headerHeight + playHubHeaderHeight) - 150;

        // whats on active item positioning
        if (typeof woActiveItem !== 'undefined' && woActiveItem.length > 0){
            var woActivePos = woActiveItem.position(), woActiveWidth = woActiveItem.width(), woActiveHeight = woActiveItem.height();
            var focusItem = gridFocusOuter.children(".wo-grid-item").css({"min-height": woActiveHeight}), gridFocusOuterHeight = gridFocusOuter.height();

            if ((screenWidth >= smadiumScreen && screenWidth < largeScreen) && woActivePos.left > 0) {
                focusItem.css({left: 0});
            } else if (screenWidth >= largeScreen && woActivePos.left > (gridFocusOuter.width()/2)) {
                focusItem.css({left: woActiveWidth});
            } else {
                focusItem.css({left: woActivePos.left});
            }

            // CHECK POSITION ACCORDING TO DISTANCE FROM BOTTOM
            if (woActiveHeight + woActivePos.top > gridFocusOuterHeight - 10){
                focusItem.css({bottom: 0});
            } else {
                focusItem.css({top: woActivePos.top});
            }
        }

        // What's On Filter Search Indicators Toggle
        if (typeof questionHolder !== 'undefined' && questionHolder.length > 0) {
            if (window.innerWidth >= mediumScreen) {
                questionHolder.find(".filters").attr("style", "");
                woBtns.each(function(i){
                    $(this).removeClass("active");
                    $(this).find(".wo-showing").html(woShowingDefault[i]);
                });
            }
        }

        // Perf list scroll bar recalculate
        if (typeof perfList !== 'undefined' && perfList.length > 0) {
            perfList.simplebar('recalculate');
        }

        if (typeof $("#openPerfs") !== 'undefined' && $("#openPerfs").length > 0) {

            var openPerfs = $("#openPerfs"),
                openWeek = openPerfs.parents(".week"),
                openDay = $(".day.selected"),
                openDayPerfs = $(".day.selected .all-perfs");

            if (screenWidth < mediumScreen && openPerfs.hasClass("cloned")){

                openPerfs.remove();

                openDayPerfs.show().prop("id", "openPerfs");

            } else if (screenWidth >= mediumScreen && !openPerfs.hasClass("cloned")) {

                var movePerfs = openPerfs.clone().appendTo(openWeek).addClass("cloned").css({height: ""});

                openPerfs.hide().css({height: "", opacity: ""}).prop("id", "");
            }
        }

    }).resize();




    /*---------- PHOTOSWIPE SETUP ----------*/
    // initPhotoSwipeFromDOM('.gallery');





    /*---------- ADDING TEST FOR MIX BLEND MODE ----------*/
    Modernizr.addTest('mix-blend-mode', function(){
        return Modernizr.testProp('mixBlendMode');
    });
    // find safari + change class of yellow 'color' header strips
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        $(".bkgrnd-strip.yellow-color").addClass("safari");
    }




    /*---------- HOMEPAGE QUICKLINKS REVEAL ----------*/
    //quickLinksBtn.click(function(e){
    //    if (screenWidth < mediumScreen && !quickLinksBtn.hasClass("opened")){
    //        quicklinksCont.css("height","auto");
    //        TweenLite.from(quicklinksCont, 0.2, {
    //            height: 0,
    //            opacity: 0,
    //            ease: Circ.easeOut
    //        });
    //        quickLinksBtn.addClass("opened");
    //    }
    //});
    quickLinksBtn.click(function(e){
        if (screenWidth < mediumScreen && !quickLinksBtn.hasClass("opened")){
            showIt(e);
        }
    })




    /*---------- SHOWHIDDEN ----------*/
    showHidden.each(function(){
        var $thisHid = $(this);

        $thisHid.click(function(e){
            e.preventDefault();
            showIt(e);
        });
    });




    /*---------- NAV FUNCTIONS ----------*/
    // MENU BUTTON FUNCTION
    theMenuButton.click(function(e){
        theBurger.toggleClass("active");
        subNavItems.removeClass("vis");

        if (window.innerWidth < largeScreen) {
            mainNav.toggleClass("nav-active").toggleClass("nav-hide");
            theBody.toggleClass("noscroll");

        } else if (window.innerWidth >= largeScreen) {
            if (lastScrollTop > headerHeight - 20) {
                globalNav.toggleClass("nav-up").toggleClass("nav-down");
            } else {
                theMenuButton.toggleClass("mb-active");
            }

            if (subNavBkgrnd.hasClass("vis")){
                subNavBkgrnd.removeClass("vis").addClass("vis-fade");

                setTimeout(function(){
                    subNavBkgrnd.removeClass("vis-fade");
                    theBody.removeClass("noscroll");
                }, 150);
            }
        }

        if (window.innerWidth >= mediumScreen && $(".timer-outer").hasClass("timer-hidden")){
            $(".timer-outer").removeClass("timer-hidden");
            TweenLite.from($(".timer-outer"), 0.2, {
                opacity: 0,
                onComplete: function(){
                    $(".timer-outer").css({"opacity": ""});
                }
            });
        } else if (window.innerWidth >= mediumScreen && window.innerWidth < largeScreen && !$(".timer-outer").hasClass("timer-hidden")) {
            TweenLite.to($(".timer-outer"), 0.3, {
                opacity: 0,
                onComplete: function(){
                    $(".timer-outer").css({"opacity": ""}).addClass("timer-hidden");
                }
            });
        }
    });

    // SET UP THE NAV
    navBtns(mainNavBtns, subNavItems);

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    // CHECK IF SCREEN HAS SCROLLED
    function hasScrolled() {
        var st = $(this).scrollTop();

        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (window.innerWidth >= largeScreen) {
            if (st > lastScrollTop && st > headerHeight - 20 && !theMenuButton.hasClass("mb-active")){
                // Scroll Down, if past headerheight, scroll the nav up + add burger to stage
                globalNav.addClass("nav-up").removeClass("nav-down");
                theMenuButton.addClass("mb-active");

            } else if (st < lastScrollTop && st < headerHeight + 100) {
                // scroll up, if past headerheight, scroll the nav down + remove burger
                globalNav.removeClass("nav-up").addClass("nav-down");
                theMenuButton.removeClass("mb-active");
                theBurger.removeClass("active");

            } else if (st < lastScrollTop && lastScrollTop - st) {
                // scroll up, if scoll is over 20px, scroll the nav down + activate burger
                globalNav.removeClass("nav-up").addClass("nav-down");
                theBurger.addClass("active");

            } else if (st > headerHeight && theBurger.hasClass("active")) {
                // Scroll down, remove cloned nav from stage, change burger back to normal
                globalNav.addClass("nav-up").removeClass("nav-down");
                theBurger.removeClass("active");

            }
        }

        // floating nav active
        if ((st > fullHeaderHeight) && ((st + windowHeight) < gFooterTop) && !(floatingMenu.hasClass("active"))){
            floatingMenu.addClass("active");
        } else if (((st < fullHeaderHeight) || ((st + windowHeight) > gFooterTop)) && floatingMenu.hasClass("active")) {
            floatingMenu.removeClass("active");
            floatNav.removeClass("active");
            floatNavOn.removeClass("open");
            floatNavOff.addClass("closed");
            nextPerf.removeClass("active");
        }
        lastScrollTop = st;
    }




    /*---------- INFO POPUP/COOKIES FUNCTIONS ----------*/
    if (typeof infoPopUp !== undefined && infoPopUp.length > 0){
        var infoPHeight = [8];

        infoPopUp.each(function(i){
            var $this = $(this);

            if (!$this.hasClass("here")) {
                var theTime = 800 + (350 * i);
                var newHeight = infoPHeight.reduce(arrayAdd,0) + 8;

                // open infoPopup after timeout
                setTimeout(function(){
                    $this.css({"visibility": "visible"}).addClass("here");
                    TweenLite.to($this, 0.4, {
                        bottom: newHeight,
                        opacity: 1,
                        ease: Circ.easeOut
                    });
                }, theTime);

                infoPHeight.push($this.height() + 8);
            }
        });
    }

    infoClose.each(function(i){
        var $this = $(this);
        $this.click(function(){
            closeInfo(i);
        })
    });

    $(".info-copy .more-button").click(function(e){
        e.preventDefault();
        var $this = $(this);

        $this.siblings(".hide").removeClass("hide");
        $this.hide();
    });




    /*---------- SEARCH FUNCTIONS ----------*/
    globalSearchOpen.each(function(i){
        $this = $(this);
        $this.click(function(e){
            e.preventDefault();
            globalSearchShow();
        });
    });

    globalSearchClose.click(function(e){
        globalSearchHide();
    });

    globalSearchInput.change(function() {
        var str = globalSearchInput.val();
        str = str.replace(/\s+/g, ' ').toLowerCase();

        globalSearchBtn.prop("href", "/search?q=" + str);
    });

    globalSearchBtn.click(function(e) {
        e.preventDefault;

        var str = globalSearchInput.val();
        str = str.replace(/\s+/g, ' ').toLowerCase();

        globalSearchBtn.prop("href", "/search?q=" + str);

        window.location.href = "/search?q=" + str;
    });

    globalSearchInput.on("keypress", function(e) {
        if (e.keyCode == 13) {
            var str = globalSearchInput.val();
            str = str.replace(/\s+/g, ' ').toLowerCase();

            globalSearchBtn.prop("href", "/search?q=" + str);

            window.location.href = "/search?q=" + str;

            return false; // prevent the button click from happening
        }
    });


    // SHOP SEARCH
    shopSearch.change(function() {
        var str = shopSearch.val();
        str = str.replace(/\s/g, ' ').toLowerCase();

        shopSearchGo.prop("href", "/shop/search?q=" + str);
    });

    shopSearchGo.click(function(e) {
        e.preventDefault;
        var str = shopSearch.val();
        str = str.replace(/\s+/g, ' ').toLowerCase();
        shopSearchGo.prop("href", "/shop/search?q=" + str);
        window.location.href = "/shop/search?q=" + str;
    });

    shopSearch.on("keypress", function(e) {
        if (e.keyCode == 13) {
            var str = shopSearch.val();
            str = str.replace(/\s+/g, ' ').toLowerCase();

            shopSearchGo.prop("href", "/shop/search?q=" + str);

            window.location.href = "/shop/search?q=" + str;

            return false; // prevent the button click from happening
        }
    });




    /*---------- FORMS' CALENDARS ----------*/
    // gift membership start date
    if ($("#membDate").length > 0 && typeof pikadayResponsive != undefined) {

        var todayPlusSeven = new Date();
        todayPlusSeven.setDate(todayPlusSeven.getDate() + 7);

        var memberDate = new pikadayResponsive(document.getElementById('membDate'), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "member-start-date",
            pikadayOptions : {
                minDate: todayPlusSeven
            }
        });

        if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) {
            $("#membDate").change(function(){

                var theActualDate = $("#membDate").attr("value");

                if (moment(theActualDate, "ll").isBefore(todayPlusSeven)){
                    $(".date-validator").fadeIn(200);
                    $("#membDate").siblings("input[type=text]").addClass("error");
                } else {
                    $(".date-validator").hide();
                    $("#membDate").siblings("input[type=text]").removeClass("error");
                }
            });
        }
    }

    // date of birth
    if ($(".dobField").length > 0 && typeof pikadayResponsive != undefined) {

        var dobStart = new Date(),
            dobEnd = new Date(),
            dobTenYears = new Date(),
            dobSet,
            dobDefault;

        dobStart.setYear(dobStart.getYear() - 100);
        dobTenYears.setYear(dobTenYears.getFullYear() - 10);
        dobEnd.setYear(dobEnd.getFullYear() - 5);

        if ($(".dobField").prop("value") !== ""){
            dobSet = new Date($(".dobField").attr("value"));
            dobDefault = true;
        } else {
            dobSet = dobEnd;
            dobDefault = false;
        }

        var dobDate = new pikadayResponsive($(".dobField"), {
            format : "ll",
            outputFormat : "YYYY-MM-DD",
            classes : "member-dob",
            pikadayOptions : {
                minDate: dobStart,
                maxDate: dobEnd,
                defaultDate: dobSet,
                setDefaultDate: dobDefault,
                yearRange: 100
            }
        });

        if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) {
            $(".dobField").change(function(){

                var theActualDate = moment($(".dobField").prop("value"), "YYYY-MM-DD");

                if (theActualDate.isBefore(dobStart)){
                    $(".form-error").hide();
                    $(".hundred-years").fadeIn(200);
                    $(".dobField").siblings("input[type=text]").addClass("error");
                } else if (theActualDate.isAfter(dobEnd)) {
                    $(".form-error").hide();
                    $(".future").fadeIn(200);
                    $(".dobField").siblings("input[type=text]").addClass("error");
                }    else if (theActualDate.isAfter(dobTenYears)) {
                    $(".form-error").hide();
                    $(".ten-years").fadeIn(200);
                    $(".dobField").siblings("input[type=text]").addClass("error");
                } else {
                    $(".form-error").hide();
                    $(".dobField").siblings("input[type=text]").removeClass("error");
                }
            });
        }
    }




    /*---------- EDUCATION RESOURCE ----------*/
    // FORM REVEAL
    searchAgain.each(function(i) {
        $(this).click(function(){
            var theForm = $(this).siblings(".form-holder");
            theForm.show();
            TweenLite.from(theForm, 0.4, {
                height: 0,
                opacity: 0
            });
            TweenLite.to($(this), 0.2, {
                height: 0,
                opacity: 0,
                "padding-top": 0,
                "padding-bottom": 0
            });
        });
    });


    resBankFilters.play = resourcePlay.val();

    resourcePlay.change(function() {
        resBankFilters.play = $(this).val();
        edResourceLink();
    });

    resourceType.each(function() {
        var resTypeCopy = $(this).attr("id"),
            resTypeArr = resBankFilters.type;

        if ($(this).attr("checked") == "checked" || $(this).is(':checked')) {
            resTypeArr.push(resTypeCopy);
        }
        edResourceLink();

        $(this).change(function(){
            // check if it's in the array, remove if it is, add if not
            if ($.inArray(resTypeCopy, resTypeArr) > -1){
                resTypeArr.splice($.inArray(resTypeCopy, resTypeArr), 1);
            } else {
                resTypeArr.push(resTypeCopy);
            }
            edResourceLink();
        });
    });

    resourceAge.each(function() {
        var resAgeCopy = $(this).attr("id"),
            resAgeArr = resBankFilters.age;

        if ($(this).attr("checked") == "checked" || $(this).is(':checked')) {
            resAgeArr.push(resAgeCopy);
        }
        edResourceLink();

        $(this).change(function(){
            //if ($(this).attr("checked") == "checked" || $(this).is(':checked')) {
            //
            //} else {

            // check if it's in the array, remove if it is, add if not
            if ($.inArray(resAgeCopy, resAgeArr) > -1){
                resAgeArr.splice($.inArray(resAgeCopy, resAgeArr), 1);
            } else {
                resAgeArr.push(resAgeCopy);
            }
            //}
            edResourceLink();
        });
    });



    /*---------- FAQ CONTACT FORM ----------*/
    if (typeof sendMessage != undefined) {
        sendMessage.click(function(e){
            e.preventDefault;

            contactFormHolder.show();
            TweenLite.from(contactFormHolder, 0.4, {
                height: 0,
                opacity: 0
            });
            TweenLite.to(sendMessage, 0.2, {
                height: 0,
                opacity: 0
            });
        });
    }




    /*---------- WHATS ON FUNCTIONS ----------*/

    // search again
    woChangeSearch.click(function(e){
        e.preventDefault();
        gimmeGo(theWOHeader);
    });

    // large sizes, open interface
    woOpenTransport.click(function(e){
        questionHolder.addClass("active");
    });
    // large sizes, close interface
    woCloseTransport.click(function(e){
        questionHolder.removeClass("active");
    });

    // make the pikaday calendars/datepicker
    if ($("#woStart").length > 0 && typeof pikadayResponsive !== undefined){

        var finalDate = new Date(),
            startSetDate,
            SDisSet = false,
            endSetDate,
            EDisSet = false,
            woStartAttr = $("#woStart").attr("data-value"),
            woEndAttr = $("#woEnd").attr("data-value");

        finalDate.setDate(finalDate.getDate() + 540);

        if (typeof woStartAttr !== typeof undefined && woStartAttr !== false) {
            startSetDate = new Date($("#woStart").attr("data-value"));
            SDisSet = true;
            selectedChoicesObj.when[0] = moment($("#woStart").attr("data-value")).format("YYYY-MM-DD");

            goButtons.each(function(i){
                $(this).removeClass("inactive");
            });
            resetButtons.each(function(i){
                $(this).removeClass("inactive");
            });

            updateLink();

        } else {
            startSetDate = new Date();

        }

        if (typeof woEndAttr !== typeof undefined && woEndAttr !== false) {
            endSetDate = new Date($("#woEnd").attr("data-value"));
            EDisSet = true;
            selectedChoicesObj.when[1] = moment($("#woEnd").attr("data-value")).format("YYYY-MM-DD");

            goButtons.each(function(i){
                $(this).removeClass("inactive");
            });
            resetButtons.each(function(i){
                $(this).removeClass("inactive");
            });

        } else {
            endSetDate = startSetDate;
            endSetDate.setDate(endSetDate.getMonth() + 1);
        }

        var firstRun = 0,
            startDate,
            endDate;

        if ($(".filters .calendar .text-fields").is(":visible")) {
            var updateStartDate = function() {
                    startPicker.pikaday.setStartRange(startDate);
                    endPicker.pikaday.setStartRange(startDate);
                    endPicker.pikaday.setMinDate(startDate);
                    // make go and reset buttons active
                    goButtons.each(function(i){
                        $(this).removeClass("inactive");
                    });
                    resetButtons.each(function(i){
                        $(this).removeClass("inactive");
                    });
                },
                updateEndDate = function() {
                    startPicker.pikaday.setEndRange(endDate);
                    startPicker.pikaday.setMaxDate(endDate);
                    endPicker.pikaday.setEndRange(endDate);
                    // make go and reset buttons active
                    goButtons.each(function(i){
                        $(this).removeClass("inactive");
                    });
                    resetButtons.each(function(i){
                        $(this).removeClass("inactive");
                    });
                },
                startPicker = new pikadayResponsive(document.getElementById('woStart'), {
                    format: "D MMM YY",
                    outputFormat: "ll",
                    classes: "woStartDate",
                    pikadayOptions : {
                        minDate: new Date(),
                        maxDate: finalDate,
                        defaultDate: startSetDate,
                        setDefaultDate: SDisSet,
                        onSelect: function() {
                            startDate = this.getDate();

                            if (firstRun > 0) {
                                updateStartDate();
                            }

                            // set the start date in the Selected Choices Object
                            startDateSel = moment(startDate).format("d MMM YY");
                            selectedChoicesObj.when[0] = moment(startDate).format("YYYY-MM-DD");

                            if (SDisSet) {
                                goButtons.each(function(i){
                                    $(this).removeClass("inactive");
                                });
                                resetButtons.each(function(i){
                                    $(this).removeClass("inactive");
                                });
                            }

                            updateLink();
                        }
                    }
                }),
                endPicker = new pikadayResponsive(document.getElementById('woEnd'), {
                    format: "D MMM YY",
                    outputFormat: "ll",
                    classes: "woEndDate",
                    pikadayOptions : {
                        minDate: new Date(),
                        maxDate: finalDate,
                        defaultDate: endSetDate,
                        setDefaultDate: EDisSet,
                        onSelect: function() {
                            endDate = this.getDate();
                            if (firstRun > 0) {
                                updateEndDate();
                            }
                            // set the end date in the Selected Choices Object
                            endDateSel = moment(endDate).format("d MMM YY");
                            selectedChoicesObj.when[1] = moment(endDate).format("YYYY-MM-DD");

                            if (EDisSet) {
                                goButtons.each(function(i){
                                    $(this).removeClass("inactive");
                                });
                                resetButtons.each(function(i){
                                    $(this).removeClass("inactive");
                                });
                            }

                            updateLink();
                        }
                    }
            });

            firstRun += 1;

            // if it's not a native picker get the Pikaday dates
            if (startPicker.pikaday){
                var _startDate = startPicker.pikaday.getDate(),
                    _endDate = endPicker.pikaday.getDate();
            } else {
                // if it is, get the current set dates
                // THIS IS SORTED IN PIKADAYRESPONSIVE.JS, line 103
            }
        }
    }

    if (_startDate) {
        startDate = _startDate;
        updateStartDate();
    }

    if (_endDate) {
        endDate = _endDate;
        updateEndDate();
    }

    // question buttons
    woBtns.each(function(i){
        $(this).click(function(){
            if (!$(this).hasClass("active") && (screenWidth < mediumScreen)) {
                openQuestion(i);
            } else if (!questionHolder.hasClass("active") && (screenWidth >= mediumScreen)) {
                questionHolder.addClass("active");
            }
        });
    });

    // open search
    woSearchOpen.click(function(e){
        e.stopPropagation();
        var theSearch = $(this), theBox = theSearch.find(".form-holder");


        if (!theSearch.hasClass("active")){
            closeQuestion();

            theSearch.addClass("active");
            TweenLite.to(theBox, 0.4, {
                height: "auto",
                "margin-top": "16px",
                "margin-bottom": "16px",
                opacity: 1,
                onComplete: function() {
                    if (window.innerWidth < mediumScreen){
                        $('html, body').animate({
                            scrollTop: theSearch.offset().top - 90
                        }, 500);
                    }
                }
            });
            TweenLite.to(theWOHeader, 0.4, {
                height: "auto"
            });
        }
    });

    // close search
    woSearchClose.click(function(e){
        e.stopPropagation();
        closeQuestion();
    });

    woSearchBar.change(function() {
        var str = woSearchBar.val();
        str = str.replace(/\s+/g, '-').toLowerCase();

        woSearchSubmit.prop("href", "/whats-on/search/" + str);
    });

    woSearchSubmit.click(function(e) {
        e.preventDefault;
        var str = woSearchBar.val();
        str = str.replace(/\s+/g, '-').toLowerCase();

        woSearchSubmit.prop("href", "/whats-on/search/" + str);

        window.location.href = "/whats-on/search/" + str;
    });

    woSearchBar.on("keypress", function(e) {
        if (e.keyCode == 13) {
            var str = woSearchBar.val();
            str = str.replace(/\s+/g, '-').toLowerCase();

            woSearchSubmit.prop("href", "/whats-on/search/" + str);

            window.location.href = "/whats-on/search/" + str;

            return false; // prevent the button click from happening
        }
    });


    // previous question buttons
    woLeft.each(function(i){
        --i;
        if (i < 0) {
            i = 2;
        }
        $(this).click(function(e){
            e.stopPropagation();
            openQuestion(i);
        });
    });

    // next question buttons
    woRight.each(function(i){
        ++i;
        if (i > 2){
            i = 0;
        }
        $(this).click(function(e){
            e.stopPropagation();
            openQuestion(i);
        });
    });

    // close question buttons
    woQCloseBtns.each(function(i) {
        var myQuestion = $(woBtns[i]);

        $(this).click(function(e){
            e.stopPropagation();
            closeQuestion();
        });

    });

    // click selections
    selects.each(function(i){
        var theSelect = $(this),
            theSelectCopy = $(this).attr("id"),
            theQuestion = theSelect.parents(".wo-q-item").attr("class").replace("wo-q-item wo-", "").replace(" active", ""),
            theQuestionArr = selectedChoicesObj[theQuestion];

        if (theSelect.attr("checked") == "checked" || theSelect.is(':checked')) {
            goButtons.each(function(i){
                $(this).removeClass("inactive");
            });
            resetButtons.each(function(i){
                $(this).removeClass("inactive");
            });

            theSelect.addClass("selected");
            theQuestionArr.push(theSelectCopy);

            // CHANGE THE LINK
            updateLink();
        }

        theSelect.click(function(e){
            e.stopPropagation();

            // if it's already selected, deselect and remove from the object
            if (theSelect.hasClass("selected")){
                theSelect.removeClass("selected");
                if ($.inArray(theSelectCopy, theQuestionArr) > -1){
                    theQuestionArr.splice($.inArray(theSelectCopy, theQuestionArr), 1);
                }
            } else {
                theSelect.addClass("selected");
                theQuestionArr.push(theSelectCopy);
            }

            // activate/deactivate go + reset buttons
            if ($(".category .selected").length > 0 && goButtons.hasClass("inactive")){
                goButtons.each(function(i){
                    $(this).removeClass("inactive");
                });
                resetButtons.each(function(i){
                    $(this).removeClass("inactive");
                });
            } else if ($(".category .selected").length < 1) {
                goButtons.each(function(i){
                    $(this).addClass("inactive");
                });
                resetButtons.each(function(i){
                    $(this).addClass("inactive");
                });
            }

            // CHANGE THE LINK
            updateLink();
        });
    });

    if (ieCalDropdown.is(":visible")) {
        if (ieCalDropdown.val() != null) {
            var rightNow = moment();

            var dropStartDate = moment([moment().year()]).month(ieCalDropdown.val());

            if (dropStartDate.isBefore(rightNow)) {
                dropStartDate.year(moment().year() + 1);
            }

            // Clone the value before .endOf()
            var dropEndDate = moment(dropStartDate).endOf('month');

            selectedChoicesObj.when[0] = moment(dropStartDate).format("YYYY-MM-DD");
            selectedChoicesObj.when[1] = moment(dropEndDate).format("YYYY-MM-DD");

            // just for demonstration:
            //console.log(selectedChoicesObj.when[0]);
            //console.log(selectedChoicesObj.when[1]);

            updateLink();

        }

        ieCalDropdown.change(function() {
            var rightNow = moment();

            var dropStartDate = moment([moment().year()]).month(ieCalDropdown.val());

            if (dropStartDate.isBefore(rightNow)) {
                dropStartDate.year(moment().year() + 1);
            }

            // Clone the value before .endOf()
            var dropEndDate = moment(dropStartDate).endOf('month');

            selectedChoicesObj.when[0] = moment(dropStartDate).format("YYYY-MM-DD");
            selectedChoicesObj.when[1] = moment(dropEndDate).format("YYYY-MM-DD");

            // just for demonstration:
            //console.log(selectedChoicesObj.when[0]);
            //console.log(selectedChoicesObj.when[1]);

            updateLink();
        });
    }

    // reset buttons
    resetButtons.each(function(i){
        $(this).click(function(e){
            if (!$(this).hasClass("inactive")){
                // remove .selected from cats and selections, make go + resets inactive
                $(".category .selected").removeClass("selected").prop('checked', false);
                resetButtons.addClass("inactive");
                goButtons.addClass("inactive");

                // remove selected items from Showing div + replace with originals
                woBtns.each(function(i){
                    $(this).find(".wo-showing").html(woShowingDefault[i]).removeClass("chosen");
                });

                startPicker.pikaday.setDate(null);
                endPicker.pikaday.setDate(null);

                // clear selectedChoicesObj array
                for (var k in selectedChoicesObj){
                    selectedChoicesObj[k] = [];
                }
            }
        });
    });

    // clicking grid item
    gridItems.each(function(i){
        var $this = $(this);
        $this.click({theGridItem: $this}, clickTheGrid);
    });

    // clicking calendar day
    allExMods.each(function(i){
        var $this = $(this);

        // OLD CLICK EVENT HANDLER
        //$this.click({theMod: $this}, expandInnerDetails);

        $this.off("click", expandInnerDetails).on("click", {theMod: $this}, expandInnerDetails);
    });




    /*---------- HORIZONTAL SCROLLERS ----------*/

    horzScollers.each(function(i){
        var $this = $(this);
        var hsItem = $this.find(".hs-item").first();
        var goLeft = $(this).siblings(".hs-transport").find(".hs-left");
        var goRight = $(this).siblings(".hs-transport").find(".hs-right");

        goLeft.click({thePosition: i, theFC: hsItem, theDirection: "left"}, hsScrollMe);
        goRight.click({thePosition: i, theFC: hsItem, theDirection: "right"}, hsScrollMe);
    })



    /*---------- PLAY HUB FUNCTIONS ----------*/
    // floating nav functions
    floatNavOn.click(function(e){
        e.preventDefault();
        e.stopPropagation()
        floatNav.toggleClass("active");
        floatNavOn.toggleClass("open");
        floatNavOff.toggleClass("closed");
        floatSubNav.toggleClass("open");
        nextPerf.toggleClass("active");
    });

    floatNavOff.click(function(e){
        e.preventDefault();
        e.stopPropagation()
        floatNav.toggleClass("active");
        floatNavOn.toggleClass("open");
        floatNavOff.toggleClass("closed");
        floatSubNav.toggleClass("open");
        nextPerf.toggleClass("active");
    });

    $(document).on(
      'click',
      function(event){
        if (!$(event.target).closest('#floatingMenu').length && floatSubNav.hasClass('open')) {
          floatNav.toggleClass("active");
          floatNavOn.toggleClass("open");
          floatNavOff.toggleClass("closed");
          floatSubNav.toggleClass("open");
          nextPerf.toggleClass("active");
        }
      }
    );

    // play header video
    playVideo.each(function(i) {
        var $this = $(this);
        $this.click(function(e) {
            $this.fadeOut(150);
            $(videoCover[i]).fadeOut(150);
            $(".hero-copy").fadeOut(150);
            var theID = $this.parents(".video-holder").attr('id');

            if (isTouchDevice()===true) {
                callPlayer(theID, function(){
                    // call the player but do nothing
                });

                if (getFrameID("homeVideoHolder") !== null){
                    var player; //Define a player object, to enable later function calls, without
                    // having to create a new class instance again.

                    // Add function to execute when the API is ready
                    YT_ready(function(){
                        var frameID = getFrameID("homeVideoHolder");
                        if (frameID) { //If the frame exists
                            player = new YT.Player(frameID, {
                                events: {
                                    "onStateChange": homeVidEnd
                                }
                            });
                        }
                    });
                }
            } else {
                callPlayer(theID, function() {
                    // This function runs once the player is ready ("onYouTubePlayerReady")
                    callPlayer(theID, "playVideo");
                    //callPlayer(theID, "")
                });
                // When the player is not ready yet, the function will be queued.
                // When the iframe cannot be found, a message is logged in the console.
                callPlayer(theID, "playVideo");

                //console.log(getFrameID("homeVideoHolder"));

                if (getFrameID("homeVideoHolder") !== null){
                    var player; //Define a player object, to enable later function calls, without
                    // having to create a new class instance again.

                    // Add function to execute when the API is ready
                    YT_ready(function(){
                        console.log("YT_ready");
                        var frameID = getFrameID("homeVideoHolder");
                        if (frameID) { //If the frame exists
                            player = new YT.Player(frameID, {
                                events: {
                                    "onStateChange": homeVidEnd
                                }
                            });
                        }
                    });
                }
            }
        });
    });




    /*---------- HOMEPAGE VIDEO FUNCTIONS ----------*/
    // when home video finishes
    function homeVidEnd(event) {
        //console.log("found state change");
        if (event.data == 0){
            //console.log("found end");
            $("#homeVideoHolder #videoCover").fadeIn(200);
            $(".hero-copy .intro-btn").css({"display":"inline-block"});
            $(".hero-copy").fadeIn(200);
            playVideo.fadeIn(200);
        }
    }




    /*---------- PERFORMANCE LIST FILTERING ----------*/
    // TEMP DATE SETTER, add dates in LL format

    // set up perf date sorter
    var unsorted_times = new Array();

    plFullList.each(function(i){

        var thePerfDate = $(this).find(".date").html();

        // perf date object
        var moment_date = moment(thePerfDate);
        var unsorted_time = new Object();
        unsorted_time.time = thePerfDate;
        unsorted_time.milli = moment_date.valueOf();
        unsorted_times.push(unsorted_time);

        $(this).attr("data-when", moment(thePerfDate, "ddd DD MMM YYYY").format("LL"));
    });

    if (unsorted_times.length > 0){
        // sort the dates
        var perfDateSorted = unsorted_times.sort(compareMilli);

        // get first performance date
        //var firstPerfDate = $(plFullList[0]).find(".date").html();
        var firstPerfDate = perfDateSorted[0].milli;

        // set the object
        plActiveFilters.when[0] = moment(firstPerfDate).format("YYYY-MM-DD");

        // get last performance date
        var lastPerfPos = perfDateSorted.length - 1;
        var lastPerfDate = perfDateSorted[lastPerfPos].milli;
        // set the object
        plActiveFilters.when[1] = moment(lastPerfDate).format("YYYY-MM-DD");
    }

    // make the pikaday calendars/datepicker
    if ($('.pl-calendar').is(":visible") && $("#plStart").length > 0 && typeof pikadayResponsive != undefined){

        var startDateSet = false,
            sdDefault,
            endDateSet = false,
            edDefault;

        if (getQueryVariable("plStart") !== false){
            startDateSet = true;
            sdDefault = moment(getQueryVariable("plStart"), "YYYY-MM-DD").toDate();
        }
        if (getQueryVariable("plEnd") !== false){
            endDateSet = true;
            edDefault = moment(getQueryVariable("plEnd"), "YYYY-MM-DD").toDate();
        }

        var startPLDate,
            endPLDate,
            updatePLStartDate = function() {
                startPLPicker.pikaday.setStartRange(startPLDate);
                endPLPicker.pikaday.setStartRange(startPLDate);
                endPLPicker.pikaday.setMinDate(startPLDate);
            },
            updatePLEndDate = function() {
                startPLPicker.pikaday.setEndRange(endPLDate);
                startPLPicker.pikaday.setMaxDate(endPLDate);
                endPLPicker.pikaday.setEndRange(endPLDate);
            },
            startPLPicker = new pikadayResponsive(document.getElementById('plStart'), {
                format: "D MMM YY",
                outputFormat: "ll",
                classes: "plStartDate",
                pikadayOptions : {
                    minDate: new Date(),
                    maxDate: new Date(moment(lastPerfDate, "ddd DD MMM YYYY").format("YYYY-MM-DD")),
                    defaultDate: sdDefault,
                    setDefaultDate: startDateSet,
                    onSelect: function() {
                        startPLDate = this.getDate();
                        updatePLStartDate();
                        // set the start date in the Selected Choices Object
                        plActiveFilters.when[0] = moment(startPLDate).format("YYYY-MM-DD");

                        perfFilter({
                            startDate: plActiveFilters.when[0]
                        });
                    }
                }
            }),
            endPLPicker = new pikadayResponsive(document.getElementById('plEnd'), {
                format: "D MMM YY",
                outputFormat: "ll",
                classes: "plEndDate",
                pikadayOptions : {
                    minDate: new Date(),
                    maxDate: new Date(moment(lastPerfDate, "ddd DD MMM YYYY").format("YYYY-MM-DD")),
                    defaultDate: edDefault,
                    setDefaultDate: endDateSet,
                    onSelect: function() {
                        endPLDate = this.getDate();
                        updatePLEndDate();
                        // set the end date in the Selected Choices Object
                        plActiveFilters.when[1] = moment(endPLDate).format("YYYY-MM-DD");

                        perfFilter({
                            endDate: plActiveFilters.when[1]
                        });
                    }
                }
            });

        // if it's not a native picker get the Pikaday dates
        if (startPLPicker.pikaday){
            var _startPLDate = startPLPicker.pikaday.getDate(),
                _endPLDate = endPLPicker.pikaday.getDate();
        } else {
            // if it is, get the current set dates
            // THIS IS SORTED IN PIKADAYRESPONSIVE.JS, line 103
        }
    }

    if (_startPLDate) {
        startPLDate = _startPLDate;
        updatePLStartDate();
    }

    if (_endPLDate) {
        endPLDate = _endPLDate;
        updatePLEndDate();
    }

    // checkbox clicks
    plChecks.each(function(){
        var theCheck = $(this);

        theCheck.click(function(){
            // use perfFilter function in play-hub.js, feed what's clicked to it
            perfFilter({
                checkBox: theCheck
            });
        });
    });

    // set up simplebar for performance list
    perfList.simplebar();


    // PRE-FILTERED PERF LISTS

    // where, time, etc filters
    if (typeof plFilterGroups !== 'undefined' && plFilterGroups.length > 0){

        var checkChangeList = [];

        plFilterGroups.each(function(i){
            var $this = $(this);
            var filterID = this.id;
            var preFilter = getQueryVariable(filterID);

            if (preFilter !== false){
                var filterChecks = $this.find(".k-checkbox");

                filterChecks.each(function(){
                    var $newThis = $(this),
                        theCategory = $newThis.attr("name"),
                        theBox = $newThis.prop("id");

                    $newThis.removeAttr("checked");

                    plActiveFilters[theCategory].splice($.inArray(theBox, plActiveFilters[theCategory]), 1);

                    if (theBox == preFilter) {
                        checkChangeList.push($newThis);
                    }
                });
            }
        });

        // go through check change list array and amend as appropriate
        for (var i = 0; i < checkChangeList.length; i++) {

            var $this = checkChangeList[i],
                theCategory = $this.attr("name"),
                theBox = $this.prop("id");

            if (getQueryVariable("plStart") !== false || getQueryVariable("plEnd") !== false) {

                // if plStart or plEnd query strings are set, manually add it to plActiveFilters and check the box
                plActiveFilters[theCategory].push(theBox);
                $this.prop("checked", "checked");

            } else {

                // if plStart or plEnd query strings aren't set
                if (i == checkChangeList.length - 1){
                    // if it's the last in the list force a click
                    $this.trigger("click");
                } else {
                    // otherwise, manually add it to plActiveFilters and check the box
                    plActiveFilters[theCategory].push(theBox);
                    $this.prop("checked", "checked");
                }
            }
        }

        // get pre-filter dates from query string
        if (getQueryVariable("plStart") !== false && getQueryVariable("plEnd") !== false) {

            // if both plStart and plEnd query strings are set, manually add the end date to plActiveFilters
            plActiveFilters.when[1] = moment(getQueryVariable("plEnd")).format("YYYY-MM-DD");
            // and force select for plStart
            startPLPicker.setDate(moment(getQueryVariable("plStart"), 'YYYY-MM-DD'));


        } else if (getQueryVariable("plStart") !== false && getQueryVariable("plEnd") == false) {

            // if only plStart query string is set
            startPLPicker.setDate(moment(getQueryVariable("plStart"), 'YYYY-MM-DD'));

        } else if (getQueryVariable("plStart") == false && getQueryVariable("plEnd") !== false){

            // if only plEnd query string is set
            endPLPicker.setDate(moment(getQueryVariable("plEnd"), 'YYYY-MM-DD'));

        }
    }
    // END PRE-FILTERED PERF LISTS



    /*---------- ADD SOCIAL SHARING ----------*/
    $("#socialSharing").sharrre({
        share: {
            facebook: true,
            twitter: true,
            pinterest: true,
            linkedin: true
        },
    });

    /*---------- CONDITIONAL FORMS SETUP ----------*/
    $(".conditional-form").each(function(i){
        var $this = $(this),
            $startItem = $this.find(".condit-start"),
            $conditName = $this.prop("id"),
            $startBtn = $startItem.find(".condit-btn"),
            currentNum = i;

        conditForms[currentNum] = {};
        conditForms[currentNum].name = $conditName;

        $startBtn.data({"parent": $conditName, "position": 0});

        $this.data({"name": $conditName, "num": i, "position": 0, "questions": ["start"]});

        $startBtn.click(function(e){
            cfOpenNext(e);
        });
    });
});



////// EXPANDABLE MODULES (CALENDAR, SHOP CATEGORIES, ETC)
// EXPAND INNER DETAILS
function expandInnerDetails(event) {
    event.stopPropagation();
    // set up variables
    var myMod = event.data.theMod;

    // if 'day' is already selected don't do anything
    if (myMod.hasClass("selected")) {
        return;
    }

    // if nothing is already open
    if (typeof expandedModule === 'undefined' || expandedModule == 'undefined') {

        expandedModule = myMod;

        // open new one
        // calendar view
        if (myMod.hasClass("day")) {
            openDayAnimation();
        }
    // if something else is already open
    } else {
        collapseInnerDetails(event);
    }
} // end expand inner details

// COLLAPSE INNER DETAILS
function collapseInnerDetails(e){
    e.stopPropagation();

    // if nothing is queued
    if (typeof expandedModule === 'undefined' || expandedModule == 'undefined') {
        // close it all up
        // for calendar view
        if ($("#openPerfs").length > 0) {
            closeDayAnimation();
        }

    // if something else is queued
    } else {
        var myMod = e.data.theMod;

        // close it all up
        // for calendar view
        if (myMod.hasClass("day")) {
            closeDayAnimation();
        }

        // set expandedModule + new dsOffset
        expandedModule = myMod;

        // open new day after timeout
        setTimeout(function(){

            dsOffset = expandedModule.offset().top;
            TweenLite.to(theBody, 0.1, {
                scrollTop: dsOffset - 90
            });

            // open new one
            // calendar view
            if (myMod.hasClass("day")) {
                openDayAnimation();
            }
        }, 450);
    }
} // end collapse inner details

////// END EXPANDABLE MODULES




////// HORIZONTAL SCROLL FUNCTIONS
function hsScrollMe(event){

    var thePosition = event.data.thePosition,
        theFC = event.data.theFC,
        theDirection = event.data.theDirection,
        theWidth = $(theFC).outerWidth(true),
        distance;

    if (theDirection == "left") {
        // minus to the left, -=theWidth
        distance = "-=" + theWidth.toString();
    } else {
        // plus to the right, +=theWidth
        distance = "+=" + theWidth.toString();
    }

    $(horzScollers[thePosition]).find(".simplebar-scroll-content").animate({scrollLeft: distance}, 400);
}




////// SHOW/HIDE HIDDEN FUNCTION
function showIt(event){
    var myTarget = $(event.target);
    var myTargetID = myTarget.prop("id");
    if (myTargetID.indexOf("Expand") == -1) {
        myTargetID = myTarget.parents(".expander").prop("id");
    }
    var myTargetInner = myTarget.html();
    var expandID = "#" + myTargetID.replace('Expand','') + "Hidden";
    var myExpand = $(expandID);

    myExpand.show();

    if (myTarget.hasClass("more-button")){
        myTarget.animate({opacity: 0}, 100);
    }
    //myTarget.html("close");

    TweenLite.from(myExpand, 0.4, {
        height: 0,
        opacity: 0,
        ease: Circ.easeOut,
        onComplete: function() {
            myTarget.unbind("click");
            if (myTarget.hasClass("more-button")){
                myTarget.removeClass("more-button").addClass("less-button").html("close").animate({opacity: 1}, 100).click(function(e){
                    e.preventDefault();
                    closeIt(myTargetID, myTargetInner, expandID);
                });
            }
            //myTarget.unbind("click").removeClass("more-button").addClass("less-button").html("close").click(function(e){
            //    e.preventDefault();
            //    closeIt(myTargetID, myTargetInner, expandID);
            //});
        }
    });
}

function closeIt(target, copy, expand) {
    var myExpand = $(expand);
    var myTarget = $("#" + target);

    myTarget.animate({opacity: 0}, 50);
    //myTarget.html(copy);

    TweenLite.to(myExpand, 0.4, {
        height: 0,
        opacity: 0,
        ease: Circ.easeOut,
        onComplete: function(){
            myExpand.css({height: "", opacity: "", display: ""});
            //myTarget.unbind("click").removeClass("less-button").addClass("more-button").html(copy).click(function(e){
            //    e.preventDefault();
            //    showIt(e);
            //});
            myTarget.unbind("click").removeClass("less-button").addClass("more-button").html(copy).animate({opacity: 1}, 50).click(function(e){
                e.preventDefault();
                showIt(e);
            });
        }
    });
}

// END SHOW/HIDE HIDDEN FUNCTION



////// GET QUERY STRING VARIABLES
function getQueryVariable(variable) {

    if (variable == "") {
        return(false);
    }

    var query = window.location.search.substring(1);

    var vars = query.split("&");

    for (var i=0; i < vars.length; i++) {
        var pair = vars[i].split("=");

        if(pair[0] == variable){
            return pair[1];
        }
    }
    return(false);
}

function multipleQueries() {
    var query = window.location.search.substring(1);

    var vars = query.split("&");

    if (vars.length > 1) {
        return(true);
    } else {
        return(false);
    }
}



////// BREAK POINT SETTING
breakpoint.refreshValue = function () {
    this.value = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/"/g, '');
}



////// RESIZE VIDEO HEIGHT FOR 16:9 - MIGHT NEED TO ADD FOR MULTIMEDIA GALLERY
function videoResize(largeVid, smlVid) {
    var videoHoldWidth;
    var videoHoldHeight;

    if (typeof largeVid !== 'undefined' && largeVid.length > 0) {
        largeVid.each(function() {
            videoHoldWidth = $(this).width();
            videoHoldHeight = Math.round((videoHoldWidth / 16) * 9);
            $(this).find("iframe").height(videoHoldHeight);
        });
    }

    if (typeof smlVid !== 'undefined' && smlVid.length > 0) {
        smlVid.each(function() {
            videoHoldWidth = $(this).width();
            videoHoldHeight = Math.round((videoHoldWidth / 16) * 9);
            $(this).find("iframe").height(videoHoldHeight);
        });
    }

    if (typeof galleryIframe !== 'undefined' && galleryIframe.length > 0) {
        galleryIframe.each(function() {
            videoHoldWidth = $(this).width();
            videoHoldHeight = Math.round((videoHoldWidth / 16) * 9);
            $(this).height(videoHoldHeight);
        })
    }

    // homepage video
    if (typeof homeVidIframe !== 'undefined' && homeVidIframe.length > 0) {
        videoHoldHeight = homeVideoCover.height();
        videoHoldWidth = Math.round((videoHoldHeight / 9) * 16);

        if (videoHoldWidth > window.innerWidth) {
            videoHoldWidth = window.innerWidth;
            videoHoldHeight = Math.round((window.innerWidth / 16) * 9);
        } else if (!homeVideoHolder.find("#videoCover").is(":visible")) {
            videoHoldHeight = Math.min(Math.round((window.innerWidth / 1440) * 684), 684);
            videoHoldWidth = Math.round((videoHoldHeight / 9) * 16);
        }

        homeVidIframe.height(videoHoldHeight);
    }

    // homepage video - native ----- NOT BEING USED FOR NOW
    /* if (typeof homeVidNative !== 'undefined' && homeVidNative.length > 0) {
        homeVidNative.each(function() {
            videoHoldHeight = homeVideoHolder.find("#videoCover img").height();
            videoHoldWidth = Math.round((videoHoldHeight / 9) * 16);

            if (videoHoldWidth > window.innerWidth) {
                videoHoldWidth = window.innerWidth;
                videoHoldHeight = Math.round((window.innerWidth / 16) * 9);
            } else if (!homeVideoHolder.find("#videoCover").is(":visible")) {
                videoHoldHeight = Math.min(Math.round((window.innerWidth / 1440) * 684), 684);
                videoHoldWidth = Math.round((videoHoldHeight / 9) * 16);
            }

            $(this).height(videoHoldHeight);
            $(this).width(videoHoldWidth);
        });
    } */
}

/**
 * @author       Rob W <gwnRob@gmail.com>
 * @website      http://stackoverflow.com/a/7513356/938089
 * @version      20131010
 * @description  Executes function on a framed YouTube video (see website link)
 *               For a full list of possible functions, see:
 *               https://developers.google.com/youtube/js_api_reference
 * @param String frame_id The id of (the div containing) the frame
 * @param String func     Desired function to call, eg. "playVideo"
 *        (Function)      Function to call when the player is ready.
 * @param Array  args     (optional) List of arguments to pass to function func */
function callPlayer(frame_id, func, args) {
    //console.log("callPlayer called, frame_id: " + frame_id + ", func: " + func + ", args: " + args);

    if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;

    var iframe = document.getElementById(frame_id);

    if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }

    // When the player is not ready yet, add the event to a queue
    // Each frame_id is associated with an own queue.
    // Each queue has three possible states:
    //  undefined = uninitialised / array = queue / 0 = ready
    if (!callPlayer.queue) callPlayer.queue = {};
    var queue = callPlayer.queue[frame_id],
        domReady = document.readyState == 'complete';

    if (domReady && !iframe) {
        // DOM is ready and iframe does not exist. Log a message
        window.console && console.log('callPlayer: Frame not found; id=' + frame_id);
        if (queue) clearInterval(queue.poller);
    } else if (func === 'listening') {

        console.log("listening buzz");

        // Sending the "listener" message to the frame, to request status updates
        if (iframe && iframe.contentWindow) {
            func = '{"event":"listening","id":' + JSON.stringify(''+frame_id) + '}';
            iframe.contentWindow.postMessage(func, '*');
        }
    } else if (!domReady ||
               iframe && (!iframe.contentWindow || queue && !queue.ready) ||
               (!queue || !queue.ready) && typeof func === 'function') {

        console.log("I'm going to set off that listening buzz");

        if (!queue) queue = callPlayer.queue[frame_id] = [];

        queue.push([func, args]);

        if (!('poller' in queue)) {

            // keep polling until the document and frame is ready
            queue.poller = setInterval(function() {
                callPlayer(frame_id, 'listening');
            }, 250);

            // Add a global "message" event listener, to catch status updates:
            messageEvent(1, function runOnceReady(e) {

                console.log("e= " + e.data);

                if (!iframe) {
                    console.log("no iframe");
                    iframe = document.getElementById(frame_id);
                    if (!iframe) return;
                    if (iframe.tagName.toUpperCase() != 'IFRAME') {
                        iframe = iframe.getElementsByTagName('iframe')[0];
                        if (!iframe) return;
                    }
                }
                if (e.source === iframe.contentWindow) {
                    // Assume that the player is ready if we receive a
                    // message from the iframe
                    clearInterval(queue.poller);
                    queue.ready = true;
                    messageEvent(0, runOnceReady);
                    // .. and release the queue:
                    while (tmp = queue.shift()) {
                        callPlayer(frame_id, tmp[0], tmp[1]);
                    }
                    // console.log("listening message received");
                }
            }, false);
        }
    } else if (iframe && iframe.contentWindow) {
        // When a function is supplied, just call it (like "onYouTubePlayerReady")
        if (func.call) return func();
        // Frame exists, send message
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
    /* IE8 does not support addEventListener... */
    function messageEvent(add, listener) {
        //console.log("add: " + add + ", listener: " + listener);

        var w3 = add ? window.addEventListener : window.removeEventListener;
        w3 ?
            w3('message', listener, !1)
        :
            (add ? window.attachEvent : window.detachEvent)('onmessage', listener);

        console.log(w3);
    }
}

// set up YouTube api for statechange etc
function getFrameID(id){
    var elem = document.getElementById(id);
    if (elem) {
        if(/^iframe$/i.test(elem.tagName)) return id; //Frame, OK
        // else: Look for frame
        var elems = elem.getElementsByTagName("iframe");
        if (!elems.length) return null; //No iframe found, FAILURE
        for (var i=0; i<elems.length; i++) {
           if (/^https?:\/\/(?:www\.)?youtube(?:-nocookie)?\.com(\/|$)/i.test(elems[i].src)) break;
        }
        elem = elems[i]; //The only, or the best iFrame
        if (elem.id) return elem.id; //Existing ID, return it
        // else: Create a new ID
        do { //Keep postfixing `-frame` until the ID is unique
            id += "-frame";
        } while (document.getElementById(id));
        elem.id = id;
        return id;
    }
    // If no element, return null.
    return null;
}

// Define YT_ready function.
var YT_ready = (function() {
    var onReady_funcs = [], api_isReady = false;
    /* @param func function     Function to execute on ready
     * @param func Boolean      If true, all qeued functions are executed
     * @param b_before Boolean  If true, the func will added to the first
                                 position in the queue*/
    return function(func, b_before) {
        if (func === true) {
            api_isReady = true;
            while (onReady_funcs.length) {
                // Removes the first func from the array, and execute func
                onReady_funcs.shift()();
            }
        } else if (typeof func == "function") {
            if (api_isReady) func();
            else onReady_funcs[b_before?"unshift":"push"](func);
        }
    }
})();
// This function will be called when the API is fully loaded
function onYouTubePlayerAPIReady() {YT_ready(true)}

// Load YouTube Frame API
(function() { // Closure, to not leak to the scope
  var s = document.createElement("script");
  s.src = (location.protocol == 'https:' ? 'https' : 'http') + "://www.youtube.com/player_api";
  var before = document.getElementsByTagName("script")[0];
  before.parentNode.insertBefore(s, before);
})();




////// TOUCH DEVICE TESTING
function isTouchDevice() {
    return true == (("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}




////// ARRAY ADDING
function arrayAdd(a, b) {
    return a + b;
}


////// INFO POPUP CLOSE
function closeInfo(pNumb) {
    var thePopup = $(infoPopUp[pNumb]),
        myPos = parseInt(thePopup.css("bottom"));

    thePopup.removeClass("here");

    // animate the popup off the stage
    TweenLite.to(thePopup, 0.4, {
        bottom: -30,
        opacity: 0,
        ease: Circ.easeIn,
        onComplete: function() {
            thePopup.css("display","none");
        }
    });

    // set up while statement
    var pNumbIt = pNumb + 1;

    // move the next one in the array down
    while (pNumbIt < infoPopUp.length) {
        if ($(infoPopUp[pNumbIt]).hasClass("here")){

            setTimeout(function(){
                moveInfoPop(pNumbIt, myPos);
            }, 200);
            break;
        }
        pNumbIt++;
    }

    //if (infoPopUp[pNumb].id == "cookieBar") {
    //    document.cookie = 'Cookies_accepted=true;path=/';
    //}

    "compatibilityBar" == infoPopUp[pNumb].id && (sessionStorage.compatibilityMessage = true);
    "cookieBar" == infoPopUp[pNumb].id && (cookieDate = new Date(Date.now())) && (cookieDate.setMonth(cookieDate.getMonth() + 1)) && (document.cookie = "cookiesAccepted=true;expires=" + cookieDate.toGMTString() + ";path=/");
}

function moveInfoPop(pNumb, newPos) {
    var myPos = $(infoPopUp[pNumb]).height() + 8 + newPos;

    TweenLite.to($(infoPopUp[pNumb]), 0.4, {
        bottom: newPos,
        ease: Circ.easeInOut,
        onComplete: function() {
            if (pNumb < infoPopUp.length - 1) {
                moveInfoPop(pNumb + 1, myPos);
            }
        }
    });
}


function findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}



////// CONDITIONAL FORMS
// open next item
function cfOpenNext(event, formItem) {

    var myTarget = $(event.currentTarget),
        nextItem = $("#" + myTarget[0].getAttribute("data-next")),
        nextBtns = nextItem.find(".condit-btn"),
        bkBtn = nextItem.find(".condit-bk-btn"),
        theForm, thePos, theNextPos, theFormPos, formEnd = false;

    // ****** do form validation here ******

    // on start when no form item has been passed through function,
    // find out from startBtns data-parent attribute.
    // In all other cases it should in a parameter in the function
    if (!formItem) {
        theForm = $("#" + myTarget.data("parent"));
    } else {
        theForm = formItem;
    }

    // add questions to array in form.data
    theForm.data("questions").push(event.target.getAttribute("data-next"));

    // set the positions for reference
    thePos = myTarget.data("position"), theFormPos = theForm.data("position"), theNextPos = thePos + 1;

    // in case user has changed a previous question
    if (thePos <= theForm.data("position") && thePos !== 0) {
        console.log("we've gone backwards");
        // set up destroys
    }

    // set the position of the form
    theForm.data("position", thePos);

    // add the functions to the next round, or set up the submit button
    if (nextBtns.length > -1) {
        nextBtns.each(function(i){
            var $this = $(this);

            $this.data("position", theNextPos).click(function(e){
                cfOpenNext(e, theForm);
            });
        });
    }

    if (nextItem.hasClass("final")){
        formEnd = true;
    }

    if (nextItem.hasClass("title-select")) {
        nextItem.find("#sbTitle").html(myTarget[0].value);
    }

    // set up the back button
    if (bkBtn.length > -1) {
        bkBtn.click(function(e){
            cfBackUp(e, theForm);
        });
    }

    // open up the next/current item. all animations need to be added in here.
    cfAnimate(nextItem, formEnd);
}

function cfAnimate(theItem, end) {
    var nxtItem = $(theItem),
        origItemHeight, newItemHeight;

    nxtItem.addClass("open");
    newItemHeight = nxtItem.height() > (windowHeight - globalHeadertop.outerHeight()) ? nxtItem.height() : windowHeight - globalHeadertop.outerHeight();
    nxtItem.css({ height: 0, opacity: 0 });

    TweenLite.to(nxtItem, 0.3, {
        height: newItemHeight
    });

    TweenLite.to(nxtItem, 0.4, {
        opacity: 1
    }).delay(0.8);

    gimmeGo(nxtItem);

    if (end){
        nxtItem.siblings(".submit").addClass("open");
    }
}

// back function
function cfBackUp(event, formItem) {
    event.preventDefault();
}

// destroy/reset items after back button + new item used
function cfDestroyPrev(items) {
    //
}
