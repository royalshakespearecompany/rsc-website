////// OPEN WHAT'S ON QUESTION
function openQuestion(activeNumber) {

    questionHolder.addClass("active");

    // close up previous question
    closeQuestion();

    // find new question to open
    var currentQ = $(woBtns[activeNumber]),
        theFilters = currentQ.find(".filters");

    currentQ.addClass("active");

    if (screenWidth < mediumScreen) {
        theFilters.css({
            height: "",
            opacity: "",
            transform: ""
        });
        TweenLite.from(theFilters, 0.6, {
            height: 0,
            opacity: 0,
            onComplete: function() {
                theFilters.css({
                    height: "",
                    opacity: "",
                    transform: ""
                });

                $('html, body').animate({
                    scrollTop: currentQ.offset().top - 90
                }, 500);
            }
        });

    }

    woActiveQ = activeNumber;
}




////// CLOSE QUESTION
function closeQuestion() {
    var myQuestion = $(".wo-q-item.active"),
        activeNumber,
        mySelections = myQuestion.find(".selection .selected"),
        selIndicator = myQuestion.find(".wo-showing"),
        currentFilters = myQuestion.find(".filters");

    $(".wo-q-item").each(function(i) {
        if ($(this).hasClass("active")) {
            activeNumber = i;
        }
    });

    if (mySelections.length > 0) {
        selIndicator.html("");
        mySelections.each(function(i) {
            var theChoice = "<span class='choice'>" + $(this).siblings("label").html() + "</span>";
            selIndicator.append(theChoice);
        });
        selIndicator.addClass("chosen");

    } else if (typeof startDateSel !== "undefined" && myQuestion.hasClass("wo-when")) {

        selIndicator.html("<span class='choice'>" + startDateSel + " - " + endDateSel + "</span>").addClass("chosen");

    } else {
        selIndicator.html(woShowingDefault[activeNumber]).removeClass("chosen");
    }

    if (screenWidth < mediumScreen) {

        TweenLite.to(currentFilters, 0.3, {
            height: 0,
            opacity: 0,
            onComplete: function() {
                myQuestion.removeClass("active");
            }
        });
    }

    if (woSearchOpen.hasClass("active")) {
        woSearchOpen.removeClass("active");
        var theBox = woSearchOpen.find(".form-holder");
        TweenLite.to(theBox, 0.4, {
            height: 0,
            "margin-top": 0,
            "margin-bottom": 0,
            opacity: 0,
            onComplete: function() {
                theBox.css({ height: "" });
            }
        });
    }
}




////// GO BUTTON
function gimmeGo(theThing) {

    //console.log($theThing);
    //var myId = $theThing.prop("id");

    //console.log("gimme go fired for: " + myId);
    //console.log("top of " + myId + " is " + myTopPos);
    //console.log("window is scrolled: " + $(window).scrollTop());

    if (theThing == 1) {
        thePage.stop().scrollTo($(".wo-results"), { duration: 1000, offsetTop: headerHeight });
    } else if (theThing == 2) {
        thePage.stop().scrollTo($("#SelectedLevel"), { duration: 1000, offsetTop: headerHeight });
    } else {
        var $theThing = $(theThing),
            myTopPos = $theThing.offset().top;

        $("body, html").stop().animate({
            scrollTop: myTopPos - headerHeight
        }, 1000);
    }
}



$.fn.scrollTo = function(target, options, callback) {
    if (typeof options == 'function' && arguments.length == 2) {
        callback = options;
        options = target;
    }
    var settings = $.extend({
        scrollTarget: target,
        offsetTop: 50,
        duration: 500,
        easing: 'swing'
    }, options);
    return this.each(function() {
        var scrollPane = $(this);
        var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
        var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
        scrollPane.animate({ scrollTop: scrollY }, parseInt(settings.duration), settings.easing, function() {
            if (typeof callback == 'function') { callback.call(this); }
        });
    });
}




////// GRID ITEMS CLICK FUNCTION
function clickTheGrid(event) {

    // clear out any previous whats on focus
    if (typeof gridFocusOuter !== 'undefined') {
        gridFocusOuter.empty().removeClass("active");
    }

    // get Grid Item + pos etc
    var thingy = event.data.theGridItem,
        thingyPos = thingy.position(),
        thingyTop = thingyPos.top,
        thingyLeft = thingyPos.left,
        thingyWidth = thingy.width(),
        thingyHeight = thingy.height();

    woActiveItem = thingy;
    gridFocusOuter = thingy.siblings(".active-gi-holder").addClass("active");
    TweenLite.to(gridFocusOuter, 0.5, { opacity: 1 });
    gridFocusOuterHeight = gridFocusOuter.height();


    // CLONE GRID ITEM INTO GRID FOCUS OUTER
    var clonedGI = thingy.clone().addClass("active").appendTo(gridFocusOuter).css({ "min-height": thingyHeight }),
        clonedGIHeight = clonedGI.height(),
        clonedGIWidth = clonedGI.width(),
        clonedGIMainImg = clonedGI.find(".main-image-holder"),
        clonedTitleCopy = clonedGI.find(".title-copy"),
        thingTitleCopyheight = thingy.find(".title-copy").height(),
        thingTitleBkgrndMarg = thingy.find(".gi-title-bkgrnd").css("margin-top"),
        clonedTitleBkgrnd = clonedGI.find(".gi-title-bkgrnd").css({ "margin-top": thingTitleBkgrndMarg }),
        clonedDetails = clonedGI.find(".gi-details");

    clonedGI.css({ height: thingyHeight, width: thingyWidth });

    // CHECK POSITION ACCORDING TO SCREENWIDTH
    // smallmedium width to large, if second in row
    if ((screenWidth >= smadiumScreen && screenWidth < largeScreen) && thingyLeft > 0) {
        clonedGI.css({ right: 0 });

        TweenLite.to(clonedGIMainImg, 0.4, {
            delay: 0.3,
            height: "147px",
            opacity: 0
        });
        TweenLite.from(clonedTitleCopy, 0.4, {
            delay: 0.3,
            height: thingTitleCopyheight
        });
        TweenLite.to(clonedTitleBkgrnd, 0.4, {
            delay: 0.3,
            "margin-top": "16px"
        });
        TweenLite.to(clonedGI, 0.4, {
            delay: 0.3,
            height: clonedGIHeight,
            onComplete: function() {
                clonedDetails.css({ display: "block" });
            }
        });
        TweenLite.to(clonedGI, 0.4, {
            delay: 0.7,
            width: clonedGIWidth,
            onComplete: function() {
                clonedGI.css({ height: "auto", width: "" });
            }
        });
        TweenLite.to(clonedDetails, 0.4, {
            delay: 1,
            opacity: 1
        });

        // larger screens + last in row
    } else if (screenWidth >= largeScreen && thingyLeft > (gridFocusOuter.width() / 2)) {
        clonedGI.css({ right: 0 });

        TweenLite.to(clonedGIMainImg, 0.4, {
            delay: 0.3,
            height: "147px",
            opacity: 0
        });
        TweenLite.from(clonedTitleCopy, 0.4, {
            delay: 0.3,
            height: thingTitleCopyheight
        });
        TweenLite.to(clonedTitleBkgrnd, 0.4, {
            delay: 0.3,
            "margin-top": "16px"
        });
        TweenLite.to(clonedGI, 0.4, {
            delay: 0.3,
            height: clonedGIHeight,
            onComplete: function() {
                clonedDetails.css({ display: "block" });
            }
        });
        TweenLite.to(clonedGI, 0.4, {
            delay: 0.7,
            width: clonedGIWidth,
            onComplete: function() {
                clonedGI.css({ height: "auto", width: "" });
            }
        });
        TweenLite.to(clonedDetails, 0.4, {
            delay: 1,
            opacity: 1
        });

        // small sizes, single column & everything else
    } else {
        clonedGI.css({ left: thingyLeft });

        TweenLite.to(clonedGIMainImg, 0.4, {
            delay: 0.3,
            height: "147px",
            opacity: 0
        });
        TweenLite.from(clonedTitleCopy, 0.4, {
            delay: 0.3,
            height: thingTitleCopyheight
        });
        TweenLite.to(clonedTitleBkgrnd, 0.4, {
            delay: 0.3,
            "margin-top": "16px"
        });
        TweenLite.to(clonedGI, 0.4, {
            delay: 0.3,
            height: clonedGIHeight,
            onComplete: function() {
                clonedDetails.css({ display: "block" });
            }
        });
        TweenLite.to(clonedGI, 0.4, {
            delay: 0.7,
            width: clonedGIWidth,
            onComplete: function() {
                clonedGI.css({ height: "auto", width: "" });
            }
        });
        TweenLite.to(clonedDetails, 0.4, {
            delay: 1,
            opacity: 1
        });
    }

    // for when the grid item is taller than the background 
    if (gridFocusOuterHeight < clonedGIHeight) {
        //console.log("true");
        TweenLite.to(gridFocusOuter, 0.4, {
            delay: 0.3,
            height: clonedGIHeight
        });
    }

    // CHECK POSITION ACCORDING TO DISTANCE FROM BOTTOM

    // if it's at the bottom and there is more than one line
    if ((thingyHeight + thingyTop > gridFocusOuterHeight - 10) && thingyTop > 10) {
        clonedGI.css({ bottom: 0 });
    } else { // any other time
        clonedGI.css({ top: thingyTop });
    }


    // CLEAR ACTIVE ITEM, VIA ACTIVE-GI-HOLDER & LESS BUTTON
    // stop propagation from cloned grid item
    clonedGI.click(function(e) {
        e.stopPropagation();
    });

    gridFocusOuter.click(function(e) {
        clearGrid();
    });

    var lessButton = gridFocusOuter.find(".less-button");

    lessButton.click(function(e) {
        e.preventDefault();
        clearGrid();
    });

    function clearGrid() {
        TweenLite.to(clonedDetails, 0.2, {
            opacity: 0
        });
        clonedGI.css({ "overflow": "hidden", "min-height": "none" });
        TweenLite.to(clonedGI, 0.3, {
            delay: 0.15,
            height: 0,
            width: 0,
            opacity: 0
        });

        // reset gridouter background
        TweenLite.to(gridFocusOuter, 0.3, {
            delay: 0.35,
            opacity: 0,
            height: gridFocusOuterHeight,
            onComplete: function() {
                gridFocusOuter.empty().removeClass("active").css({ height: "" });
            }
        });
    }
}




////// UPDATE WHAT'S ON LINK
function updateLink() {
    var newLink = "/whats-on/filter/",
        whatLink, whereLink, startLink, endLink, viewLink;

    // set up what
    if (selectedChoicesObj.what.length > 0) {
        whatLink = selectedChoicesObj.what.join("-");
    } else {
        whatLink = "any";
    }

    // set up where
    if (selectedChoicesObj.where.length > 0) {
        whereLink = selectedChoicesObj.where.join("-");
    } else {
        whereLink = "any";
    }

    // set up when
    startLink = selectedChoicesObj.when[0];
    endLink = selectedChoicesObj.when[1];
    if (startLink == null) startLink = "any";
    if (endLink == null) endLink = "any";

    // set up view
    if (startLink != "any" && endLink != "any") {
        viewLink = "calendar";
    } else {
        viewLink = selectedChoicesObj.view != undefined && selectedChoicesObj.view.length > 0 ? selectedChoicesObj.view : "grid";
    }

    // update the link
    newLink += "what/" + whatLink + "/where/" + whereLink + "/when/" + startLink + "/" + endLink + "/view/" + viewLink;
    goButtons.attr("href", newLink);

}




////// SHOW GRID/CALENDAR VIEW
function ShowView(a) {
    var b, c;
    b = a.replace("wo-", "");

    switch (b) {
        case "grid-view":
            c = "grid";
            break;
        case "calendar-view":
            c = "calendar";
            break;
    }

    selectedChoicesObj.view = c;
    updateLink();

    $("#" + b).siblings(".view").fadeOut("slow", function() {
        $("#" + b).fadeIn("slow");

        $("#" + a).siblings(".view-btn").removeClass("selected");
        $("#" + a).addClass("selected");

        gimmeGo($("#" + b));
    });
}




////// CALENDAR FUNCTIONS
// OPEN DAY ANIMATION
function openDayAnimation() {
    // set the vars up
    var perfSum = expandedModule.find(".perf-sum"),
        perfSumHeight = perfSum.height(),
        myPerfs = expandedModule.find(".ex-mod-inner"),
        myWeek = expandedModule.parent(".week"),
        activePerfs, closeDay, allPerfsCloseDay;

    // check screenwidth, clone and append to 'week' if wider than 790px
    if (screenWidth >= mediumScreen) {
        activePerfs = myPerfs.clone().appendTo(myWeek).prop("id", "openPerfs").addClass("cloned");
        console.log("cloning");
    } else {
        activePerfs = myPerfs.prop("id", "openPerfs");
    }

    // add 'selected' class to 'day', then remove perfs summary
    expandedModule.addClass("selected");
    expandedModule.attr("id", "selectedDay");
    // set up the close button
    closeDay = expandedModule.find(".more-button + .less-button");
    allPerfsCloseDay = activePerfs.find(".less-button");

    TweenLite.from(perfSum, 0.2, {
        height: perfSumHeight,
        opacity: 1,
        onComplete: function() {
            perfSum.css({ opacity: "", height: "" });
        }
    });

    // add active perfs to stage, then animate to full height
    ////// USE TIMELINE LIGHT TO ADD EACH PERF ONTO STAGE ONE AFTER ANOTHER, SHOULD BE ABLE TO DO THIS PROGRAMATICALLY
    activePerfs.show();
    TweenLite.from(activePerfs, 1, {
        height: 0,
        opacity: 0,
        ease: Power2.easeInOut,
        onComplete: function() {
            activePerfs.css({ height: "", opacity: "" });

            // CLOSE DAY FUNCTION
            closeDay.click(function(e) {
                dsOffset = expandedModule.offset().top;
                expandedModule = 'undefined';
                collapseInnerDetails(e);
            });
            // CLOSE DAY FUNCTION
            allPerfsCloseDay.click(function(e) {
                dsOffset = expandedModule.offset().top;
                expandedModule = 'undefined';
                collapseInnerDetails(e);
            });
        }
    });
}

// CLOSE DAY ANIMATION
function closeDayAnimation() {

    var openPerfs = $("#openPerfs"),
        openPerfsHeight = openPerfs.height(),
        openDay = $("#selectedDay"),
        openPerfSum = $("#selectedDay .perf-sum");

    openPerfs.attr("id", "");
    TweenLite.to(openPerfs, 0.4, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            openPerfs.hide();

            // on bigger screens remove the cloned one
            if (screenWidth >= mediumScreen) {
                openPerfs.remove();
            } else {
                openPerfs.css({ height: "", opacity: "" });
            }
        }
    });

    openDay.attr("id", "").removeClass("selected");

    TweenLite.from(openPerfSum, 0.4, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            openPerfSum.css({ opacity: "", height: "" });
        }
    });

    if (typeof expandedModule === 'undefined' || expandedModule == 'undefined') {
        // reset window position;
        TweenLite.to(theBody, 0.1, {
            scrollTop: dsOffset - 90
        });
    }
}