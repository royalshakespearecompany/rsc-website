(function($, window){

  var SELECTORS = {
      AUDIOGALLERY: '.audio-gallery',
      AUDIOPLAYER: '.js-audio-player',
      PLAYLIST: '.js-playlist-item',
      PLAYLISTCONTAINER: '.js-audio-gallery__playlist',
      VOLUME: '.js-volume-bar',
      MUTE: '.js-mute-control',
      PLAYBUTTON: '.js-playPauseAudio',
      PREVBUTTON: '.js-prevAudio',
      NEXTBUTTON: '.js-nextAudio',
      COUNTPLAYBACKELEMENT: '.js-countPlaybackElement',
      PROGRESSBAR: '.js-audio-gallery__progress',
      PROGRESSTRACK: '.js-update-progress',
      DOWNLOAD: '.js-download'
  };

  var CSS_CLASSES = {
      ACTIVE_ITEM: 'is-active',
      PAUSED: 'is-paused',
      PLAYING: 'is-playing'
  };

  var CSS_VOLUME_CLASSES = ['mute', 'volume1', 'volume2', 'volume3'];

  var audio = {
      player : undefined,
      items : undefined,
      playlist: undefined,
      itemDuration: undefined
  };

  var controls = {
      playPauseBtn : undefined,
      nextBtn : undefined,
      prevBtn : undefined,
      mute: undefined
  };

  var state = {
      playing: false,
      currentPosition: 0,
      totalLength: 0,
      muted: false,
      currentVolume: 0.5
  };

  var progressBar = {
      container: undefined,
      track: undefined,
      trackWidth: 0,
      increment: 0
  };

  var countPlaybackInterval;

  if ( $(SELECTORS.AUDIOPLAYER).length) {
    init();
    return false;
  }


  function init() {
      var playerEl = document.querySelector(SELECTORS.AUDIOPLAYER);
      var galleryEl = document.querySelector(SELECTORS.AUDIOGALLERY);

      if (!playerEl) {
          return false;
      }

      audio.player = playerEl;
      audio.playlist = $(SELECTORS.PLAYLIST);
      audio.playlistContainer = $(SELECTORS.PLAYLISTCONTAINER);
      audio.countPlaybackElement = $(SELECTORS.COUNTPLAYBACKELEMENT);
      audio.items = audio.playlist.map(function(i,item) {
          return $(item).data('src');
      });

      controls.playPauseBtn = document.querySelector(SELECTORS.PLAYBUTTON);
      controls.prevBtn = document.querySelector(SELECTORS.PREVBUTTON);
      controls.nextBtn = document.querySelector(SELECTORS.NEXTBUTTON);
      controls.volume = document.querySelector(SELECTORS.VOLUME);
      controls.mute = document.querySelector(SELECTORS.MUTE);

      progressBar.container = $(SELECTORS.PROGRESSBAR);
      progressBar.track = $(SELECTORS.PROGRESSTRACK);

      state.totalLength = audio.items.length;

      if (state.totalLength === 1) {
        $(audio.playlistContainer).addClass('single');
        $(controls.prevBtn).remove();
        $(controls.nextBtn).remove();
      }


      audio.player.volume = 0.5;

      loadAudio();
      resetCountPlayback();
      setEventHandlers();
      $(galleryEl).removeClass('hidden');
  }

  var dragging = false;

  function setEventHandlers() {

      audio.player.addEventListener('canplaythrough', function(e) {
          e.preventDefault();

          console.log(e.currentTarget.duration);
          audio.itemDuration = e.currentTarget.duration;
          $('.js-countTotalTimeElement').html(formatCountPlayback(audio.itemDuration))
      });

      audio.player.addEventListener('ended', function(e) {
          e.preventDefault();
          resetCountPlayback();
          nextAudio();
          // So it doesn't loop
          if (state.currentPosition) {
              playPauseAudio();
          }
      });

      $(progressBar.container).on('mousedown', function(e) {
          e.preventDefault();
          dragging = true;
      });

      $(progressBar.container).on('mousemove', function(e) {
          e.preventDefault();
          if (dragging) {
              audio.player.currentTime = setTrackPosition(e.offsetX);
              setTrackProgress();
          }
      });

      $('.js-download').each(function(i, val){
        $(val).on('click', function(e){
            e.preventDefault();
            var closest = $(val).closest('.js-playlist-item');
            var data = closest.data('src');
            var filenameParts = data.split('/');
            SaveToDisk(data, filenameParts[filenameParts.length-1]);
        });
      });

      $(progressBar.container).on('mouseup', function(e) {
          e.preventDefault();
          audio.player.currentTime = setTrackPosition(e.offsetX);
          setTrackProgress();
          dragging = false;
      });

      $(controls.playPauseBtn).on('click', function(e) {
          e.preventDefault();
          playPauseAudio();
      });

      $(controls.prevBtn).on('click', function(e) {
          e.preventDefault();
          prevAudio();
          // playPauseAudio();
      });

      $(controls.nextBtn).on('click', function(e) {
          e.preventDefault();
          nextAudio();
          // playPauseAudio();
      });

      $(controls.volume).on('input', function(e) {
          e.preventDefault();
          state.muted = false;
          var volume = $(this).val();
          audio.player.volume = volume;
          state.currentVolume = volume;
          setVolumeCSSClass();
      });

      $(controls.volume).on('change', function(e) {
          e.preventDefault();
          setVolumeCSSClass();
      });

      $(controls.mute).on('click', function(e) {
          e.preventDefault();
          muteAudio();
      });


      audio.playlist.each(function(i, element) {
          $(element).on('click', function(e) {
              e.stopPropagation();
              state.currentPosition = i;
              loadAudio();
          });
      });

  }

  function startCountPlayback() {
      countPlaybackInterval = window.setInterval( function() {
          audio.countPlaybackElement.html(formatCountPlayback(audio.player.currentTime));
          setTrackProgress();
      }, 10);
  }

  function setVolumeCSSClass() {
      CSS_VOLUME_CLASSES.forEach(function(cssClass) {
          $(controls.mute).removeClass(cssClass);
      });

      if (state.currentVolume <= 0) {
          $(controls.mute).addClass(CSS_VOLUME_CLASSES[0]);
      } else if (state.currentVolume <= 0.3) {
          $(controls.mute).addClass(CSS_VOLUME_CLASSES[1]);
      } else if (state.currentVolume <= 0.6) {
          $(controls.mute).addClass(CSS_VOLUME_CLASSES[2]);
      } else {
          $(controls.mute).addClass(CSS_VOLUME_CLASSES[3]);
      }
  }

  function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var evt = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': false
        });
        save.dispatchEvent(evt);

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }

    // for IE < 11
    else if ( !! window.ActiveXObject && document.execCommand)     {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}

  function stopCountPlayback() {
      window.clearInterval(countPlaybackInterval);
  }

  function formatCountPlayback(time) {
      var date = new Date(null);
      date.setSeconds(time); // specify value for SECONDS here
      // TODO: Do we need to use moment here?
      return moment.parseZone(date).format('mm:ss');
  }

  function resetCountPlayback() {
      stopCountPlayback();
      audio.countPlaybackElement.html(formatCountPlayback(0));
  }

  function nextAudio() {
      var nextPos = ++state.currentPosition;
      if (nextPos >= state.totalLength) {
          state.currentPosition = 0;
      } else {
          state.currentPosition = nextPos;
      }
      loadAudio();
  }

  function prevAudio() {
      if (audio.player.currentTime > 3) {
          audio.player.currentTime = 0;
          stopAudio();
          audio.countPlaybackElement.html(formatCountPlayback(audio.player.currentTime));
          setTrackProgress();
          return false;
      }

      var prevPos = --state.currentPosition;
      if (prevPos < 0) {
          state.currentPosition = state.totalLength - 1;
      } else {
          state.currentPosition = prevPos;
      }
      loadAudio();
  }

  function playPauseAudio() {
      if (!state.playing) {
          $(controls.playPauseBtn).addClass(CSS_CLASSES.PLAYING);
          audio.player.play();
          startCountPlayback();
      } else {
          $(controls.playPauseBtn).removeClass(CSS_CLASSES.PLAYING);
          audio.player.pause();
          stopCountPlayback();
      }
      state.playing = !state.playing;
  }

  function stopAudio() {
      state.playing = false;
      audio.player.pause();
  }

  function loadAudio() {
      audio.player.pause();
      state.playing = false;
      $(controls.playPauseBtn).removeClass(CSS_CLASSES.PLAYING);
      audio.player.src = audio.items[state.currentPosition];
      resetCountPlayback();
      hightlightCurrentPlaylistItem();
      setTrackProgress();
  }

  function muteAudio() {
      if (!state.muted) {
          audio.player.volume = 0;
          state.currentVolume = 0;
          $(controls.volume).val(0);
      } else {
          audio.player.volume = state.currentVolume;
      }
      setVolumeCSSClass();
      state.muted = !state.muted;
  }

  function hightlightCurrentPlaylistItem() {
      audio.playlist.each(function(i, element) {
          $(element).removeClass(CSS_CLASSES.ACTIVE_ITEM);
      });
      $(audio.playlist[state.currentPosition]).addClass(CSS_CLASSES.ACTIVE_ITEM);
  }

  function setTrackProgress() {
      progressBar.track.width(getTrackPercentage() + '%');
  }

  function setTrackPosition(xPos) {
      var percentage = ( xPos / $(progressBar.container).width() ) * 100;
      return parseInt((percentage / 100 ) * audio.itemDuration, 10);
  }

  function getTrackPercentage() {
      var percentage = (audio.player.currentTime / audio.itemDuration) * 100;
      return percentage;
  }

})(jQuery, window)
