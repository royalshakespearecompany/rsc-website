(function(window, $, undefined) {

    var SELECTOR = '.js-gallery';
    var $elems = $(SELECTOR);
    var canrun = ($elems.length) ? true : false;

    var pswpElement;

    var galleries = [];
    var hashData = {};

    if (canrun) {
        init();
    }

    function init() {
        pswpElement = document.querySelectorAll('.pswp')[0];
        $elems.each(function(i, elem) {

            if ($(elem).closest('.cast-creative-gallery-widget').length > 0) {
                $(elem).find('.launch-all').remove();
            }

            $(this).attr({ 'data-pswp-uid': i + 1 });
            galleries.push({
                uid: 'data-pswp-uid__' + (i + 1),
                gallery: this,
                config: parseThumbnailElements(this),
                title: $(this).find('.gallery-title').html()
            })
        });
        addEventHandlers();
        hashData = photoswipeParseHash();
        if (hashData.pid && hashData.gid) {
            initializeGallery(galleries[hashData.gid - 1].gallery, parseInt(hashData.pid));
        }

    }

    function photoswipeParseHash() {
        var hash = window.location.hash.substring(1),
            params = {};

        if (hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if (params.gid) {
            params.gid = parseInt(params.gid, 10);
        }
        return params;
    };

    function initializeGallery(galleryElement, idx) {
        var index = idx || 0;
        var disableAnimation = false;
        var galleryTitle = $(galleryElement).find('.gallery-title').html();
        var config = parseThumbnailElements(galleryElement);
        var showCaptionEl = config.showCaptionEl;
        var items = config.items;

        var options = {
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),
            galleryTitle: galleryTitle,
            history: true,
            galleryPIDs: true,
            closeOnVerticalDrag: false,
            captionEl: showCaptionEl,
            index: index,
            closeOnScroll: false,
            showAnimationDuration: 40,
            timeToIdle: 0,
            getThumbBoundsFn: function(index) {
                if (items.length === 0 || !items[index].el) {
                    return { x: 0, y: 0, w: 0 };
                }
                var thumbnail = $(items[index].el).closest('.js-gallery-item')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();
                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            }
        }
        launchGallery(items, options);
    }

    function addEventHandlers() {
        galleries.forEach(function(galleyObj) {
            if (galleyObj.config.createGallery) {
                var slides = $(galleyObj.gallery).find('.js-gallery-item');
                var launchButton = $(galleyObj.gallery).find('.launch-all');
                $(slides).each(function(i) {
                    $(this).on('click', function(e) {
                        e.preventDefault();
                        initializeGallery(galleyObj.gallery, i);
                    })
                });

                $(launchButton).on('click', function(e) {
                    e.preventDefault();
                    initializeGallery(galleyObj.gallery, 0);
                })
            }
        })
    }

    function launchGallery(items, options) {

        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_RSC, items, options);
        gallery.init();

        gallery.listen('initialZoomInEnd', addExtraContent);
        gallery.listen('afterChange', addExtraContent);
        gallery.listen('close', function() {
            $('.pswp__video-container').find('iframe').attr('src', '');
        });
    }

    function overrideMobile() {
        if (gallery.framework.features.touch) {
            $('.header-container').css({ 'display': 'none' });
            $('.main-container').css({ 'display': 'none' });
            $('.footer-container').css({ 'display': 'none' });
        }
        window.setTimeout(function() {
            $('.mfp-ready').css({ 'top': 0 });
            $('body').addClass('mfp-open');
            var height = $('.mfp-content').outerHeight();
            var windowHeight = $(window).height();
            if (windowHeight > height) {
                height = windowHeight;
            }
            $('.mfp-bg').css({ height: height + 'px' });
        }, 10);
    }

    function resetMobile() {
        $('.header-container').css({ 'display': 'block' });
        $('.main-container').css({ 'display': 'block' });
        $('.footer-container').css({ 'display': 'block' });
        $('body').removeClass('mfp-open');
        if (gallery.framework.features.touch) {
            gallery.updateSize(true);
        }
    }

    function addExtraContent() {

        var captionCenter = $('#captionCenter');
        var mediaHolder = $('body');
        var extraInfoButton = $(gallery.template).find('.js-extrainfo-button');

        if (captionCenter.height() > 150) {
            mediaHolder.addClass('overflow');
            extraInfoButton.removeClass('hidden');
            extraInfoButton.off('click').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $.magnificPopup.open({
                    items: {
                        src: '<div class="gallery__extra-popup">' + gallery.currItem.title + '</div>',
                        type: 'inline'
                    },
                    alignTop: true,
                    callbacks: {
                        open: function() {
                            overrideMobile();
                        },
                        close: function() {
                            resetMobile();
                        },
                        resize: function() {
                            overrideMobile();
                            // resize event triggers only when height is changed or layout forced
                        }
                    }
                });
            });

        } else {
            mediaHolder.removeClass('overflow');
            mediaHolder.css({ 'height': 'auto' });
            extraInfoButton.addClass('hidden');
        }

    }

    function parseThumbnailElements(galleryElement) {
        var slides = $(galleryElement).find('.js-gallery-item');
        var items = [];
        var showCaptionEl = true;
        var numberOfSlides = slides.length;

        $(slides).each(function(i) {
            $(this).attr({ 'data-pswp-uid': i });

            var item,
                title,
                link,
                size,
                src,
                data,
                alt;

            link = $(this).find('a');
            size = $(link).data('size');
            if (size) {
                size = size.split('x');
            }
            src = $(link).attr('href');
            title = $(this).find('.gallery__description').html();
            // added alt text
            alt = $(link).data('alt');

            if (src) {
                var bgImage = $(this).find('.gallery-image__container').css("background-image");
                if (bgImage) {
                    bgImage = bgImage.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
                } else {
                    bgImage = undefined;
                }
                item = {
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10),
                    title: title,
                    msrc: bgImage
                };
                if (src.indexOf('https://www.youtube.com/') === 0) {
                    data = src.split("/watch?v=")[1];
                    item.isVideo = true;
                    item.youTubeID = data.split("&cc_load_policy=")[0];
                    item.captionID = data.split("&cc_load_policy=")[1];
                } else if (src.indexOf('https://vimeo.com/') === 0) {
                    item.isVideo = true;
                    item.vimeoID = src.split('vimeo.com/')[1];
                } else {
                    item.src = src;
                }
                // added alt text
                item.alt = alt;
            }

            var extraInfo = $(this).find('.gallery-cast-profile__extrainfo');
            if (extraInfo.length > 0) {
                item.extraInfo = extraInfo.html();
            }

            if ($(this).find('.js-cast-bio').length) {
                var wrapped = $(this).wrapInner('<div class="cast-profile-wrapper"></div>');
                var htmlString = $(wrapped).clone().wrap('<div>').parent().html();
                showCaptionEl = false;
                item = {
                    html: htmlString
                }
            }

            if (item) {
                item.el = $(this)[0];
                item.pid = i,
                    items.push(item);
            }

        });

        return {
            items: items,
            showCaptionEl: showCaptionEl,
            createGallery: (items.length === numberOfSlides) ? true : false
        }
    }

    //Added by Luke - Remove the gallery title if it exists when the title header is being used.
    if (document.getElementById("titleHeader")) {
        $(".gallery-title").remove();
    }


})(window, jQuery)