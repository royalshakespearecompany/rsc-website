$(function () {
    if ($('.cast-profile').length < 1) return false;

    /* Cast Profile */
    $('.cast-profile').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        fade: true,
        touchThreshold: 5,
        adaptiveHeight: true
    })
	.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

	    $('.cast-thumbs .active-thumb').removeClass('active-thumb');

	    var $current = $('.cast-thumbs .slick-current');
	    var $prev = $current.prevAll('[data-thumb="' + nextSlide + '"]').first();
	    var $next = $current.nextAll('[data-thumb="' + nextSlide + '"]').first();

	    if ($prev.length > 0 && $next.length > 0) {
	        var prevDistance = $current.index() - $prev.index();
	        var nextDistance = $next.index() - $current.index();
	        if (prevDistance < nextDistance) {
	            $('.cast-thumbs').slick('slickGoTo', $prev.attr('data-slick-index'));
	            $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass('active-thumb');
	        }
	        else {
	            $('.cast-thumbs').slick('slickGoTo', $next.attr('data-slick-index'));
	            $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass('active-thumb');
	        }
	    }
	    else if ($prev.length > 0) {
	        $('.cast-thumbs').slick('slickGoTo', $prev.attr('data-slick-index'));
	        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass('active-thumb');
	    }
	    else if ($next.length > 0) {
	        $('.cast-thumbs').slick('slickGoTo', $next.attr('data-slick-index'));
	        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass('active-thumb');
	    }
	    else {
	        var $thumbs = $('.cast-thumbs [data-thumb="' + nextSlide + '"]').closest('.slick-slide');
	        var shortestDistance = null;
	        var $shortestIndex = null;
	        $thumbs.each(function () {
	            var distance = Math.abs($current.index() - $(this).index());
	            if (shortestDistance == null || distance < shortestDistance) {
	                shortestDistance = distance;
	                shortestIndex = $(this).attr('data-slick-index');
	            }
	        });
	        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass('active-thumb');
	        $('.cast-thumbs').slick('slickGoTo', shortestIndex);
	    }

	})
	.on('afterChange', function (event, slick, currentSlide) {
	    $img = $(this).find('.slick-active figure img');
	    if ($img.length > 0) {
	        $('.cast-profile .slick-prev, .cast-profile .slick-next').css('top', $img.first().height() / 2)
	    }
	    else {
	        $('.cast-profile .slick-prev, .cast-profile .slick-next').css('top', $(this).find('.slick-active').height() / 2)
	    }
	});

    $('.carousel-prev').click(function () {
        $('.cast-profile').slick('slickPrev');
    });
    $('.carousel-next').click(function () {
        $('.cast-profile').slick('slickNext');
    });
    $('.close-cast-profile').click(function () {


        $('.cast-profile-container').slideUp(500, function () {
            $(this).css('display', 'block').css('position', 'absolute');
        });

        //$('.cast-profile-container').slideUp(300);
        $('.cast-thumb.active-thumb').removeClass('active-thumb');
    });


    /* Cast Thumbs */
    var i = 0;
    $('.cast-thumbs').children().each(function () {
        $(this).attr('data-thumb', i);
        i++;
    });
    $('.cast-thumbs').on('click', '.cast-thumb', function (e) {

        var thumb = $(this).data('thumb');
        $('.active-thumb').removeClass('active-thumb');
        $('[data-thumb="' + thumb + '"]').addClass('active-thumb');

        $('.cast-profile').slick('slickGoTo', thumb);
        $('.cast-profile').resize();
        $('.cast-profile .slick-list').height($('.cast-profile .slick-current').height());
        $container = $('.cast-profile-container');
        if ($container.css('position') == 'absolute') {
            $container.css('position', 'relative').css('display', 'none');
        }
        $container.slideDown(400, function () {
            $('.slick-current').css("z-index", "0");
        });

        $('.cast-creative-widget').focus();
		$('body, html').scrollTop(Math.floor($('.cast-creative-widget').offset().top - $('.header-container .header-top').height()));


    });


    $('.cast-thumbs').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        focusOnSelect: true,
        touchThreshold: 50,
        speed: 750,
        rows: ($(window).width() >= 1440 ? 2 : 1),
        mobileFirst: true,
        responsive: [
			{
			    breakpoint: 0,
			    settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			    }
			},
			{
			    breakpoint: 480,
			    settings: {
			        slidesToShow: 2,
			        slidesToScroll: 1
			    }
			},
			{
			    breakpoint: 1024,
			    settings: {
			        slidesToShow: 3,
			        slidesToScroll: 1
			    }
			},
			{
			    breakpoint: 1440,
			    settings: {
			        slidesToShow: 4,
			        slidesToScroll: 1,
			    }
			}
        ]
    });


    /* Creative */
    $('.creative-carousel').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        focusOnSelect: true,
        touchThreshold: 50,
        speed: 750,
        mobileFirst: true,
        responsive: [
        {
            breakpoint: 0,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 790,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1440,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1
            }
        }
        ]
    });


    /* Hash Carousel Trigger */
    hashChange = function () {
        var hash = location.hash.substr(1);
        if (hash !== '') {
            $slide = $('[data-hash="' + hash + '"]');
            if ($slide.length > 0) {
                $slide.first().trigger('click');
            }
        }
    };
    hashChange();
    window.onhashchange = function () {
        hashChange();
    };



    $('.slick-slide div:last-child .cast-thumb').addClass('odd-thumb');
});
