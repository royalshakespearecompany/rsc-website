(function($, window, undefined){

  var b_crumbs = $('.breadcrumbs .bc');
  if (b_crumbs.length > 0) {
    run();
  }

  function run(){
    if (!b_crumbs.find('li:first a').attr('href').length) {
      return false;
    }

    var firstBreadCrumbLink = b_crumbs.find('li:first a').attr('href').replace(/\//g,'');
    var links = $.map($('#globalNav .top-level > li > a'), function(link){
      var href, el;

      if ($(link).attr('href')) {
        href = $(link).attr('href').replace(/\//g,'');
        el = $(link);
        if (href === '#') {
          var subNavLink = $(link).next('.subnav-item');
          var link = subNavLink.find('.main-link a');
          if (link.length > 0) {
            href = link.attr('href').replace(/\//g,'');
          }
        }
      }
      return {
        href: href,
        el : el
      };
    });

    var foundLink = links.reduce(function(arr, link){
      if ( link.href === firstBreadCrumbLink ) {
        arr.push(link);
      }
      return arr;
    },[])

    if (foundLink.length > 0) {
      foundLink[0].el.parent().addClass('selected');
    }

  }

})(jQuery, window)
