////// SUB NAV SHOW
function navBtns(theButtons, theSubs){
    theButtons.each(function(i){
        var theThis = $(this);
        var theListEntry = theThis.parent("li");
        var theBkgrnd = subNavBkgrnd;
        
        $(this).click(function(e){
            e.preventDefault();
            
            // if this one is already open, close it all up
            if ($(theSubs[i]).hasClass("vis")){

                theSubs.removeClass("vis-fade");
                theButtons.removeClass("vis-fade");
                $(theSubs[i]).removeClass("vis").addClass("vis-fade");
                theThis.removeClass("vis").addClass("vis-fade");
                theBkgrnd.removeClass("vis").addClass("vis-fade");
                theListEntry.removeClass("vis").addClass("vis-fade");

                setTimeout(function(){
                    theThis.removeClass("vis-fade");
                    $(theSubs[i]).removeClass("vis-fade");
                    theBkgrnd.removeClass("vis-fade");
                    theListEntry.removeClass("vis-fade");

                    if (window.innerWidth >= largeScreen) {
                        // allow scrolling again
                        theBody.removeClass("noscroll");
                    }
                    
                    // add timer back to stage
                    if (window.innerWidth >= mediumScreen && $(".timer-outer").hasClass("timer-hidden")) {
                        $(".timer-outer").removeClass("timer-hidden");
                        TweenLite.from($(".timer-outer"), 0.2, {
                            opacity: 0,
                            onComplete: function(){
                                $(".timer-outer").css({"opacity": ""});
                            }
                        });
                    }
                }, 150);

            // if this isn't open, close all other ones then open it
            } else {
                
                // check if the burger is active, make it active if not
                if (!theMenuButton.hasClass("mb-active")){
                    theMenuButton.addClass("mb-active");
                    theBurger.addClass("active");
                }
                
                // remove vis class from all items + add it to the current one
                theButtons.removeClass("vis");
                theThis.toggleClass("vis");
                theSubs.removeClass("vis");
                $(theSubs[i]).toggleClass("vis");
                mainNavLis.removeClass("vis");
                theListEntry.addClass("vis");

                // make the bkgrnd visible if it's not already
                if (!theBkgrnd.hasClass("vis")){
                    theBkgrnd.addClass("vis");
                }

                //setTimeout(function(){
                //    $(".top-level-link.vis").removeClass("vis");
                //}, 150);

                // stop scrolling on larger screens
                if (window.innerWidth >= mediumScreen && !theBody.hasClass("noscroll")) {
                    
                    if (window.innerWidth >= largeScreen) {
                        theBody.addClass("noscroll");
                    }
                        
                    // add timer back to stage
                    if (!$(".timer-outer").hasClass("timer-hidden")) {
                        
                        TweenLite.to($(".timer-outer"), 0.3, {
                            opacity: 0,
                            onComplete: function(){
                                $(".timer-outer").css({"opacity": ""}).addClass("timer-hidden");
                            }
                        });
                    }
                }
            }
        });
    });
}

////// END SUB NAV SHOW


////// GLOBAL SEARCH TOGGLE
function globalSearchShow() {
    globalSearch.addClass("vis");
        
    if (window.innerWidth >= largeScreen) {
        theBody.addClass("noscroll");
    }
}

function globalSearchHide() {
    globalSearch.addClass("vis-fade").removeClass("vis");
    
    setTimeout(function(){
        globalSearch.removeClass("vis-fade");
        
        if (window.innerWidth >= largeScreen) {
            theBody.removeClass("noscroll");
        }
    }, 300);
}

////// END GLOBAL SEARCH TOGGLE


////// EDUCATION RESOURCES LINK CREATOR
function edResourceLink() {
    var newLink = "/education/teacher-resources/search/", playLink, typeLink, ageLink;
    
    if (resBankFilters.play != "") {
        playLink = resBankFilters.play;
    } else {
        playLink = "any";
    }
    
    if (resBankFilters.age.length > 0) {
        ageLink = resBankFilters.age.join("-");
    } else {
        ageLink = "any";
    }
    
    if (resBankFilters.type.length > 0) {
        typeLink = resBankFilters.type.join("-");
    } else {
        typeLink = "any";
    }
    
    newLink += "play/" + playLink + "/type/" + typeLink + "/age/" + ageLink + "/";
    
    $("#resButton").removeClass("inactive").attr("href", newLink);
    
}

////// END EDUCATION RESOURCES LINK CREATOR