function perfFilter(myFilters) {
    var theCheckbox = myFilters.checkBox || null,
        theStart = myFilters.startDate || plActiveFilters.when[0],
        theEnd = myFilters.endDate || plActiveFilters.when[1];

    plWarning.fadeOut(150).html("");

    var errMessage1 = "Sorry, there are no performances for the PLACES you have selected",
        errMessage2 = "Sorry, there are no performances for the TIMES you have selected",
        errMessage3 = "Sorry, there are no performances for the DATES you have selected";

    // subtract a day from start date to include perfs on that date
    var theStartParsed = moment(theStart, "YYYY-MM-DD").subtract(1, 'days').format("ll");

    // add a day to end date to include perfs on that date
    var theEndParsed = moment(theEnd, "YYYY-MM-DD").add(1, 'days').format("ll");

    if (theCheckbox){
        var theCategory = theCheckbox.attr("name"),
            theBox = theCheckbox.attr("id"),
            checked = theCheckbox.is(":checked");

        // add or remove what's checked from the plActiveFilters object
        for (var theKey in plActiveFilters){
            if (theKey == theCategory){
                if (checked){
                    plActiveFilters[theKey].push(theBox);
                } else {
                    plActiveFilters[theKey].splice($.inArray(theBox, plActiveFilters[theKey]), 1);
                }
            }
        }
    }

    plLoader.fadeIn(200);

    // set up the filters
    var theFilters = "";

    // check the where filter
    for (var i = 0; i < plActiveFilters.where.length; i++){
        //theFilters += "[data-location='" + plActiveFilters.where[i] + "']";
        theFilters += "[data-where='" + plActiveFilters.where[i] + "']";
        if (i < plActiveFilters.where.length - 1){
            theFilters += ",";
        }
    }


    // filter for where
    var firstFilters = plFullList.filter(theFilters);

    // fade peformances out
    plFullList.fadeOut(200);
    apHeaders.fadeOut(200);

    // reset the filters
    theFilters = "";

    // check the time filter
    for (var i = 0; i < plActiveFilters.time.length; i++){
        theFilters += "[data-time='" + plActiveFilters.time[i] + "']";
        if (i < plActiveFilters.time.length - 1){
            theFilters += ",";
        }
    }

    // filter for matinee/evening
    var secondFilters = firstFilters.filter(theFilters);

    setTimeout(function(){

        var filterCount = 0;

        secondFilters.each(function(){
            var thePerf = $(this);

            if (moment(thePerf.data("when"), "LL").isBetween(theStartParsed, theEndParsed)){
                thePerf.show();
                filterCount++;

                // access performance headers, reintroduce to stage
                if (apHeaders.length > 0){
                    apHeaders.each(function(){
                        var $this = $(this);
                        if ($this.prop("id") == thePerf.data("title")){
                            $this.show();
                        }
                    });
                }
            }
        });

        if (firstFilters.length < 1) {
            plWarning.fadeIn(150).html(errMessage1);
        } else if (secondFilters.length < 1) {
            plWarning.fadeIn(150).html(errMessage2);
        } else if (filterCount == 0) {
            plWarning.fadeIn(150).html(errMessage3);
        }

        plLoader.fadeOut(150);
        perfList.simplebar('recalculate');

    }, 700);

}

// compare dates
function compareMilli(a,b) {
	if(a.milli < b.milli) return -1;
	if(a.milli > b.milli) return 1;
	return 0;
}

/*
$(document).ready(function() {
  $('a.buy-btn').on('click touchend', function(e) {
    var el = $(this);
    var link = el.attr('href');
    window.location = link;
  });
});
*/