function gallerySetup(theGallery, theThumbs, thePhotos) {
    var $galleryThumbs = $("#" + theThumbs);
    var $galleryPhotos = $("#" + thePhotos);
    var $galleryThumbsContainer = $("#" + theThumbs + "cont");
    var $galleryPhotosContainer = $("#" + thePhotos + "cont");
    var $this = $(theGallery);

    /* Gallery Thumbs */
    var i = 0;
    $galleryThumbs.children().each(function(){
        $(this).attr('data-thumb', i);
        i++;
    });
    var slideCount = i;
    $galleryThumbs.find('figure img').each(function(){
        $(this).addClass($(this).width() > $(this).height() ? 'max-width' : 'max-height');
    });

    $galleryThumbs.slick({
        infinite: true,
        arrows: true,
        dots: false,
        touchThreshold: 50,
        speed: 750,
        variableWidth: true,
        asNavFor: "#" + thePhotos,
        focusOnSelect: true,
        mobileFirst: true
    });

    $galleryThumbsContainer
        .prepend('<span class="slick-prev-custom" id="slickPrevCustom-' + theThumbs + '" role="button">Previous</span>')
        .append('<span class="slick-next-custom" id="slickNextCustom-' + theThumbs + '" role="button">Next</span>');

    $('#slickPrevCustom-' + theThumbs).click(function(){
        //$galleryThumbs.slick('slickPrev');
        $galleryThumbs.slick('slickGoTo', $('#' + theThumbs + ' .slick-current').prev().attr('data-slick-index'));
    });
    $('#slickNextCustom-' + theThumbs).click(function(){
        //$galleryThumbs.slick('slickNext');
        $galleryThumbs.slick('slickGoTo', $('#' + theThumbs + ' .slick-current').next().attr('data-slick-index'));
    });

    $galleryThumbs.on('click', $galleryThumbs.children('.gallery-thumb'), function(){
        //console.log('HELLO ' + theThumbs);
        $('body, html').scrollTop(Math.floor($this.offset().top - $('.header-container .header-top').height()));
    });

    /* Gallery Photo */
    $galleryPhotos.on('init', function(event, slick){
        $galleryThumbs.find('.gallery-thumb[data-thumb="0"]').addClass('active-thumb');
        $galleryPhotosContainer.find('.slide-number').text('1/' + slideCount);
    });
    $galleryPhotos.find('figure img').each(function(){
        $(this).addClass($(this).width() > $(this).height() ? 'max-width' : 'max-height');
    });
    $galleryPhotos.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        fade: true,
        touchThreshold: 5,
        adaptiveHeight: true,
        asNavFor: "#" + theThumbs
    })
        .on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $galleryThumbs.find('.active-thumb').removeClass('active-thumb');
        $galleryThumbs.find('.gallery-thumb[data-thumb="' + nextSlide + '"]').addClass('active-thumb');
        $galleryPhotosContainer.find('.slide-number').text((nextSlide + 1) + '/' + slideCount);
    });
}

$(function () {

	if ($('.gallery-widget').length > 0) {

        $(".gallery-widget").each(function(j){
            $this =$(this).attr("id", "galleryWidget" + j);

            var theThumbsID = "galleryThumbs" + j;
            var thePhotosID = "galleryPhotos" + j;

            $this.find(".gallery-thumbs").attr("id", theThumbsID);
            $this.find(".gallery-photos").attr("id", thePhotosID);
            $this.find(".gallery-thumbs-container").attr("id", theThumbsID + "cont");
            $this.find(".gallery-photo-container").attr("id", thePhotosID + "cont");

            gallerySetup($this, theThumbsID, thePhotosID);


            /* Hash Carousel Trigger
            hashChange = function() {
                var hash = location.hash.substr(1);
                if (hash !== '') {
                    $slide = $('[data-hash="' + hash + '"]');
                    if ($slide.length > 0) {
                        $slide.first().trigger('click');
                    }
                }
            };
            hashChange();
            window.onhashchange = function (){
                hashChange();
            }; */
        });
    }
});
