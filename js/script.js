function navBtns(theButtons, theSubs) {
    theButtons.each(function(i) {
        var theThis = $(this), theListEntry = theThis.parent("li"), theBkgrnd = subNavBkgrnd;
        $(this).click(function(e) {
            e.preventDefault(), $(theSubs[i]).hasClass("vis") ? (theSubs.removeClass("vis-fade"), 
            theButtons.removeClass("vis-fade"), $(theSubs[i]).removeClass("vis").addClass("vis-fade"), 
            theThis.removeClass("vis").addClass("vis-fade"), theBkgrnd.removeClass("vis").addClass("vis-fade"), 
            theListEntry.removeClass("vis").addClass("vis-fade"), setTimeout(function() {
                theThis.removeClass("vis-fade"), $(theSubs[i]).removeClass("vis-fade"), theBkgrnd.removeClass("vis-fade"), 
                theListEntry.removeClass("vis-fade"), window.innerWidth >= largeScreen && theBody.removeClass("noscroll"), 
                window.innerWidth >= mediumScreen && $(".timer-outer").hasClass("timer-hidden") && ($(".timer-outer").removeClass("timer-hidden"), 
                TweenLite.from($(".timer-outer"), .2, {
                    opacity: 0,
                    onComplete: function() {
                        $(".timer-outer").css({
                            opacity: ""
                        });
                    }
                }));
            }, 150)) : (theMenuButton.hasClass("mb-active") || (theMenuButton.addClass("mb-active"), 
            theBurger.addClass("active")), theButtons.removeClass("vis"), theThis.toggleClass("vis"), 
            theSubs.removeClass("vis"), $(theSubs[i]).toggleClass("vis"), mainNavLis.removeClass("vis"), 
            theListEntry.addClass("vis"), theBkgrnd.hasClass("vis") || theBkgrnd.addClass("vis"), 
            window.innerWidth >= mediumScreen && !theBody.hasClass("noscroll") && (window.innerWidth >= largeScreen && theBody.addClass("noscroll"), 
            $(".timer-outer").hasClass("timer-hidden") || TweenLite.to($(".timer-outer"), .3, {
                opacity: 0,
                onComplete: function() {
                    $(".timer-outer").css({
                        opacity: ""
                    }).addClass("timer-hidden");
                }
            })));
        });
    });
}

function globalSearchShow() {
    globalSearch.addClass("vis"), window.innerWidth >= largeScreen && theBody.addClass("noscroll");
}

function globalSearchHide() {
    globalSearch.addClass("vis-fade").removeClass("vis"), setTimeout(function() {
        globalSearch.removeClass("vis-fade"), window.innerWidth >= largeScreen && theBody.removeClass("noscroll");
    }, 300);
}

function edResourceLink() {
    var playLink, typeLink, ageLink, newLink = "/education/teacher-resources/search/";
    playLink = "" != resBankFilters.play ? resBankFilters.play : "any", ageLink = resBankFilters.age.length > 0 ? resBankFilters.age.join("-") : "any", 
    typeLink = resBankFilters.type.length > 0 ? resBankFilters.type.join("-") : "any", 
    newLink += "play/" + playLink + "/type/" + typeLink + "/age/" + ageLink + "/", $("#resButton").removeClass("inactive").attr("href", newLink);
}

function gallerySetup(theGallery, theThumbs, thePhotos) {
    var $galleryThumbs = $("#" + theThumbs), $galleryPhotos = $("#" + thePhotos), $galleryThumbsContainer = $("#" + theThumbs + "cont"), $galleryPhotosContainer = $("#" + thePhotos + "cont"), $this = $(theGallery), i = 0;
    $galleryThumbs.children().each(function() {
        $(this).attr("data-thumb", i), i++;
    });
    var slideCount = i;
    $galleryThumbs.find("figure img").each(function() {
        $(this).addClass($(this).width() > $(this).height() ? "max-width" : "max-height");
    }), $galleryThumbs.slick({
        infinite: !0,
        arrows: !0,
        dots: !1,
        touchThreshold: 50,
        speed: 750,
        variableWidth: !0,
        asNavFor: "#" + thePhotos,
        focusOnSelect: !0,
        mobileFirst: !0
    }), $galleryThumbsContainer.prepend('<span class="slick-prev-custom" id="slickPrevCustom-' + theThumbs + '" role="button">Previous</span>').append('<span class="slick-next-custom" id="slickNextCustom-' + theThumbs + '" role="button">Next</span>'), 
    $("#slickPrevCustom-" + theThumbs).click(function() {
        $galleryThumbs.slick("slickGoTo", $("#" + theThumbs + " .slick-current").prev().attr("data-slick-index"));
    }), $("#slickNextCustom-" + theThumbs).click(function() {
        $galleryThumbs.slick("slickGoTo", $("#" + theThumbs + " .slick-current").next().attr("data-slick-index"));
    }), $galleryThumbs.on("click", $galleryThumbs.children(".gallery-thumb"), function() {
        $("body, html").scrollTop(Math.floor($this.offset().top - $(".header-container .header-top").height()));
    }), $galleryPhotos.on("init", function(event, slick) {
        $galleryThumbs.find('.gallery-thumb[data-thumb="0"]').addClass("active-thumb"), 
        $galleryPhotosContainer.find(".slide-number").text("1/" + slideCount);
    }), $galleryPhotos.find("figure img").each(function() {
        $(this).addClass($(this).width() > $(this).height() ? "max-width" : "max-height");
    }), $galleryPhotos.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        arrows: !0,
        fade: !0,
        touchThreshold: 5,
        adaptiveHeight: !0,
        asNavFor: "#" + theThumbs
    }).on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        $galleryThumbs.find(".active-thumb").removeClass("active-thumb"), $galleryThumbs.find('.gallery-thumb[data-thumb="' + nextSlide + '"]').addClass("active-thumb"), 
        $galleryPhotosContainer.find(".slide-number").text(nextSlide + 1 + "/" + slideCount);
    });
}

function expandInnerDetails(event) {
    event.stopPropagation();
    var myMod = event.data.theMod;
    myMod.hasClass("selected") || ("undefined" == typeof expandedModule || "undefined" == expandedModule ? (expandedModule = myMod, 
    myMod.hasClass("day") && openDayAnimation()) : collapseInnerDetails(event));
}

function collapseInnerDetails(e) {
    if (e.stopPropagation(), "undefined" == typeof expandedModule || "undefined" == expandedModule) $("#openPerfs").length > 0 && closeDayAnimation(); else {
        var myMod = e.data.theMod;
        myMod.hasClass("day") && closeDayAnimation(), expandedModule = myMod, setTimeout(function() {
            dsOffset = expandedModule.offset().top, TweenLite.to(theBody, .1, {
                scrollTop: dsOffset - 90
            }), myMod.hasClass("day") && openDayAnimation();
        }, 450);
    }
}

function hsScrollMe(event) {
    var distance, thePosition = event.data.thePosition, theFC = event.data.theFC, theDirection = event.data.theDirection, theWidth = $(theFC).outerWidth(!0);
    distance = "left" == theDirection ? "-=" + theWidth.toString() : "+=" + theWidth.toString(), 
    $(horzScollers[thePosition]).find(".simplebar-scroll-content").animate({
        scrollLeft: distance
    }, 400);
}

function showIt(event) {
    var myTarget = $(event.target), myTargetID = myTarget.prop("id");
    myTargetID.indexOf("Expand") == -1 && (myTargetID = myTarget.parents(".expander").prop("id"));
    var myTargetInner = myTarget.html(), expandID = "#" + myTargetID.replace("Expand", "") + "Hidden", myExpand = $(expandID);
    myExpand.show(), myTarget.hasClass("more-button") && myTarget.animate({
        opacity: 0
    }, 100), TweenLite.from(myExpand, .4, {
        height: 0,
        opacity: 0,
        ease: Circ.easeOut,
        onComplete: function() {
            myTarget.unbind("click"), myTarget.hasClass("more-button") && myTarget.removeClass("more-button").addClass("less-button").html("close").animate({
                opacity: 1
            }, 100).click(function(e) {
                e.preventDefault(), closeIt(myTargetID, myTargetInner, expandID);
            });
        }
    });
}

function closeIt(target, copy, expand) {
    var myExpand = $(expand), myTarget = $("#" + target);
    myTarget.animate({
        opacity: 0
    }, 50), TweenLite.to(myExpand, .4, {
        height: 0,
        opacity: 0,
        ease: Circ.easeOut,
        onComplete: function() {
            myExpand.css({
                height: "",
                opacity: "",
                display: ""
            }), myTarget.unbind("click").removeClass("less-button").addClass("more-button").html(copy).animate({
                opacity: 1
            }, 50).click(function(e) {
                e.preventDefault(), showIt(e);
            });
        }
    });
}

function getQueryVariable(variable) {
    if ("" == variable) return !1;
    for (var query = window.location.search.substring(1), vars = query.split("&"), i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) return pair[1];
    }
    return !1;
}

function multipleQueries() {
    var query = window.location.search.substring(1), vars = query.split("&");
    return vars.length > 1;
}

function videoResize(largeVid, smlVid) {
    var videoHoldWidth, videoHoldHeight;
    "undefined" != typeof largeVid && largeVid.length > 0 && largeVid.each(function() {
        videoHoldWidth = $(this).width(), videoHoldHeight = Math.round(videoHoldWidth / 16 * 9), 
        $(this).find("iframe").height(videoHoldHeight);
    }), "undefined" != typeof smlVid && smlVid.length > 0 && smlVid.each(function() {
        videoHoldWidth = $(this).width(), videoHoldHeight = Math.round(videoHoldWidth / 16 * 9), 
        $(this).find("iframe").height(videoHoldHeight);
    }), "undefined" != typeof galleryIframe && galleryIframe.length > 0 && galleryIframe.each(function() {
        videoHoldWidth = $(this).width(), videoHoldHeight = Math.round(videoHoldWidth / 16 * 9), 
        $(this).height(videoHoldHeight);
    }), "undefined" != typeof homeVidIframe && homeVidIframe.length > 0 && (videoHoldHeight = homeVideoCover.height(), 
    videoHoldWidth = Math.round(videoHoldHeight / 9 * 16), videoHoldWidth > window.innerWidth ? (videoHoldWidth = window.innerWidth, 
    videoHoldHeight = Math.round(window.innerWidth / 16 * 9)) : homeVideoHolder.find("#videoCover").is(":visible") || (videoHoldHeight = Math.min(Math.round(window.innerWidth / 1440 * 684), 684), 
    videoHoldWidth = Math.round(videoHoldHeight / 9 * 16)), homeVidIframe.height(videoHoldHeight));
}

function callPlayer(frame_id, func, args) {
    function messageEvent(add, listener) {
        var w3 = add ? window.addEventListener : window.removeEventListener;
        w3 ? w3("message", listener, !1) : (add ? window.attachEvent : window.detachEvent)("onmessage", listener), 
        console.log(w3);
    }
    window.jQuery && frame_id instanceof jQuery && (frame_id = frame_id.get(0).id);
    var iframe = document.getElementById(frame_id);
    iframe && "IFRAME" != iframe.tagName.toUpperCase() && (iframe = iframe.getElementsByTagName("iframe")[0]), 
    callPlayer.queue || (callPlayer.queue = {});
    var queue = callPlayer.queue[frame_id], domReady = "complete" == document.readyState;
    if (domReady && !iframe) window.console && console.log("callPlayer: Frame not found; id=" + frame_id), 
    queue && clearInterval(queue.poller); else if ("listening" === func) console.log("listening buzz"), 
    iframe && iframe.contentWindow && (func = '{"event":"listening","id":' + JSON.stringify("" + frame_id) + "}", 
    iframe.contentWindow.postMessage(func, "*")); else if (domReady && (!iframe || iframe.contentWindow && (!queue || queue.ready)) && (queue && queue.ready || "function" != typeof func)) {
        if (iframe && iframe.contentWindow) {
            if (func.call) return func();
            iframe.contentWindow.postMessage(JSON.stringify({
                event: "command",
                func: func,
                args: args || [],
                id: frame_id
            }), "*");
        }
    } else console.log("I'm going to set off that listening buzz"), queue || (queue = callPlayer.queue[frame_id] = []), 
    queue.push([ func, args ]), "poller" in queue || (queue.poller = setInterval(function() {
        callPlayer(frame_id, "listening");
    }, 250), messageEvent(1, function runOnceReady(e) {
        if (console.log("e= " + e.data), !iframe) {
            if (console.log("no iframe"), iframe = document.getElementById(frame_id), !iframe) return;
            if ("IFRAME" != iframe.tagName.toUpperCase() && (iframe = iframe.getElementsByTagName("iframe")[0], 
            !iframe)) return;
        }
        if (e.source === iframe.contentWindow) for (clearInterval(queue.poller), queue.ready = !0, 
        messageEvent(0, runOnceReady); tmp = queue.shift(); ) callPlayer(frame_id, tmp[0], tmp[1]);
    }, !1));
}

function getFrameID(id) {
    var elem = document.getElementById(id);
    if (elem) {
        if (/^iframe$/i.test(elem.tagName)) return id;
        var elems = elem.getElementsByTagName("iframe");
        if (!elems.length) return null;
        for (var i = 0; i < elems.length && !/^https?:\/\/(?:www\.)?youtube(?:-nocookie)?\.com(\/|$)/i.test(elems[i].src); i++) ;
        if (elem = elems[i], elem.id) return elem.id;
        do id += "-frame"; while (document.getElementById(id));
        return elem.id = id, id;
    }
    return null;
}

function onYouTubePlayerAPIReady() {
    YT_ready(!0);
}

function isTouchDevice() {
    return 1 == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0);
}

function arrayAdd(a, b) {
    return a + b;
}

function closeInfo(pNumb) {
    var thePopup = $(infoPopUp[pNumb]), myPos = parseInt(thePopup.css("bottom"));
    thePopup.removeClass("here"), TweenLite.to(thePopup, .4, {
        bottom: -30,
        opacity: 0,
        ease: Circ.easeIn,
        onComplete: function() {
            thePopup.css("display", "none");
        }
    });
    for (var pNumbIt = pNumb + 1; pNumbIt < infoPopUp.length; ) {
        if ($(infoPopUp[pNumbIt]).hasClass("here")) {
            setTimeout(function() {
                moveInfoPop(pNumbIt, myPos);
            }, 200);
            break;
        }
        pNumbIt++;
    }
    "compatibilityBar" == infoPopUp[pNumb].id && (sessionStorage.compatibilityMessage = !0), 
    "cookieBar" == infoPopUp[pNumb].id && (cookieDate = new Date(Date.now())) && cookieDate.setMonth(cookieDate.getMonth() + 1) && (document.cookie = "cookiesAccepted=true;expires=" + cookieDate.toGMTString() + ";path=/");
}

function moveInfoPop(pNumb, newPos) {
    var myPos = $(infoPopUp[pNumb]).height() + 8 + newPos;
    TweenLite.to($(infoPopUp[pNumb]), .4, {
        bottom: newPos,
        ease: Circ.easeInOut,
        onComplete: function() {
            pNumb < infoPopUp.length - 1 && moveInfoPop(pNumb + 1, myPos);
        }
    });
}

function findAncestor(el, cls) {
    for (;(el = el.parentElement) && !el.classList.contains(cls); ) ;
    return el;
}

function cfOpenNext(event, formItem) {
    var theForm, thePos, theNextPos, theFormPos, myTarget = $(event.currentTarget), nextItem = $("#" + myTarget[0].getAttribute("data-next")), nextBtns = nextItem.find(".condit-btn"), bkBtn = nextItem.find(".condit-bk-btn"), formEnd = !1;
    theForm = formItem ? formItem : $("#" + myTarget.data("parent")), theForm.data("questions").push(event.target.getAttribute("data-next")), 
    thePos = myTarget.data("position"), theFormPos = theForm.data("position"), theNextPos = thePos + 1, 
    thePos <= theForm.data("position") && 0 !== thePos && console.log("we've gone backwards"), 
    theForm.data("position", thePos), nextBtns.length > -1 && nextBtns.each(function(i) {
        var $this = $(this);
        $this.data("position", theNextPos).click(function(e) {
            cfOpenNext(e, theForm);
        });
    }), nextItem.hasClass("final") && (formEnd = !0), nextItem.hasClass("title-select") && nextItem.find("#sbTitle").html(myTarget[0].value), 
    bkBtn.length > -1 && bkBtn.click(function(e) {
        cfBackUp(e, theForm);
    }), cfAnimate(nextItem, formEnd);
}

function cfAnimate(theItem, end) {
    var newItemHeight, nxtItem = $(theItem);
    nxtItem.addClass("open"), newItemHeight = nxtItem.height() > windowHeight - globalHeadertop.outerHeight() ? nxtItem.height() : windowHeight - globalHeadertop.outerHeight(), 
    nxtItem.css({
        height: 0,
        opacity: 0
    }), TweenLite.to(nxtItem, .3, {
        height: newItemHeight
    }), TweenLite.to(nxtItem, .4, {
        opacity: 1
    }).delay(.8), gimmeGo(nxtItem), end && nxtItem.siblings(".submit").addClass("open");
}

function cfBackUp(event, formItem) {
    event.preventDefault();
}

function cfDestroyPrev(items) {}

function MischiefInline($) {
    function init() {
        storageAvailable("localStorage") && run();
    }
    function run() {
        var lastVisit = getLocalStorage("mis_run_date");
        if (lastVisit) {
            var diff = new Date().getTime() - new Date(lastVisit).getTime(), seconds = qs_config.debug === !0 ? 100 : 86400 * config.gapInterval;
            diff > seconds && (localStorage.removeItem("mis_run"), localStorage.removeItem("mis_run_date"));
        }
        if (!getLocalStorage("mis_run")) {
            removeTimeouts(), setHasRun();
            var markup = buildTemplate();
            mischiefWidget = $(markup), mischiefWidgetHolder = mischiefWidget.find(".mischief-widget-holder"), 
            mischiefWidgetHolder.css({
                background: "url(/RSC/img/mischief-background_crop.png) no-repeat 50% 50%",
                height: "210px",
                width: "210px",
                position: "relative",
                display: "block",
                float: "right"
            }), overlay = mischiefWidget.find(".overlay"), overlay.css({
                background: "url(/RSC/img/mischief-overlay_crop.png) no-repeat 50% 50%",
                height: "100%",
                width: "100%",
                position: "absolute",
                display: "block",
                top: 0,
                left: 0
            }), section.prepend(mischiefWidget), startAnimation();
        }
    }
    function getRandomInt(min, max) {
        return min = Math.ceil(min), max = Math.floor(max), Math.floor(Math.random() * (max - min)) + min;
    }
    function startAnimation() {
        var flickerTime = 50;
        setInterval(function() {
            var valMin = 1, valMax = 100, pauseMin = 300, pauseMax = 400, val = getRandomInt(parseInt(valMin, 10), parseInt(valMax, 10)), pause = getRandomInt(parseInt(pauseMin, 10), parseInt(pauseMax, 10));
            val < 25 && (overlay.css({
                display: "block"
            }), setTimeout(function() {
                overlay.css({
                    display: "none"
                });
            }, pause));
        }, flickerTime);
    }
    function buildTemplate() {
        return [ '<div class="mischief-widget mischief-widget--inline">', '<a href="' + config.link + '">', '<div class="mischief-widget-holder">', '<div class="mischief-widget__inner">', '<div class="overlay"></div>', "</div>", "</div>", "</a>", "</div>" ].join("");
    }
    function storageAvailable(type) {
        try {
            var storage = window[type], x = "__storage_test__";
            return storage.setItem(x, x), storage.removeItem(x), !0;
        } catch (e) {
            return !1;
        }
    }
    function getLocalStorage(key) {
        var data = localStorage.getItem(key);
        return !!data && data;
    }
    function setLocalStorage(key, value) {
        localStorage.setItem(key, value);
    }
    function removeTimeouts() {
        timeouts.forEach(function(timeout) {
            window.clearTimeout(timeout);
        }), timeouts = [];
    }
    function setHasRun() {
        var today = new Date();
        setLocalStorage("mis_run", 1), setLocalStorage("mis_run_date", today.toISOString());
    }
    var elem = $("#mischieflink"), canRun = elem.length;
    if (!canRun) return !1;
    var qs_config = {
        debug: $("#mischieflink").data("always") || "false"
    }, section = $(".play-hub #copyInner .sfContentBlock").first(), config = {
        counter: 0,
        gapInterval: $("#mischieflink").data("gapInterval") || 2,
        link: $("#mischieflink").data("link") || "/"
    };
    if (section.length < 1) return !1;
    var mischiefWidget, overlay, mischiefWidgetHolder, timeouts = [];
    return init(), {
        init: init
    };
}

function perfFilter(myFilters) {
    var theCheckbox = myFilters.checkBox || null, theStart = myFilters.startDate || plActiveFilters.when[0], theEnd = myFilters.endDate || plActiveFilters.when[1];
    plWarning.fadeOut(150).html("");
    var errMessage1 = "Sorry, there are no performances for the PLACES you have selected", errMessage2 = "Sorry, there are no performances for the TIMES you have selected", errMessage3 = "Sorry, there are no performances for the DATES you have selected", theStartParsed = moment(theStart, "YYYY-MM-DD").subtract(1, "days").format("ll"), theEndParsed = moment(theEnd, "YYYY-MM-DD").add(1, "days").format("ll");
    if (theCheckbox) {
        var theCategory = theCheckbox.attr("name"), theBox = theCheckbox.attr("id"), checked = theCheckbox.is(":checked");
        for (var theKey in plActiveFilters) theKey == theCategory && (checked ? plActiveFilters[theKey].push(theBox) : plActiveFilters[theKey].splice($.inArray(theBox, plActiveFilters[theKey]), 1));
    }
    plLoader.fadeIn(200);
    for (var theFilters = "", i = 0; i < plActiveFilters.where.length; i++) theFilters += "[data-where='" + plActiveFilters.where[i] + "']", 
    i < plActiveFilters.where.length - 1 && (theFilters += ",");
    var firstFilters = plFullList.filter(theFilters);
    plFullList.fadeOut(200), apHeaders.fadeOut(200), theFilters = "";
    for (var i = 0; i < plActiveFilters.time.length; i++) theFilters += "[data-time='" + plActiveFilters.time[i] + "']", 
    i < plActiveFilters.time.length - 1 && (theFilters += ",");
    var secondFilters = firstFilters.filter(theFilters);
    setTimeout(function() {
        var filterCount = 0;
        secondFilters.each(function() {
            var thePerf = $(this);
            moment(thePerf.data("when"), "LL").isBetween(theStartParsed, theEndParsed) && (thePerf.show(), 
            filterCount++, apHeaders.length > 0 && apHeaders.each(function() {
                var $this = $(this);
                $this.prop("id") == thePerf.data("title") && $this.show();
            }));
        }), firstFilters.length < 1 ? plWarning.fadeIn(150).html(errMessage1) : secondFilters.length < 1 ? plWarning.fadeIn(150).html(errMessage2) : 0 == filterCount && plWarning.fadeIn(150).html(errMessage3), 
        plLoader.fadeOut(150), perfList.simplebar("recalculate");
    }, 700);
}

function compareMilli(a, b) {
    return a.milli < b.milli ? -1 : a.milli > b.milli ? 1 : 0;
}

function openQuestion(activeNumber) {
    questionHolder.addClass("active"), closeQuestion();
    var currentQ = $(woBtns[activeNumber]), theFilters = currentQ.find(".filters");
    currentQ.addClass("active"), screenWidth < mediumScreen && (theFilters.css({
        height: "",
        opacity: "",
        transform: ""
    }), TweenLite.from(theFilters, .6, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            theFilters.css({
                height: "",
                opacity: "",
                transform: ""
            }), $("html, body").animate({
                scrollTop: currentQ.offset().top - 90
            }, 500);
        }
    })), woActiveQ = activeNumber;
}

function closeQuestion() {
    var activeNumber, myQuestion = $(".wo-q-item.active"), mySelections = myQuestion.find(".selection .selected"), selIndicator = myQuestion.find(".wo-showing"), currentFilters = myQuestion.find(".filters");
    if ($(".wo-q-item").each(function(i) {
        $(this).hasClass("active") && (activeNumber = i);
    }), mySelections.length > 0 ? (selIndicator.html(""), mySelections.each(function(i) {
        var theChoice = "<span class='choice'>" + $(this).siblings("label").html() + "</span>";
        selIndicator.append(theChoice);
    }), selIndicator.addClass("chosen")) : "undefined" != typeof startDateSel && myQuestion.hasClass("wo-when") ? selIndicator.html("<span class='choice'>" + startDateSel + " - " + endDateSel + "</span>").addClass("chosen") : selIndicator.html(woShowingDefault[activeNumber]).removeClass("chosen"), 
    screenWidth < mediumScreen && TweenLite.to(currentFilters, .3, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            myQuestion.removeClass("active");
        }
    }), woSearchOpen.hasClass("active")) {
        woSearchOpen.removeClass("active");
        var theBox = woSearchOpen.find(".form-holder");
        TweenLite.to(theBox, .4, {
            height: 0,
            "margin-top": 0,
            "margin-bottom": 0,
            opacity: 0,
            onComplete: function() {
                theBox.css({
                    height: ""
                });
            }
        });
    }
}

function gimmeGo(theThing) {
    if (1 == theThing) thePage.stop().scrollTo($(".wo-results"), {
        duration: 1e3,
        offsetTop: headerHeight
    }); else if (2 == theThing) thePage.stop().scrollTo($("#SelectedLevel"), {
        duration: 1e3,
        offsetTop: headerHeight
    }); else {
        var $theThing = $(theThing), myTopPos = $theThing.offset().top;
        $("body, html").stop().animate({
            scrollTop: myTopPos - headerHeight
        }, 1e3);
    }
}

function clickTheGrid(event) {
    function clearGrid() {
        TweenLite.to(clonedDetails, .2, {
            opacity: 0
        }), clonedGI.css({
            overflow: "hidden",
            "min-height": "none"
        }), TweenLite.to(clonedGI, .3, {
            delay: .15,
            height: 0,
            width: 0,
            opacity: 0
        }), TweenLite.to(gridFocusOuter, .3, {
            delay: .35,
            opacity: 0,
            height: gridFocusOuterHeight,
            onComplete: function() {
                gridFocusOuter.empty().removeClass("active").css({
                    height: ""
                });
            }
        });
    }
    "undefined" != typeof gridFocusOuter && gridFocusOuter.empty().removeClass("active");
    var thingy = event.data.theGridItem, thingyPos = thingy.position(), thingyTop = thingyPos.top, thingyLeft = thingyPos.left, thingyWidth = thingy.width(), thingyHeight = thingy.height();
    woActiveItem = thingy, gridFocusOuter = thingy.siblings(".active-gi-holder").addClass("active"), 
    TweenLite.to(gridFocusOuter, .5, {
        opacity: 1
    }), gridFocusOuterHeight = gridFocusOuter.height();
    var clonedGI = thingy.clone().addClass("active").appendTo(gridFocusOuter).css({
        "min-height": thingyHeight
    }), clonedGIHeight = clonedGI.height(), clonedGIWidth = clonedGI.width(), clonedGIMainImg = clonedGI.find(".main-image-holder"), clonedTitleCopy = clonedGI.find(".title-copy"), thingTitleCopyheight = thingy.find(".title-copy").height(), thingTitleBkgrndMarg = thingy.find(".gi-title-bkgrnd").css("margin-top"), clonedTitleBkgrnd = clonedGI.find(".gi-title-bkgrnd").css({
        "margin-top": thingTitleBkgrndMarg
    }), clonedDetails = clonedGI.find(".gi-details");
    clonedGI.css({
        height: thingyHeight,
        width: thingyWidth
    }), screenWidth >= smadiumScreen && screenWidth < largeScreen && thingyLeft > 0 ? (clonedGI.css({
        right: 0
    }), TweenLite.to(clonedGIMainImg, .4, {
        delay: .3,
        height: "147px",
        opacity: 0
    }), TweenLite.from(clonedTitleCopy, .4, {
        delay: .3,
        height: thingTitleCopyheight
    }), TweenLite.to(clonedTitleBkgrnd, .4, {
        delay: .3,
        "margin-top": "16px"
    }), TweenLite.to(clonedGI, .4, {
        delay: .3,
        height: clonedGIHeight,
        onComplete: function() {
            clonedDetails.css({
                display: "block"
            });
        }
    }), TweenLite.to(clonedGI, .4, {
        delay: .7,
        width: clonedGIWidth,
        onComplete: function() {
            clonedGI.css({
                height: "auto",
                width: ""
            });
        }
    }), TweenLite.to(clonedDetails, .4, {
        delay: 1,
        opacity: 1
    })) : screenWidth >= largeScreen && thingyLeft > gridFocusOuter.width() / 2 ? (clonedGI.css({
        right: 0
    }), TweenLite.to(clonedGIMainImg, .4, {
        delay: .3,
        height: "147px",
        opacity: 0
    }), TweenLite.from(clonedTitleCopy, .4, {
        delay: .3,
        height: thingTitleCopyheight
    }), TweenLite.to(clonedTitleBkgrnd, .4, {
        delay: .3,
        "margin-top": "16px"
    }), TweenLite.to(clonedGI, .4, {
        delay: .3,
        height: clonedGIHeight,
        onComplete: function() {
            clonedDetails.css({
                display: "block"
            });
        }
    }), TweenLite.to(clonedGI, .4, {
        delay: .7,
        width: clonedGIWidth,
        onComplete: function() {
            clonedGI.css({
                height: "auto",
                width: ""
            });
        }
    }), TweenLite.to(clonedDetails, .4, {
        delay: 1,
        opacity: 1
    })) : (clonedGI.css({
        left: thingyLeft
    }), TweenLite.to(clonedGIMainImg, .4, {
        delay: .3,
        height: "147px",
        opacity: 0
    }), TweenLite.from(clonedTitleCopy, .4, {
        delay: .3,
        height: thingTitleCopyheight
    }), TweenLite.to(clonedTitleBkgrnd, .4, {
        delay: .3,
        "margin-top": "16px"
    }), TweenLite.to(clonedGI, .4, {
        delay: .3,
        height: clonedGIHeight,
        onComplete: function() {
            clonedDetails.css({
                display: "block"
            });
        }
    }), TweenLite.to(clonedGI, .4, {
        delay: .7,
        width: clonedGIWidth,
        onComplete: function() {
            clonedGI.css({
                height: "auto",
                width: ""
            });
        }
    }), TweenLite.to(clonedDetails, .4, {
        delay: 1,
        opacity: 1
    })), gridFocusOuterHeight < clonedGIHeight && TweenLite.to(gridFocusOuter, .4, {
        delay: .3,
        height: clonedGIHeight
    }), thingyHeight + thingyTop > gridFocusOuterHeight - 10 && thingyTop > 10 ? clonedGI.css({
        bottom: 0
    }) : clonedGI.css({
        top: thingyTop
    }), clonedGI.click(function(e) {
        e.stopPropagation();
    }), gridFocusOuter.click(function(e) {
        clearGrid();
    });
    var lessButton = gridFocusOuter.find(".less-button");
    lessButton.click(function(e) {
        e.preventDefault(), clearGrid();
    });
}

function updateLink() {
    var whatLink, whereLink, startLink, endLink, viewLink, newLink = "/whats-on/filter/";
    whatLink = selectedChoicesObj.what.length > 0 ? selectedChoicesObj.what.join("-") : "any", 
    whereLink = selectedChoicesObj.where.length > 0 ? selectedChoicesObj.where.join("-") : "any", 
    startLink = selectedChoicesObj.when[0], endLink = selectedChoicesObj.when[1], null == startLink && (startLink = "any"), 
    null == endLink && (endLink = "any"), viewLink = "any" != startLink && "any" != endLink ? "calendar" : void 0 != selectedChoicesObj.view && selectedChoicesObj.view.length > 0 ? selectedChoicesObj.view : "grid", 
    newLink += "what/" + whatLink + "/where/" + whereLink + "/when/" + startLink + "/" + endLink + "/view/" + viewLink, 
    goButtons.attr("href", newLink);
}

function ShowView(a) {
    var b, c;
    switch (b = a.replace("wo-", "")) {
      case "grid-view":
        c = "grid";
        break;

      case "calendar-view":
        c = "calendar";
    }
    selectedChoicesObj.view = c, updateLink(), $("#" + b).siblings(".view").fadeOut("slow", function() {
        $("#" + b).fadeIn("slow"), $("#" + a).siblings(".view-btn").removeClass("selected"), 
        $("#" + a).addClass("selected"), gimmeGo($("#" + b));
    });
}

function openDayAnimation() {
    var activePerfs, closeDay, allPerfsCloseDay, perfSum = expandedModule.find(".perf-sum"), perfSumHeight = perfSum.height(), myPerfs = expandedModule.find(".ex-mod-inner"), myWeek = expandedModule.parent(".week");
    screenWidth >= mediumScreen ? (activePerfs = myPerfs.clone().appendTo(myWeek).prop("id", "openPerfs").addClass("cloned"), 
    console.log("cloning")) : activePerfs = myPerfs.prop("id", "openPerfs"), expandedModule.addClass("selected"), 
    expandedModule.attr("id", "selectedDay"), closeDay = expandedModule.find(".more-button + .less-button"), 
    allPerfsCloseDay = activePerfs.find(".less-button"), TweenLite.from(perfSum, .2, {
        height: perfSumHeight,
        opacity: 1,
        onComplete: function() {
            perfSum.css({
                opacity: "",
                height: ""
            });
        }
    }), activePerfs.show(), TweenLite.from(activePerfs, 1, {
        height: 0,
        opacity: 0,
        ease: Power2.easeInOut,
        onComplete: function() {
            activePerfs.css({
                height: "",
                opacity: ""
            }), closeDay.click(function(e) {
                dsOffset = expandedModule.offset().top, expandedModule = "undefined", collapseInnerDetails(e);
            }), allPerfsCloseDay.click(function(e) {
                dsOffset = expandedModule.offset().top, expandedModule = "undefined", collapseInnerDetails(e);
            });
        }
    });
}

function closeDayAnimation() {
    var openPerfs = $("#openPerfs"), openDay = (openPerfs.height(), $("#selectedDay")), openPerfSum = $("#selectedDay .perf-sum");
    openPerfs.attr("id", ""), TweenLite.to(openPerfs, .4, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            openPerfs.hide(), screenWidth >= mediumScreen ? openPerfs.remove() : openPerfs.css({
                height: "",
                opacity: ""
            });
        }
    }), openDay.attr("id", "").removeClass("selected"), TweenLite.from(openPerfSum, .4, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            openPerfSum.css({
                opacity: "",
                height: ""
            });
        }
    }), "undefined" != typeof expandedModule && "undefined" != expandedModule || TweenLite.to(theBody, .1, {
        scrollTop: dsOffset - 90
    });
}

!function($, window) {
    function init() {
        var playerEl = document.querySelector(SELECTORS.AUDIOPLAYER), galleryEl = document.querySelector(SELECTORS.AUDIOGALLERY);
        return !!playerEl && (audio.player = playerEl, audio.playlist = $(SELECTORS.PLAYLIST), 
        audio.playlistContainer = $(SELECTORS.PLAYLISTCONTAINER), audio.countPlaybackElement = $(SELECTORS.COUNTPLAYBACKELEMENT), 
        audio.items = audio.playlist.map(function(i, item) {
            return $(item).data("src");
        }), controls.playPauseBtn = document.querySelector(SELECTORS.PLAYBUTTON), controls.prevBtn = document.querySelector(SELECTORS.PREVBUTTON), 
        controls.nextBtn = document.querySelector(SELECTORS.NEXTBUTTON), controls.volume = document.querySelector(SELECTORS.VOLUME), 
        controls.mute = document.querySelector(SELECTORS.MUTE), progressBar.container = $(SELECTORS.PROGRESSBAR), 
        progressBar.track = $(SELECTORS.PROGRESSTRACK), state.totalLength = audio.items.length, 
        1 === state.totalLength && ($(audio.playlistContainer).addClass("single"), $(controls.prevBtn).remove(), 
        $(controls.nextBtn).remove()), audio.player.volume = .5, loadAudio(), resetCountPlayback(), 
        setEventHandlers(), void $(galleryEl).removeClass("hidden"));
    }
    function setEventHandlers() {
        audio.player.addEventListener("canplaythrough", function(e) {
            e.preventDefault(), console.log(e.currentTarget.duration), audio.itemDuration = e.currentTarget.duration, 
            $(".js-countTotalTimeElement").html(formatCountPlayback(audio.itemDuration));
        }), audio.player.addEventListener("ended", function(e) {
            e.preventDefault(), resetCountPlayback(), nextAudio(), state.currentPosition && playPauseAudio();
        }), $(progressBar.container).on("mousedown", function(e) {
            e.preventDefault(), dragging = !0;
        }), $(progressBar.container).on("mousemove", function(e) {
            e.preventDefault(), dragging && (audio.player.currentTime = setTrackPosition(e.offsetX), 
            setTrackProgress());
        }), $(".js-download").each(function(i, val) {
            $(val).on("click", function(e) {
                e.preventDefault();
                var closest = $(val).closest(".js-playlist-item"), data = closest.data("src"), filenameParts = data.split("/");
                SaveToDisk(data, filenameParts[filenameParts.length - 1]);
            });
        }), $(progressBar.container).on("mouseup", function(e) {
            e.preventDefault(), audio.player.currentTime = setTrackPosition(e.offsetX), setTrackProgress(), 
            dragging = !1;
        }), $(controls.playPauseBtn).on("click", function(e) {
            e.preventDefault(), playPauseAudio();
        }), $(controls.prevBtn).on("click", function(e) {
            e.preventDefault(), prevAudio();
        }), $(controls.nextBtn).on("click", function(e) {
            e.preventDefault(), nextAudio();
        }), $(controls.volume).on("input", function(e) {
            e.preventDefault(), state.muted = !1;
            var volume = $(this).val();
            audio.player.volume = volume, state.currentVolume = volume, setVolumeCSSClass();
        }), $(controls.volume).on("change", function(e) {
            e.preventDefault(), setVolumeCSSClass();
        }), $(controls.mute).on("click", function(e) {
            e.preventDefault(), muteAudio();
        }), audio.playlist.each(function(i, element) {
            $(element).on("click", function(e) {
                e.stopPropagation(), state.currentPosition = i, loadAudio();
            });
        });
    }
    function startCountPlayback() {
        countPlaybackInterval = window.setInterval(function() {
            audio.countPlaybackElement.html(formatCountPlayback(audio.player.currentTime)), 
            setTrackProgress();
        }, 10);
    }
    function setVolumeCSSClass() {
        CSS_VOLUME_CLASSES.forEach(function(cssClass) {
            $(controls.mute).removeClass(cssClass);
        }), state.currentVolume <= 0 ? $(controls.mute).addClass(CSS_VOLUME_CLASSES[0]) : state.currentVolume <= .3 ? $(controls.mute).addClass(CSS_VOLUME_CLASSES[1]) : state.currentVolume <= .6 ? $(controls.mute).addClass(CSS_VOLUME_CLASSES[2]) : $(controls.mute).addClass(CSS_VOLUME_CLASSES[3]);
    }
    function SaveToDisk(fileURL, fileName) {
        if (window.ActiveXObject) {
            if (window.ActiveXObject && document.execCommand) {
                var _window = window.open(fileURL, "_blank");
                _window.document.close(), _window.document.execCommand("SaveAs", !0, fileName || fileURL), 
                _window.close();
            }
        } else {
            var save = document.createElement("a");
            save.href = fileURL, save.target = "_blank", save.download = fileName || "unknown";
            var evt = new MouseEvent("click", {
                view: window,
                bubbles: !0,
                cancelable: !1
            });
            save.dispatchEvent(evt), (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }
    }
    function stopCountPlayback() {
        window.clearInterval(countPlaybackInterval);
    }
    function formatCountPlayback(time) {
        var date = new Date(null);
        return date.setSeconds(time), moment.parseZone(date).format("mm:ss");
    }
    function resetCountPlayback() {
        stopCountPlayback(), audio.countPlaybackElement.html(formatCountPlayback(0));
    }
    function nextAudio() {
        var nextPos = ++state.currentPosition;
        nextPos >= state.totalLength ? state.currentPosition = 0 : state.currentPosition = nextPos, 
        loadAudio();
    }
    function prevAudio() {
        if (audio.player.currentTime > 3) return audio.player.currentTime = 0, stopAudio(), 
        audio.countPlaybackElement.html(formatCountPlayback(audio.player.currentTime)), 
        setTrackProgress(), !1;
        var prevPos = --state.currentPosition;
        prevPos < 0 ? state.currentPosition = state.totalLength - 1 : state.currentPosition = prevPos, 
        loadAudio();
    }
    function playPauseAudio() {
        state.playing ? ($(controls.playPauseBtn).removeClass(CSS_CLASSES.PLAYING), audio.player.pause(), 
        stopCountPlayback()) : ($(controls.playPauseBtn).addClass(CSS_CLASSES.PLAYING), 
        audio.player.play(), startCountPlayback()), state.playing = !state.playing;
    }
    function stopAudio() {
        state.playing = !1, audio.player.pause();
    }
    function loadAudio() {
        audio.player.pause(), state.playing = !1, $(controls.playPauseBtn).removeClass(CSS_CLASSES.PLAYING), 
        audio.player.src = audio.items[state.currentPosition], resetCountPlayback(), hightlightCurrentPlaylistItem(), 
        setTrackProgress();
    }
    function muteAudio() {
        state.muted ? audio.player.volume = state.currentVolume : (audio.player.volume = 0, 
        state.currentVolume = 0, $(controls.volume).val(0)), setVolumeCSSClass(), state.muted = !state.muted;
    }
    function hightlightCurrentPlaylistItem() {
        audio.playlist.each(function(i, element) {
            $(element).removeClass(CSS_CLASSES.ACTIVE_ITEM);
        }), $(audio.playlist[state.currentPosition]).addClass(CSS_CLASSES.ACTIVE_ITEM);
    }
    function setTrackProgress() {
        progressBar.track.width(getTrackPercentage() + "%");
    }
    function setTrackPosition(xPos) {
        var percentage = xPos / $(progressBar.container).width() * 100;
        return parseInt(percentage / 100 * audio.itemDuration, 10);
    }
    function getTrackPercentage() {
        var percentage = audio.player.currentTime / audio.itemDuration * 100;
        return percentage;
    }
    var countPlaybackInterval, SELECTORS = {
        AUDIOGALLERY: ".audio-gallery",
        AUDIOPLAYER: ".js-audio-player",
        PLAYLIST: ".js-playlist-item",
        PLAYLISTCONTAINER: ".js-audio-gallery__playlist",
        VOLUME: ".js-volume-bar",
        MUTE: ".js-mute-control",
        PLAYBUTTON: ".js-playPauseAudio",
        PREVBUTTON: ".js-prevAudio",
        NEXTBUTTON: ".js-nextAudio",
        COUNTPLAYBACKELEMENT: ".js-countPlaybackElement",
        PROGRESSBAR: ".js-audio-gallery__progress",
        PROGRESSTRACK: ".js-update-progress",
        DOWNLOAD: ".js-download"
    }, CSS_CLASSES = {
        ACTIVE_ITEM: "is-active",
        PAUSED: "is-paused",
        PLAYING: "is-playing"
    }, CSS_VOLUME_CLASSES = [ "mute", "volume1", "volume2", "volume3" ], audio = {
        player: void 0,
        items: void 0,
        playlist: void 0,
        itemDuration: void 0
    }, controls = {
        playPauseBtn: void 0,
        nextBtn: void 0,
        prevBtn: void 0,
        mute: void 0
    }, state = {
        playing: !1,
        currentPosition: 0,
        totalLength: 0,
        muted: !1,
        currentVolume: .5
    }, progressBar = {
        container: void 0,
        track: void 0,
        trackWidth: 0,
        increment: 0
    };
    if ($(SELECTORS.AUDIOPLAYER).length) return init(), !1;
    var dragging = !1;
}(jQuery, window), $(function() {
    if ($(".cast-profile").length < 1) return !1;
    $(".cast-profile").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        arrows: !0,
        fade: !0,
        touchThreshold: 5,
        adaptiveHeight: !0
    }).on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        $(".cast-thumbs .active-thumb").removeClass("active-thumb");
        var $current = $(".cast-thumbs .slick-current"), $prev = $current.prevAll('[data-thumb="' + nextSlide + '"]').first(), $next = $current.nextAll('[data-thumb="' + nextSlide + '"]').first();
        if ($prev.length > 0 && $next.length > 0) {
            var prevDistance = $current.index() - $prev.index(), nextDistance = $next.index() - $current.index();
            prevDistance < nextDistance ? ($(".cast-thumbs").slick("slickGoTo", $prev.attr("data-slick-index")), 
            $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb")) : ($(".cast-thumbs").slick("slickGoTo", $next.attr("data-slick-index")), 
            $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"));
        } else if ($prev.length > 0) $(".cast-thumbs").slick("slickGoTo", $prev.attr("data-slick-index")), 
        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"); else if ($next.length > 0) $(".cast-thumbs").slick("slickGoTo", $next.attr("data-slick-index")), 
        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"); else {
            var $thumbs = $('.cast-thumbs [data-thumb="' + nextSlide + '"]').closest(".slick-slide"), shortestDistance = null;
            $thumbs.each(function() {
                var distance = Math.abs($current.index() - $(this).index());
                (null == shortestDistance || distance < shortestDistance) && (shortestDistance = distance, 
                shortestIndex = $(this).attr("data-slick-index"));
            }), $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"), 
            $(".cast-thumbs").slick("slickGoTo", shortestIndex);
        }
    }).on("afterChange", function(event, slick, currentSlide) {
        $img = $(this).find(".slick-active figure img"), $img.length > 0 ? $(".cast-profile .slick-prev, .cast-profile .slick-next").css("top", $img.first().height() / 2) : $(".cast-profile .slick-prev, .cast-profile .slick-next").css("top", $(this).find(".slick-active").height() / 2);
    }), $(".carousel-prev").click(function() {
        $(".cast-profile").slick("slickPrev");
    }), $(".carousel-next").click(function() {
        $(".cast-profile").slick("slickNext");
    }), $(".close-cast-profile").click(function() {
        $(".cast-profile-container").slideUp(500, function() {
            $(this).css("display", "block").css("position", "absolute");
        }), $(".cast-thumb.active-thumb").removeClass("active-thumb");
    });
    var i = 0;
    $(".cast-thumbs").children().each(function() {
        $(this).attr("data-thumb", i), i++;
    }), $(".cast-thumbs").on("click", ".cast-thumb", function(e) {
        var thumb = $(this).data("thumb");
        $(".active-thumb").removeClass("active-thumb"), $('[data-thumb="' + thumb + '"]').addClass("active-thumb"), 
        $(".cast-profile").slick("slickGoTo", thumb), $(".cast-profile").resize(), $(".cast-profile .slick-list").height($(".cast-profile .slick-current").height()), 
        $container = $(".cast-profile-container"), "absolute" == $container.css("position") && $container.css("position", "relative").css("display", "none"), 
        $container.slideDown(400, function() {
            $(".slick-current").css("z-index", "0");
        }), $(".cast-creative-widget").focus(), $("body, html").scrollTop(Math.floor($(".cast-creative-widget").offset().top - $(".header-container .header-top").height()));
    }), $(".cast-thumbs").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        focusOnSelect: !0,
        touchThreshold: 50,
        speed: 750,
        rows: $(window).width() >= 1440 ? 2 : 1,
        mobileFirst: !0,
        responsive: [ {
            breakpoint: 0,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1440,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        } ]
    }), $(".creative-carousel").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: !1,
        focusOnSelect: !0,
        touchThreshold: 50,
        speed: 750,
        mobileFirst: !0,
        responsive: [ {
            breakpoint: 0,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 790,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1440,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1
            }
        } ]
    }), hashChange = function() {
        var hash = location.hash.substr(1);
        "" !== hash && ($slide = $('[data-hash="' + hash + '"]'), $slide.length > 0 && $slide.first().trigger("click"));
    }, hashChange(), window.onhashchange = function() {
        hashChange();
    }, $(".slick-slide div:last-child .cast-thumb").addClass("odd-thumb");
}), $(function() {
    $(".gallery-widget").length > 0 && $(".gallery-widget").each(function(j) {
        $this = $(this).attr("id", "galleryWidget" + j);
        var theThumbsID = "galleryThumbs" + j, thePhotosID = "galleryPhotos" + j;
        $this.find(".gallery-thumbs").attr("id", theThumbsID), $this.find(".gallery-photos").attr("id", thePhotosID), 
        $this.find(".gallery-thumbs-container").attr("id", theThumbsID + "cont"), $this.find(".gallery-photo-container").attr("id", thePhotosID + "cont"), 
        gallerySetup($this, theThumbsID, thePhotosID);
    });
}), $(document).ready(function() {
    function addEventHandlers() {
        $(".js-magnific").on("click", function(e) {
            e.preventDefault();
            var content = $(this).closest(".js-magnific-content").html();
            $.magnificPopup.open({
                mainClass: "mfp-wrap mfp-wrap-case",
                items: {
                    src: '<div class="case-book-popout-panel">' + content + "</div>",
                    type: "inline"
                }
            });
        }), $(".js-magnific-video").on("click", function(e) {
            e.preventDefault();
            var title = $(this).data("title"), src = $(this).attr("href");
            $.magnificPopup.open({
                type: "iframe",
                items: {
                    src: src
                },
                iframe: {
                    markup: '<div class="mfp-iframe-scaler"><h1 class="popup-title">' + title + '</h1><div class="mfp-close"></div><iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe></div>',
                    mainClass: "mfp-fade",
                    removalDelay: 160,
                    preloader: !1,
                    fixedContentPos: !1
                }
            });
        });
    }
    var caseFilePage = $("article.case-book");
    caseFilePage.length > 0 && addEventHandlers(), $(".case-book .media-gallery .dk-gray").length && $("div.gallery-launcher").removeClass("dk-gray");
}), function($, window, undefined) {
    function run() {
        if (!b_crumbs.find("li:first a").attr("href").length) return !1;
        var firstBreadCrumbLink = b_crumbs.find("li:first a").attr("href").replace(/\//g, ""), links = $.map($("#globalNav .top-level > li > a"), function(link) {
            var href, el;
            if ($(link).attr("href") && (href = $(link).attr("href").replace(/\//g, ""), el = $(link), 
            "#" === href)) {
                var subNavLink = $(link).next(".subnav-item"), link = subNavLink.find(".main-link a");
                link.length > 0 && (href = link.attr("href").replace(/\//g, ""));
            }
            return {
                href: href,
                el: el
            };
        }), foundLink = links.reduce(function(arr, link) {
            return link.href === firstBreadCrumbLink && arr.push(link), arr;
        }, []);
        foundLink.length > 0 && foundLink[0].el.parent().addClass("selected");
    }
    var b_crumbs = $(".breadcrumbs .bc");
    b_crumbs.length > 0 && run();
}(jQuery, window), function(window, $, undefined) {
    function init() {
        pswpElement = document.querySelectorAll(".pswp")[0], $elems.each(function(i, elem) {
            $(elem).closest(".cast-creative-gallery-widget").length > 0 && $(elem).find(".launch-all").remove(), 
            $(this).attr({
                "data-pswp-uid": i + 1
            }), galleries.push({
                uid: "data-pswp-uid__" + (i + 1),
                gallery: this,
                config: parseThumbnailElements(this),
                title: $(this).find(".gallery-title").html()
            });
        }), addEventHandlers(), hashData = photoswipeParseHash(), hashData.pid && hashData.gid && initializeGallery(galleries[hashData.gid - 1].gallery, parseInt(hashData.pid));
    }
    function photoswipeParseHash() {
        var hash = window.location.hash.substring(1), params = {};
        if (hash.length < 5) return params;
        for (var vars = hash.split("&"), i = 0; i < vars.length; i++) if (vars[i]) {
            var pair = vars[i].split("=");
            pair.length < 2 || (params[pair[0]] = pair[1]);
        }
        return params.gid && (params.gid = parseInt(params.gid, 10)), params;
    }
    function initializeGallery(galleryElement, idx) {
        var index = idx || 0, galleryTitle = $(galleryElement).find(".gallery-title").html(), config = parseThumbnailElements(galleryElement), showCaptionEl = config.showCaptionEl, items = config.items, options = {
            galleryUID: galleryElement.getAttribute("data-pswp-uid"),
            galleryTitle: galleryTitle,
            history: !0,
            galleryPIDs: !0,
            closeOnVerticalDrag: !1,
            captionEl: showCaptionEl,
            index: index,
            closeOnScroll: !1,
            showAnimationDuration: 40,
            timeToIdle: 0,
            getThumbBoundsFn: function(index) {
                if (0 === items.length || !items[index].el) return {
                    x: 0,
                    y: 0,
                    w: 0
                };
                var thumbnail = $(items[index].el).closest(".js-gallery-item")[0], pageYScroll = window.pageYOffset || document.documentElement.scrollTop, rect = thumbnail.getBoundingClientRect();
                return {
                    x: rect.left,
                    y: rect.top + pageYScroll,
                    w: rect.width
                };
            }
        };
        launchGallery(items, options);
    }
    function addEventHandlers() {
        galleries.forEach(function(galleyObj) {
            if (galleyObj.config.createGallery) {
                var slides = $(galleyObj.gallery).find(".js-gallery-item"), launchButton = $(galleyObj.gallery).find(".launch-all");
                $(slides).each(function(i) {
                    $(this).on("click", function(e) {
                        e.preventDefault(), initializeGallery(galleyObj.gallery, i);
                    });
                }), $(launchButton).on("click", function(e) {
                    e.preventDefault(), initializeGallery(galleyObj.gallery, 0);
                });
            }
        });
    }
    function launchGallery(items, options) {
        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_RSC, items, options), gallery.init(), 
        gallery.listen("initialZoomInEnd", addExtraContent), gallery.listen("afterChange", addExtraContent), 
        gallery.listen("close", function() {
            $(".pswp__video-container").find("iframe").attr("src", "");
        });
    }
    function overrideMobile() {
        gallery.framework.features.touch && ($(".header-container").css({
            display: "none"
        }), $(".main-container").css({
            display: "none"
        }), $(".footer-container").css({
            display: "none"
        })), window.setTimeout(function() {
            $(".mfp-ready").css({
                top: 0
            }), $("body").addClass("mfp-open");
            var height = $(".mfp-content").outerHeight(), windowHeight = $(window).height();
            windowHeight > height && (height = windowHeight), $(".mfp-bg").css({
                height: height + "px"
            });
        }, 10);
    }
    function resetMobile() {
        $(".header-container").css({
            display: "block"
        }), $(".main-container").css({
            display: "block"
        }), $(".footer-container").css({
            display: "block"
        }), $("body").removeClass("mfp-open"), gallery.framework.features.touch && gallery.updateSize(!0);
    }
    function addExtraContent() {
        var captionCenter = $("#captionCenter"), mediaHolder = $("body"), extraInfoButton = $(gallery.template).find(".js-extrainfo-button");
        captionCenter.height() > 150 ? (mediaHolder.addClass("overflow"), extraInfoButton.removeClass("hidden"), 
        extraInfoButton.off("click").on("click", function(e) {
            e.preventDefault(), e.stopPropagation(), $.magnificPopup.open({
                items: {
                    src: '<div class="gallery__extra-popup">' + gallery.currItem.title + "</div>",
                    type: "inline"
                },
                alignTop: !0,
                callbacks: {
                    open: function() {
                        overrideMobile();
                    },
                    close: function() {
                        resetMobile();
                    },
                    resize: function() {
                        overrideMobile();
                    }
                }
            });
        })) : (mediaHolder.removeClass("overflow"), mediaHolder.css({
            height: "auto"
        }), extraInfoButton.addClass("hidden"));
    }
    function parseThumbnailElements(galleryElement) {
        var slides = $(galleryElement).find(".js-gallery-item"), items = [], showCaptionEl = !0, numberOfSlides = slides.length;
        return $(slides).each(function(i) {
            $(this).attr({
                "data-pswp-uid": i
            });
            var item, title, link, size, src, data, alt;
            if (link = $(this).find("a"), size = $(link).data("size"), size && (size = size.split("x")), 
            src = $(link).attr("href"), title = $(this).find(".gallery__description").html(), 
            alt = $(link).data("alt"), src) {
                var bgImage = $(this).find(".gallery-image__container").css("background-image");
                bgImage = bgImage ? bgImage.replace(/.*\s?url\([\'\"]?/, "").replace(/[\'\"]?\).*/, "") : undefined, 
                item = {
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10),
                    title: title,
                    msrc: bgImage
                }, 0 === src.indexOf("https://www.youtube.com/") ? (data = src.split("/watch?v=")[1], 
                item.isVideo = !0, item.youTubeID = data.split("&cc_load_policy=")[0], item.captionID = data.split("&cc_load_policy=")[1]) : 0 === src.indexOf("https://vimeo.com/") ? (item.isVideo = !0, 
                item.vimeoID = src.split("vimeo.com/")[1]) : item.src = src, item.alt = alt;
            }
            var extraInfo = $(this).find(".gallery-cast-profile__extrainfo");
            if (extraInfo.length > 0 && (item.extraInfo = extraInfo.html()), $(this).find(".js-cast-bio").length) {
                var wrapped = $(this).wrapInner('<div class="cast-profile-wrapper"></div>'), htmlString = $(wrapped).clone().wrap("<div>").parent().html();
                showCaptionEl = !1, item = {
                    html: htmlString
                };
            }
            item && (item.el = $(this)[0], item.pid = i, items.push(item));
        }), {
            items: items,
            showCaptionEl: showCaptionEl,
            createGallery: items.length === numberOfSlides
        };
    }
    var pswpElement, SELECTOR = ".js-gallery", $elems = $(SELECTOR), canrun = !!$elems.length, galleries = [], hashData = {};
    canrun && init(), document.getElementById("titleHeader") && $(".gallery-title").remove();
}(window, jQuery);

var theBody = $("body"), thePage = $("body, html"), mediaHolder = $("#mediaHolder"), largeVideoHolder = $(".large-video-holder"), largeIframe = $(".large-video-holder iframe"), smlVideoHolder = $(".small-video-holder"), smlIframe = $(".small-video-holder iframe"), homeVideoHolder = $("#homeVideoHolder"), homeVidIframe = $("#homeVideoHolder iframe"), homeVideoCover = $("#videoCover img"), quickLinksBtn = $(".ql-head"), quicklinksCont = $(".ql-body"), smallScreen = 480, smadiumScreen = 600, mediumScreen = 790, largeScreen = 1024, xlargeScreen = 1440, screenWidth = window.innerWidth, inCinemas = $("#inCinemasHolder"), showHidden = $(".show-hidden"), conditForms = [], introCopy = $("#playIntroCopy p"), prBooking = $("#prBooking"), copyInner = $("#copyInner"), moveIntro, movePRBooking, quoteHolder = $(".quote-holder:last-of-type"), floatNavOn = $("#floatNavOnBtn"), floatNavOff = $("#floatNavOffBtn"), floatNav = $("#floatNavList"), floatSubNav = $("#floatingMenu .sub-nav"), nextPerf = $("#nextPerf"), videoCover = $("#videoCover"), playVideo = $("#playVideo"), mainImage = $(".play-hub .main-image"), sponLogo = $("#spHolder"), moveSponsor, plFullList = $(".performance-list-item"), plChecks = $(".pl-filter .k-checkbox"), plActiveFilters = {
    where: [ "stratford", "london" ],
    time: [ "matinee", "evening" ],
    when: []
}, plFilterGroups = $(".pl-filter:not(.pl-calendar):not(.pl-promocode)"), perfList = $("#perfList"), plWarning = $("#pl-warning"), plLoader = $(".loader-outer"), apHeaders = $(".pl-list h2"), theWOHeader = $(".whatson header"), theWOHeaderOrigHeight = theWOHeader.height(), woBtns = $(".wo-q-item"), woOpenTransport = $(".wo-transport-lrg .more-button"), woCloseTransport = $(".wo-transport-lrg .less-button"), woSearchOpen = $("#woSearchOpen"), woSearchClose = woSearchOpen.find(".less-button"), woSearchForm = woSearchOpen.find(".form-holder"), woSearchSubmit = $("#woSearchBtn"), woSearchBar = $("#woSearchBar"), questionHolder = $(".wo-questions"), woHeader = $(".wo-header"), woQCloseBtns = $(".wo-q-item .less-button"), woLeft = $(".wo-left"), woRight = $(".wo-right"), woActiveQ, selects = $(".selection input"), ieCalDropdown = $(".cal-ie-fallback #ie-month-select"), goButtons = $(".go-button"), resetButtons = $(".reset-filters"), selectedChoices = [], selectedChoicesObj = {
    what: [],
    where: [],
    when: [ "any", "any" ],
    view: "grid"
}, woShowingDefault = [ "Plays, Events, Activities&hellip;", "Stratford-upon-Avon, London, On Tour&hellip;", "This Week, Next Week, Next Month&hellip;" ], woActiveItem, gridItems = $(".wo-grid-item"), gridFocusOuter, rightNow = moment().format("LL"), yesterday = moment().subtract(1, "days").format("LL"), nextWeek = moment().add(7, "days").format("LL"), lastDay = moment().add(4, "months").format("LL"), startDateSel, endDateSel, dsOffset, woChangeSearch = $("#woChangeSearch"), breakpoint = {}, globalHeader = $("#globalHeader"), globalNav = $("#globalNav"), globalHeadertop = $(".header-top"), loginSearch = $(".login-search"), topLevelLogin = $("#MobileTopLevelLogin .login-link"), theMenuButton = $("#menuButton"), theBurger = $("#burgerHolder"), mainNav = $("nav"), mainNavLis = $(".top-level li"), mainNavBtns = $(".top-level li a.top-level-link:not(.nodrop)"), subNavItems = $(".subnav-item"), subNavBkgrnd = $(".subnav-bkgrnd"), subNavClose = $("#subNavCloseBtn"), didScroll, lastScrollTop = 0, delta = 5, headerHeight = globalNav.outerHeight() + globalHeadertop.outerHeight(), fullHeaderHeight = headerHeight + $(".play-hub header").outerHeight() - 40, gFooterOffset = $(".footer-container").offset(), gFooterTop = gFooterOffset.top, windowHeight = window.innerHeight, floatingMenu = $("#floatingMenu"), globalSearch = $(".global-search-box"), globalSearchOpen = $(".search-open"), globalSearchClose = $(".global-search-box .close-btn"), globalSearchInput = $("#globalSearchInput"), globalSearchBtn = $("#globalSearchButton"), shopSearch = $(".shop-header input[type=text]"), shopSearchGo = $(".shop-header #shopSearch"), infoPopUp = $(".info-popup"), infoClose = $(".info-popup .close-btn"), sendMessage = $("#send-message"), contactFormHolder = $("#contact-form-holder"), galleryIframe = $(".gallery-photo-container iframe"), searchAgain = $(".search-again"), resourceForm = $(".resource-form"), resourcePlay = $(".resource-form #resPlaySelect"), resourceType = $(".resource-form #resType input[type=checkbox]"), resourceAge = $(".resource-form #resAge input[type=checkbox]"), resBankFilters = {
    play: [],
    type: [],
    age: []
}, allExMods = $(".ex-mod"), expandedModule, horzScollers = $(".hs-outer"), hsTransport = $(".hs-transport"), horzScollerInners = [];

$(function() {
    function hasScrolled() {
        var st = $(this).scrollTop();
        Math.abs(lastScrollTop - st) <= delta || (window.innerWidth >= largeScreen && (st > lastScrollTop && st > headerHeight - 20 && !theMenuButton.hasClass("mb-active") ? (globalNav.addClass("nav-up").removeClass("nav-down"), 
        theMenuButton.addClass("mb-active")) : st < lastScrollTop && st < headerHeight + 100 ? (globalNav.removeClass("nav-up").addClass("nav-down"), 
        theMenuButton.removeClass("mb-active"), theBurger.removeClass("active")) : st < lastScrollTop && lastScrollTop - st ? (globalNav.removeClass("nav-up").addClass("nav-down"), 
        theBurger.addClass("active")) : st > headerHeight && theBurger.hasClass("active") && (globalNav.addClass("nav-up").removeClass("nav-down"), 
        theBurger.removeClass("active"))), st > fullHeaderHeight && st + windowHeight < gFooterTop && !floatingMenu.hasClass("active") ? floatingMenu.addClass("active") : (st < fullHeaderHeight || st + windowHeight > gFooterTop) && floatingMenu.hasClass("active") && (floatingMenu.removeClass("active"), 
        floatNav.removeClass("active"), floatNavOn.removeClass("open"), floatNavOff.addClass("closed"), 
        nextPerf.removeClass("active")), lastScrollTop = st);
    }
    function homeVidEnd(event) {
        0 == event.data && ($("#homeVideoHolder #videoCover").fadeIn(200), $(".hero-copy .intro-btn").css({
            display: "inline-block"
        }), $(".hero-copy").fadeIn(200), playVideo.fadeIn(200));
    }
    if ($(window).resize(function() {
        screenWidth = window.innerWidth, screenWidth < largeScreen ? (headerHeight = globalHeadertop.outerHeight(), 
        theBurger.hasClass("active") && (theBurger.removeClass("active"), mainNavBtns.removeClass("vis"), 
        globalNav.removeClass("nav-active").addClass("nav-hide"), subNavBkgrnd.removeClass("vis"), 
        subNavItems.removeClass("vis"), theBody.removeClass("noscroll")), $(".timer-outer").removeClass("timer-hidden")) : (headerHeight = globalNav.outerHeight() + globalHeadertop.outerHeight(), 
        theBurger.hasClass("active")), videoResize(largeVideoHolder, smlVideoHolder), "undefined" != typeof introCopy && introCopy.length > 0 && ("undefined" != typeof prBooking && prBooking.length > 0 ? (window.innerWidth >= mediumScreen && window.innerWidth < xlargeScreen && $("#moveIntro").length < 1 ? (moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop("id", "moveIntro").prependTo(copyInner), 
        introCopy.css({
            display: "none"
        }), introCopy.parent(".play-intro-copy").css({
            position: "",
            bottom: ""
        })) : window.innerWidth < mediumScreen && $("#moveIntro").length > 0 ? (moveIntro.remove(), 
        introCopy.css({
            display: ""
        }), introCopy.parent(".play-intro-copy").css({
            position: "",
            bottom: ""
        })) : window.innerWidth >= xlargeScreen && $("#moveIntro").length > 0 ? (moveIntro.remove(), 
        introCopy.css({
            display: ""
        }), introCopy.parent(".play-intro-copy").css({
            position: "absolute",
            bottom: "32px"
        })) : window.innerWidth >= xlargeScreen && introCopy.parent(".play-intro-copy").css({
            position: "absolute",
            bottom: "32px"
        }), window.innerWidth >= mediumScreen && window.innerWidth < largeScreen && $("#movePRBooking").length < 1 ? movePRBooking = prBooking.clone().removeClass("ongray-intro").addClass("intro-copy").prop("id", "movePRBooking").prependTo(copyInner) : !(window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#movePRBooking").length > 0 && movePRBooking.remove()) : window.innerWidth >= mediumScreen && window.innerWidth < largeScreen && $("#moveIntro").length < 1 ? moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop("id", "moveIntro").prependTo(copyInner) : !(window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#moveIntro").length > 0 && moveIntro.remove()), 
        "undefined" != typeof mainImage && mainImage.length > 0 && "undefined" != typeof sponLogo && sponLogo.length > 0 && (window.innerWidth < mediumScreen && sponLogo.hasClass("first-level") && $(".move-sponsor").length < 1 ? moveSponsor = sponLogo.clone().addClass("move-sponsor").insertAfter(mainImage) : window.innerWidth >= mediumScreen && $(".move-sponsor").length > 0 && moveSponsor.remove());
        var playHubHeaderHeight = $(".play-hub header").outerHeight();
        if (windowHeight = window.innerHeight, gFooterOffset = $(".footer-container").offset(), 
        gFooterTop = gFooterOffset.top, fullHeaderHeight = headerHeight + playHubHeaderHeight - 150, 
        "undefined" != typeof woActiveItem && woActiveItem.length > 0) {
            var woActivePos = woActiveItem.position(), woActiveWidth = woActiveItem.width(), woActiveHeight = woActiveItem.height(), focusItem = gridFocusOuter.children(".wo-grid-item").css({
                "min-height": woActiveHeight
            }), gridFocusOuterHeight = gridFocusOuter.height();
            screenWidth >= smadiumScreen && screenWidth < largeScreen && woActivePos.left > 0 ? focusItem.css({
                left: 0
            }) : screenWidth >= largeScreen && woActivePos.left > gridFocusOuter.width() / 2 ? focusItem.css({
                left: woActiveWidth
            }) : focusItem.css({
                left: woActivePos.left
            }), woActiveHeight + woActivePos.top > gridFocusOuterHeight - 10 ? focusItem.css({
                bottom: 0
            }) : focusItem.css({
                top: woActivePos.top
            });
        }
        if ("undefined" != typeof questionHolder && questionHolder.length > 0 && window.innerWidth >= mediumScreen && (questionHolder.find(".filters").attr("style", ""), 
        woBtns.each(function(i) {
            $(this).removeClass("active"), $(this).find(".wo-showing").html(woShowingDefault[i]);
        })), "undefined" != typeof perfList && perfList.length > 0 && perfList.simplebar("recalculate"), 
        "undefined" != typeof $("#openPerfs") && $("#openPerfs").length > 0) {
            var openPerfs = $("#openPerfs"), openWeek = openPerfs.parents(".week"), openDayPerfs = ($(".day.selected"), 
            $(".day.selected .all-perfs"));
            if (screenWidth < mediumScreen && openPerfs.hasClass("cloned")) openPerfs.remove(), 
            openDayPerfs.show().prop("id", "openPerfs"); else if (screenWidth >= mediumScreen && !openPerfs.hasClass("cloned")) {
                openPerfs.clone().appendTo(openWeek).addClass("cloned").css({
                    height: ""
                });
                openPerfs.hide().css({
                    height: "",
                    opacity: ""
                }).prop("id", "");
            }
        }
    }).resize(), Modernizr.addTest("mix-blend-mode", function() {
        return Modernizr.testProp("mixBlendMode");
    }), navigator.userAgent.indexOf("Safari") != -1 && navigator.userAgent.indexOf("Chrome") == -1 && $(".bkgrnd-strip.yellow-color").addClass("safari"), 
    quickLinksBtn.click(function(e) {
        screenWidth < mediumScreen && !quickLinksBtn.hasClass("opened") && showIt(e);
    }), showHidden.each(function() {
        var $thisHid = $(this);
        $thisHid.click(function(e) {
            e.preventDefault(), showIt(e);
        });
    }), theMenuButton.click(function(e) {
        theBurger.toggleClass("active"), subNavItems.removeClass("vis"), window.innerWidth < largeScreen ? (mainNav.toggleClass("nav-active").toggleClass("nav-hide"), 
        theBody.toggleClass("noscroll")) : window.innerWidth >= largeScreen && (lastScrollTop > headerHeight - 20 ? globalNav.toggleClass("nav-up").toggleClass("nav-down") : theMenuButton.toggleClass("mb-active"), 
        subNavBkgrnd.hasClass("vis") && (subNavBkgrnd.removeClass("vis").addClass("vis-fade"), 
        setTimeout(function() {
            subNavBkgrnd.removeClass("vis-fade"), theBody.removeClass("noscroll");
        }, 150))), window.innerWidth >= mediumScreen && $(".timer-outer").hasClass("timer-hidden") ? ($(".timer-outer").removeClass("timer-hidden"), 
        TweenLite.from($(".timer-outer"), .2, {
            opacity: 0,
            onComplete: function() {
                $(".timer-outer").css({
                    opacity: ""
                });
            }
        })) : window.innerWidth >= mediumScreen && window.innerWidth < largeScreen && !$(".timer-outer").hasClass("timer-hidden") && TweenLite.to($(".timer-outer"), .3, {
            opacity: 0,
            onComplete: function() {
                $(".timer-outer").css({
                    opacity: ""
                }).addClass("timer-hidden");
            }
        });
    }), navBtns(mainNavBtns, subNavItems), $(window).scroll(function(event) {
        didScroll = !0;
    }), setInterval(function() {
        didScroll && (hasScrolled(), didScroll = !1);
    }, 250), void 0 !== typeof infoPopUp && infoPopUp.length > 0) {
        var infoPHeight = [ 8 ];
        infoPopUp.each(function(i) {
            var $this = $(this);
            if (!$this.hasClass("here")) {
                var theTime = 800 + 350 * i, newHeight = infoPHeight.reduce(arrayAdd, 0) + 8;
                setTimeout(function() {
                    $this.css({
                        visibility: "visible"
                    }).addClass("here"), TweenLite.to($this, .4, {
                        bottom: newHeight,
                        opacity: 1,
                        ease: Circ.easeOut
                    });
                }, theTime), infoPHeight.push($this.height() + 8);
            }
        });
    }
    if (infoClose.each(function(i) {
        var $this = $(this);
        $this.click(function() {
            closeInfo(i);
        });
    }), $(".info-copy .more-button").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.siblings(".hide").removeClass("hide"), $this.hide();
    }), globalSearchOpen.each(function(i) {
        $this = $(this), $this.click(function(e) {
            e.preventDefault(), globalSearchShow();
        });
    }), globalSearchClose.click(function(e) {
        globalSearchHide();
    }), globalSearchInput.change(function() {
        var str = globalSearchInput.val();
        str = str.replace(/\s+/g, " ").toLowerCase(), globalSearchBtn.prop("href", "/search?q=" + str);
    }), globalSearchBtn.click(function(e) {
        e.preventDefault;
        var str = globalSearchInput.val();
        str = str.replace(/\s+/g, " ").toLowerCase(), globalSearchBtn.prop("href", "/search?q=" + str), 
        window.location.href = "/search?q=" + str;
    }), globalSearchInput.on("keypress", function(e) {
        if (13 == e.keyCode) {
            var str = globalSearchInput.val();
            return str = str.replace(/\s+/g, " ").toLowerCase(), globalSearchBtn.prop("href", "/search?q=" + str), 
            window.location.href = "/search?q=" + str, !1;
        }
    }), shopSearch.change(function() {
        var str = shopSearch.val();
        str = str.replace(/\s/g, " ").toLowerCase(), shopSearchGo.prop("href", "/shop/search?q=" + str);
    }), shopSearchGo.click(function(e) {
        e.preventDefault;
        var str = shopSearch.val();
        str = str.replace(/\s+/g, " ").toLowerCase(), shopSearchGo.prop("href", "/shop/search?q=" + str), 
        window.location.href = "/shop/search?q=" + str;
    }), shopSearch.on("keypress", function(e) {
        if (13 == e.keyCode) {
            var str = shopSearch.val();
            return str = str.replace(/\s+/g, " ").toLowerCase(), shopSearchGo.prop("href", "/shop/search?q=" + str), 
            window.location.href = "/shop/search?q=" + str, !1;
        }
    }), $("#membDate").length > 0 && void 0 != typeof pikadayResponsive) {
        var todayPlusSeven = new Date();
        todayPlusSeven.setDate(todayPlusSeven.getDate() + 7);
        new pikadayResponsive(document.getElementById("membDate"), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "member-start-date",
            pikadayOptions: {
                minDate: todayPlusSeven
            }
        });
        (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) && $("#membDate").change(function() {
            var theActualDate = $("#membDate").attr("value");
            moment(theActualDate, "ll").isBefore(todayPlusSeven) ? ($(".date-validator").fadeIn(200), 
            $("#membDate").siblings("input[type=text]").addClass("error")) : ($(".date-validator").hide(), 
            $("#membDate").siblings("input[type=text]").removeClass("error"));
        });
    }
    if ($(".dobField").length > 0 && void 0 != typeof pikadayResponsive) {
        var dobSet, dobDefault, dobStart = new Date(), dobEnd = new Date(), dobTenYears = new Date();
        dobStart.setYear(dobStart.getYear() - 100), dobTenYears.setYear(dobTenYears.getFullYear() - 10), 
        dobEnd.setYear(dobEnd.getFullYear() - 5), "" !== $(".dobField").prop("value") ? (dobSet = new Date($(".dobField").attr("value")), 
        dobDefault = !0) : (dobSet = dobEnd, dobDefault = !1);
        new pikadayResponsive($(".dobField"), {
            format: "ll",
            outputFormat: "YYYY-MM-DD",
            classes: "member-dob",
            pikadayOptions: {
                minDate: dobStart,
                maxDate: dobEnd,
                defaultDate: dobSet,
                setDefaultDate: dobDefault,
                yearRange: 100
            }
        });
        (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) && $(".dobField").change(function() {
            var theActualDate = moment($(".dobField").prop("value"), "YYYY-MM-DD");
            theActualDate.isBefore(dobStart) ? ($(".form-error").hide(), $(".hundred-years").fadeIn(200), 
            $(".dobField").siblings("input[type=text]").addClass("error")) : theActualDate.isAfter(dobEnd) ? ($(".form-error").hide(), 
            $(".future").fadeIn(200), $(".dobField").siblings("input[type=text]").addClass("error")) : theActualDate.isAfter(dobTenYears) ? ($(".form-error").hide(), 
            $(".ten-years").fadeIn(200), $(".dobField").siblings("input[type=text]").addClass("error")) : ($(".form-error").hide(), 
            $(".dobField").siblings("input[type=text]").removeClass("error"));
        });
    }
    if (searchAgain.each(function(i) {
        $(this).click(function() {
            var theForm = $(this).siblings(".form-holder");
            theForm.show(), TweenLite.from(theForm, .4, {
                height: 0,
                opacity: 0
            }), TweenLite.to($(this), .2, {
                height: 0,
                opacity: 0,
                "padding-top": 0,
                "padding-bottom": 0
            });
        });
    }), resBankFilters.play = resourcePlay.val(), resourcePlay.change(function() {
        resBankFilters.play = $(this).val(), edResourceLink();
    }), resourceType.each(function() {
        var resTypeCopy = $(this).attr("id"), resTypeArr = resBankFilters.type;
        ("checked" == $(this).attr("checked") || $(this).is(":checked")) && resTypeArr.push(resTypeCopy), 
        edResourceLink(), $(this).change(function() {
            $.inArray(resTypeCopy, resTypeArr) > -1 ? resTypeArr.splice($.inArray(resTypeCopy, resTypeArr), 1) : resTypeArr.push(resTypeCopy), 
            edResourceLink();
        });
    }), resourceAge.each(function() {
        var resAgeCopy = $(this).attr("id"), resAgeArr = resBankFilters.age;
        ("checked" == $(this).attr("checked") || $(this).is(":checked")) && resAgeArr.push(resAgeCopy), 
        edResourceLink(), $(this).change(function() {
            $.inArray(resAgeCopy, resAgeArr) > -1 ? resAgeArr.splice($.inArray(resAgeCopy, resAgeArr), 1) : resAgeArr.push(resAgeCopy), 
            edResourceLink();
        });
    }), void 0 != typeof sendMessage && sendMessage.click(function(e) {
        e.preventDefault, contactFormHolder.show(), TweenLite.from(contactFormHolder, .4, {
            height: 0,
            opacity: 0
        }), TweenLite.to(sendMessage, .2, {
            height: 0,
            opacity: 0
        });
    }), woChangeSearch.click(function(e) {
        e.preventDefault(), gimmeGo(theWOHeader);
    }), woOpenTransport.click(function(e) {
        questionHolder.addClass("active");
    }), woCloseTransport.click(function(e) {
        questionHolder.removeClass("active");
    }), $("#woStart").length > 0 && void 0 !== typeof pikadayResponsive) {
        var startSetDate, endSetDate, finalDate = new Date(), SDisSet = !1, EDisSet = !1, woStartAttr = $("#woStart").attr("data-value"), woEndAttr = $("#woEnd").attr("data-value");
        finalDate.setDate(finalDate.getDate() + 540), "undefined" != typeof woStartAttr && woStartAttr !== !1 ? (startSetDate = new Date($("#woStart").attr("data-value")), 
        SDisSet = !0, selectedChoicesObj.when[0] = moment($("#woStart").attr("data-value")).format("YYYY-MM-DD"), 
        goButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), resetButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), updateLink()) : startSetDate = new Date(), "undefined" != typeof woEndAttr && woEndAttr !== !1 ? (endSetDate = new Date($("#woEnd").attr("data-value")), 
        EDisSet = !0, selectedChoicesObj.when[1] = moment($("#woEnd").attr("data-value")).format("YYYY-MM-DD"), 
        goButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), resetButtons.each(function(i) {
            $(this).removeClass("inactive");
        })) : (endSetDate = startSetDate, endSetDate.setDate(endSetDate.getMonth() + 1));
        var startDate, endDate, firstRun = 0;
        if ($(".filters .calendar .text-fields").is(":visible")) {
            var updateStartDate = function() {
                startPicker.pikaday.setStartRange(startDate), endPicker.pikaday.setStartRange(startDate), 
                endPicker.pikaday.setMinDate(startDate), goButtons.each(function(i) {
                    $(this).removeClass("inactive");
                }), resetButtons.each(function(i) {
                    $(this).removeClass("inactive");
                });
            }, updateEndDate = function() {
                startPicker.pikaday.setEndRange(endDate), startPicker.pikaday.setMaxDate(endDate), 
                endPicker.pikaday.setEndRange(endDate), goButtons.each(function(i) {
                    $(this).removeClass("inactive");
                }), resetButtons.each(function(i) {
                    $(this).removeClass("inactive");
                });
            }, startPicker = new pikadayResponsive(document.getElementById("woStart"), {
                format: "D MMM YY",
                outputFormat: "ll",
                classes: "woStartDate",
                pikadayOptions: {
                    minDate: new Date(),
                    maxDate: finalDate,
                    defaultDate: startSetDate,
                    setDefaultDate: SDisSet,
                    onSelect: function() {
                        startDate = this.getDate(), firstRun > 0 && updateStartDate(), startDateSel = moment(startDate).format("d MMM YY"), 
                        selectedChoicesObj.when[0] = moment(startDate).format("YYYY-MM-DD"), SDisSet && (goButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        }), resetButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        })), updateLink();
                    }
                }
            }), endPicker = new pikadayResponsive(document.getElementById("woEnd"), {
                format: "D MMM YY",
                outputFormat: "ll",
                classes: "woEndDate",
                pikadayOptions: {
                    minDate: new Date(),
                    maxDate: finalDate,
                    defaultDate: endSetDate,
                    setDefaultDate: EDisSet,
                    onSelect: function() {
                        endDate = this.getDate(), firstRun > 0 && updateEndDate(), endDateSel = moment(endDate).format("d MMM YY"), 
                        selectedChoicesObj.when[1] = moment(endDate).format("YYYY-MM-DD"), EDisSet && (goButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        }), resetButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        })), updateLink();
                    }
                }
            });
            if (firstRun += 1, startPicker.pikaday) var _startDate = startPicker.pikaday.getDate(), _endDate = endPicker.pikaday.getDate();
        }
    }
    if (_startDate && (startDate = _startDate, updateStartDate()), _endDate && (endDate = _endDate, 
    updateEndDate()), woBtns.each(function(i) {
        $(this).click(function() {
            !$(this).hasClass("active") && screenWidth < mediumScreen ? openQuestion(i) : !questionHolder.hasClass("active") && screenWidth >= mediumScreen && questionHolder.addClass("active");
        });
    }), woSearchOpen.click(function(e) {
        e.stopPropagation();
        var theSearch = $(this), theBox = theSearch.find(".form-holder");
        theSearch.hasClass("active") || (closeQuestion(), theSearch.addClass("active"), 
        TweenLite.to(theBox, .4, {
            height: "auto",
            "margin-top": "16px",
            "margin-bottom": "16px",
            opacity: 1,
            onComplete: function() {
                window.innerWidth < mediumScreen && $("html, body").animate({
                    scrollTop: theSearch.offset().top - 90
                }, 500);
            }
        }), TweenLite.to(theWOHeader, .4, {
            height: "auto"
        }));
    }), woSearchClose.click(function(e) {
        e.stopPropagation(), closeQuestion();
    }), woSearchBar.change(function() {
        var str = woSearchBar.val();
        str = str.replace(/\s+/g, "-").toLowerCase(), woSearchSubmit.prop("href", "/whats-on/search/" + str);
    }), woSearchSubmit.click(function(e) {
        e.preventDefault;
        var str = woSearchBar.val();
        str = str.replace(/\s+/g, "-").toLowerCase(), woSearchSubmit.prop("href", "/whats-on/search/" + str), 
        window.location.href = "/whats-on/search/" + str;
    }), woSearchBar.on("keypress", function(e) {
        if (13 == e.keyCode) {
            var str = woSearchBar.val();
            return str = str.replace(/\s+/g, "-").toLowerCase(), woSearchSubmit.prop("href", "/whats-on/search/" + str), 
            window.location.href = "/whats-on/search/" + str, !1;
        }
    }), woLeft.each(function(i) {
        --i, i < 0 && (i = 2), $(this).click(function(e) {
            e.stopPropagation(), openQuestion(i);
        });
    }), woRight.each(function(i) {
        ++i, i > 2 && (i = 0), $(this).click(function(e) {
            e.stopPropagation(), openQuestion(i);
        });
    }), woQCloseBtns.each(function(i) {
        $(woBtns[i]);
        $(this).click(function(e) {
            e.stopPropagation(), closeQuestion();
        });
    }), selects.each(function(i) {
        var theSelect = $(this), theSelectCopy = $(this).attr("id"), theQuestion = theSelect.parents(".wo-q-item").attr("class").replace("wo-q-item wo-", "").replace(" active", ""), theQuestionArr = selectedChoicesObj[theQuestion];
        ("checked" == theSelect.attr("checked") || theSelect.is(":checked")) && (goButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), resetButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), theSelect.addClass("selected"), theQuestionArr.push(theSelectCopy), updateLink()), 
        theSelect.click(function(e) {
            e.stopPropagation(), theSelect.hasClass("selected") ? (theSelect.removeClass("selected"), 
            $.inArray(theSelectCopy, theQuestionArr) > -1 && theQuestionArr.splice($.inArray(theSelectCopy, theQuestionArr), 1)) : (theSelect.addClass("selected"), 
            theQuestionArr.push(theSelectCopy)), $(".category .selected").length > 0 && goButtons.hasClass("inactive") ? (goButtons.each(function(i) {
                $(this).removeClass("inactive");
            }), resetButtons.each(function(i) {
                $(this).removeClass("inactive");
            })) : $(".category .selected").length < 1 && (goButtons.each(function(i) {
                $(this).addClass("inactive");
            }), resetButtons.each(function(i) {
                $(this).addClass("inactive");
            })), updateLink();
        });
    }), ieCalDropdown.is(":visible")) {
        if (null != ieCalDropdown.val()) {
            var rightNow = moment(), dropStartDate = moment([ moment().year() ]).month(ieCalDropdown.val());
            dropStartDate.isBefore(rightNow) && dropStartDate.year(moment().year() + 1);
            var dropEndDate = moment(dropStartDate).endOf("month");
            selectedChoicesObj.when[0] = moment(dropStartDate).format("YYYY-MM-DD"), selectedChoicesObj.when[1] = moment(dropEndDate).format("YYYY-MM-DD"), 
            updateLink();
        }
        ieCalDropdown.change(function() {
            var rightNow = moment(), dropStartDate = moment([ moment().year() ]).month(ieCalDropdown.val());
            dropStartDate.isBefore(rightNow) && dropStartDate.year(moment().year() + 1);
            var dropEndDate = moment(dropStartDate).endOf("month");
            selectedChoicesObj.when[0] = moment(dropStartDate).format("YYYY-MM-DD"), selectedChoicesObj.when[1] = moment(dropEndDate).format("YYYY-MM-DD"), 
            updateLink();
        });
    }
    resetButtons.each(function(i) {
        $(this).click(function(e) {
            if (!$(this).hasClass("inactive")) {
                $(".category .selected").removeClass("selected").prop("checked", !1), resetButtons.addClass("inactive"), 
                goButtons.addClass("inactive"), woBtns.each(function(i) {
                    $(this).find(".wo-showing").html(woShowingDefault[i]).removeClass("chosen");
                }), startPicker.pikaday.setDate(null), endPicker.pikaday.setDate(null);
                for (var k in selectedChoicesObj) selectedChoicesObj[k] = [];
            }
        });
    }), gridItems.each(function(i) {
        var $this = $(this);
        $this.click({
            theGridItem: $this
        }, clickTheGrid);
    }), allExMods.each(function(i) {
        var $this = $(this);
        $this.off("click", expandInnerDetails).on("click", {
            theMod: $this
        }, expandInnerDetails);
    }), horzScollers.each(function(i) {
        var $this = $(this), hsItem = $this.find(".hs-item").first(), goLeft = $(this).siblings(".hs-transport").find(".hs-left"), goRight = $(this).siblings(".hs-transport").find(".hs-right");
        goLeft.click({
            thePosition: i,
            theFC: hsItem,
            theDirection: "left"
        }, hsScrollMe), goRight.click({
            thePosition: i,
            theFC: hsItem,
            theDirection: "right"
        }, hsScrollMe);
    }), floatNavOn.click(function(e) {
        e.preventDefault(), e.stopPropagation(), floatNav.toggleClass("active"), floatNavOn.toggleClass("open"), 
        floatNavOff.toggleClass("closed"), floatSubNav.toggleClass("open"), nextPerf.toggleClass("active");
    }), floatNavOff.click(function(e) {
        e.preventDefault(), e.stopPropagation(), floatNav.toggleClass("active"), floatNavOn.toggleClass("open"), 
        floatNavOff.toggleClass("closed"), floatSubNav.toggleClass("open"), nextPerf.toggleClass("active");
    }), $(document).on("click", function(event) {
        !$(event.target).closest("#floatingMenu").length && floatSubNav.hasClass("open") && (floatNav.toggleClass("active"), 
        floatNavOn.toggleClass("open"), floatNavOff.toggleClass("closed"), floatSubNav.toggleClass("open"), 
        nextPerf.toggleClass("active"));
    }), playVideo.each(function(i) {
        var $this = $(this);
        $this.click(function(e) {
            $this.fadeOut(150), $(videoCover[i]).fadeOut(150), $(".hero-copy").fadeOut(150);
            var theID = $this.parents(".video-holder").attr("id");
            if (isTouchDevice() === !0) {
                if (callPlayer(theID, function() {}), null !== getFrameID("homeVideoHolder")) {
                    var player;
                    YT_ready(function() {
                        var frameID = getFrameID("homeVideoHolder");
                        frameID && (player = new YT.Player(frameID, {
                            events: {
                                onStateChange: homeVidEnd
                            }
                        }));
                    });
                }
            } else if (callPlayer(theID, function() {
                callPlayer(theID, "playVideo");
            }), callPlayer(theID, "playVideo"), null !== getFrameID("homeVideoHolder")) {
                var player;
                YT_ready(function() {
                    console.log("YT_ready");
                    var frameID = getFrameID("homeVideoHolder");
                    frameID && (player = new YT.Player(frameID, {
                        events: {
                            onStateChange: homeVidEnd
                        }
                    }));
                });
            }
        });
    });
    var unsorted_times = new Array();
    if (plFullList.each(function(i) {
        var thePerfDate = $(this).find(".date").html(), moment_date = moment(thePerfDate), unsorted_time = new Object();
        unsorted_time.time = thePerfDate, unsorted_time.milli = moment_date.valueOf(), unsorted_times.push(unsorted_time), 
        $(this).attr("data-when", moment(thePerfDate, "ddd DD MMM YYYY").format("LL"));
    }), unsorted_times.length > 0) {
        var perfDateSorted = unsorted_times.sort(compareMilli), firstPerfDate = perfDateSorted[0].milli;
        plActiveFilters.when[0] = moment(firstPerfDate).format("YYYY-MM-DD");
        var lastPerfPos = perfDateSorted.length - 1, lastPerfDate = perfDateSorted[lastPerfPos].milli;
        plActiveFilters.when[1] = moment(lastPerfDate).format("YYYY-MM-DD");
    }
    if ($(".pl-calendar").is(":visible") && $("#plStart").length > 0 && void 0 != typeof pikadayResponsive) {
        var sdDefault, edDefault, startDateSet = !1, endDateSet = !1;
        getQueryVariable("plStart") !== !1 && (startDateSet = !0, sdDefault = moment(getQueryVariable("plStart"), "YYYY-MM-DD").toDate()), 
        getQueryVariable("plEnd") !== !1 && (endDateSet = !0, edDefault = moment(getQueryVariable("plEnd"), "YYYY-MM-DD").toDate());
        var startPLDate, endPLDate, updatePLStartDate = function() {
            startPLPicker.pikaday.setStartRange(startPLDate), endPLPicker.pikaday.setStartRange(startPLDate), 
            endPLPicker.pikaday.setMinDate(startPLDate);
        }, updatePLEndDate = function() {
            startPLPicker.pikaday.setEndRange(endPLDate), startPLPicker.pikaday.setMaxDate(endPLDate), 
            endPLPicker.pikaday.setEndRange(endPLDate);
        }, startPLPicker = new pikadayResponsive(document.getElementById("plStart"), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "plStartDate",
            pikadayOptions: {
                minDate: new Date(),
                maxDate: new Date(moment(lastPerfDate, "ddd DD MMM YYYY").format("YYYY-MM-DD")),
                defaultDate: sdDefault,
                setDefaultDate: startDateSet,
                onSelect: function() {
                    startPLDate = this.getDate(), updatePLStartDate(), plActiveFilters.when[0] = moment(startPLDate).format("YYYY-MM-DD"), 
                    perfFilter({
                        startDate: plActiveFilters.when[0]
                    });
                }
            }
        }), endPLPicker = new pikadayResponsive(document.getElementById("plEnd"), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "plEndDate",
            pikadayOptions: {
                minDate: new Date(),
                maxDate: new Date(moment(lastPerfDate, "ddd DD MMM YYYY").format("YYYY-MM-DD")),
                defaultDate: edDefault,
                setDefaultDate: endDateSet,
                onSelect: function() {
                    endPLDate = this.getDate(), updatePLEndDate(), plActiveFilters.when[1] = moment(endPLDate).format("YYYY-MM-DD"), 
                    perfFilter({
                        endDate: plActiveFilters.when[1]
                    });
                }
            }
        });
        if (startPLPicker.pikaday) var _startPLDate = startPLPicker.pikaday.getDate(), _endPLDate = endPLPicker.pikaday.getDate();
    }
    if (_startPLDate && (startPLDate = _startPLDate, updatePLStartDate()), _endPLDate && (endPLDate = _endPLDate, 
    updatePLEndDate()), plChecks.each(function() {
        var theCheck = $(this);
        theCheck.click(function() {
            perfFilter({
                checkBox: theCheck
            });
        });
    }), perfList.simplebar(), "undefined" != typeof plFilterGroups && plFilterGroups.length > 0) {
        var checkChangeList = [];
        plFilterGroups.each(function(i) {
            var $this = $(this), filterID = this.id, preFilter = getQueryVariable(filterID);
            if (preFilter !== !1) {
                var filterChecks = $this.find(".k-checkbox");
                filterChecks.each(function() {
                    var $newThis = $(this), theCategory = $newThis.attr("name"), theBox = $newThis.prop("id");
                    $newThis.removeAttr("checked"), plActiveFilters[theCategory].splice($.inArray(theBox, plActiveFilters[theCategory]), 1), 
                    theBox == preFilter && checkChangeList.push($newThis);
                });
            }
        });
        for (var i = 0; i < checkChangeList.length; i++) {
            var $this = checkChangeList[i], theCategory = $this.attr("name"), theBox = $this.prop("id");
            getQueryVariable("plStart") !== !1 || getQueryVariable("plEnd") !== !1 ? (plActiveFilters[theCategory].push(theBox), 
            $this.prop("checked", "checked")) : i == checkChangeList.length - 1 ? $this.trigger("click") : (plActiveFilters[theCategory].push(theBox), 
            $this.prop("checked", "checked"));
        }
        getQueryVariable("plStart") !== !1 && getQueryVariable("plEnd") !== !1 ? (plActiveFilters.when[1] = moment(getQueryVariable("plEnd")).format("YYYY-MM-DD"), 
        startPLPicker.setDate(moment(getQueryVariable("plStart"), "YYYY-MM-DD"))) : getQueryVariable("plStart") !== !1 && 0 == getQueryVariable("plEnd") ? startPLPicker.setDate(moment(getQueryVariable("plStart"), "YYYY-MM-DD")) : 0 == getQueryVariable("plStart") && getQueryVariable("plEnd") !== !1 && endPLPicker.setDate(moment(getQueryVariable("plEnd"), "YYYY-MM-DD"));
    }
    $("#socialSharing").sharrre({
        share: {
            facebook: !0,
            twitter: !0,
            pinterest: !0,
            linkedin: !0
        }
    }), $(".conditional-form").each(function(i) {
        var $this = $(this), $startItem = $this.find(".condit-start"), $conditName = $this.prop("id"), $startBtn = $startItem.find(".condit-btn"), currentNum = i;
        conditForms[currentNum] = {}, conditForms[currentNum].name = $conditName, $startBtn.data({
            parent: $conditName,
            position: 0
        }), $this.data({
            name: $conditName,
            num: i,
            position: 0,
            questions: [ "start" ]
        }), $startBtn.click(function(e) {
            cfOpenNext(e);
        });
    });
}), breakpoint.refreshValue = function() {
    this.value = window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"/g, "");
};

var YT_ready = function() {
    var onReady_funcs = [], api_isReady = !1;
    return function(func, b_before) {
        if (func === !0) for (api_isReady = !0; onReady_funcs.length; ) onReady_funcs.shift()(); else "function" == typeof func && (api_isReady ? func() : onReady_funcs[b_before ? "unshift" : "push"](func));
    };
}();

!function() {
    var s = document.createElement("script");
    s.src = ("https:" == location.protocol ? "https" : "http") + "://www.youtube.com/player_api";
    var before = document.getElementsByTagName("script")[0];
    before.parentNode.insertBefore(s, before);
}();

var jQuery = jQuery || void 0;

$.fn.scrollTo = function(target, options, callback) {
    "function" == typeof options && 2 == arguments.length && (callback = options, options = target);
    var settings = $.extend({
        scrollTarget: target,
        offsetTop: 50,
        duration: 500,
        easing: "swing"
    }, options);
    return this.each(function() {
        var scrollPane = $(this), scrollTarget = "number" == typeof settings.scrollTarget ? settings.scrollTarget : $(settings.scrollTarget), scrollY = "number" == typeof scrollTarget ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
        scrollPane.animate({
            scrollTop: scrollY
        }, parseInt(settings.duration), settings.easing, function() {
            "function" == typeof callback && callback.call(this);
        });
    });
};