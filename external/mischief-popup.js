function MischiefPopup($){
  var this_js_script = $('script[src*=mischief-popup]');
  var parts = $(this_js_script).attr('src').split('?');

  var qs_config = {
    debug: false
  };

  if(parts.length > 1) {
    var groups = parts[1].split('&');
    qs_config = groups.reduce(function(acc, part){
        var paramParts = part.split('=');
        if(paramParts[1]) {
          acc[paramParts[0]] = paramParts[1];
        }
        return acc;
    },{});
  };

  if ($ === undefined) {
    return false;
  }

  var mischiefWidget;
  var overlay;
  var mischiefWidgetHolder;
  var timeouts = [];
  var config = {
    interval: 100, // time to wait after page load until we begin animation
    counter: 0,
    gapInterval: 2 // how many days to wait until we play again
  };

  init();

  function init() {
    if (storageAvailable('localStorage')) {
      run();
    }
  }

  function run(){

    var markup = buildTemplate();

    mischiefWidget = $(markup);
    // background image

    mischiefWidgetHolder = mischiefWidget.find('.mischief-widget-holder');
    mischiefWidgetHolder.css({
      background: 'url(img/mischief-background_crop.png) no-repeat 50% 50%',
      height: '100%',
      width: '100%',
      position: 'absolute',
      display: 'block',
      top: 0,
      left: 0
    });

    overlay = mischiefWidget.find('.overlay');
    overlay.css({
      background: 'url(img/mischief-overlay_crop.png) no-repeat 50% 50%',
      height: '100%',
      width: '100%',
      position: 'absolute',
      display: 'block',
      top: 0,
      left: 0
    });


    $('body').prepend(mischiefWidget);

    var lastVisit = getLocalStorage('mis_run_date');
    if (lastVisit) {
      var diff = new Date().getTime() - new Date(lastVisit).getTime();

      var seconds = (qs_config.debug === 'true') ? 100 : config.gapInterval * 86400000;
      if (diff > seconds) {
        localStorage.removeItem('mis_run');
        localStorage.removeItem('mis_run_date');
      }
    }


    if (!getLocalStorage('mis_run')) {
      timeouts.push(
        window.setInterval(function(){
          config.counter++;
          if (config.counter > 2) {
            removeTimeouts();
            setHasRun();
            animate();
          }
        }, 1000)
      );
    }
  }

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function startAnimation(){
    var flickerTime = 50;
    var timer = 0;

    var intervalId = setInterval(function(){
      // var val = getRandomInt(1, 50);
      // var pause = getRandomInt(50, 100);

      var valMin = 1;
      var valMax = 100;
      var pauseMin = 300;
      var pauseMax = 400;

      var val = getRandomInt(parseInt(valMin, 10), parseInt(valMax, 10));
      var pause = getRandomInt(parseInt(pauseMin, 10), parseInt(pauseMax, 10));
      if (val < 25) {
        overlay.css({'display':'block'});
        setTimeout(function(){
          overlay.css({'display':'none'});
        }, pause)
      }

      timer += 1;
      if (timer > 300) {
        mischiefWidget.removeClass('loaded').addClass('swingimage');
      }


    }, flickerTime);
  }





  function buildTemplate(){
    return [
      '<div class="mischief-widget mischief-widget--popup">',
      '<a href="http://example.com">',
      '<div class="mischief-widget-holder">',
      '<div class="mischief-widget__inner">',
      '<div class="overlay"></div>',
      '</div>',
      '</div>',
      '</a>',
      '<div class="mischief-widget__close-btn"><span class="mischief-widget__close-icon">X</span></div>',
      '</div>'
    ].join('');
  }

  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  function animate(){
    mischiefWidget.css({'display':'block'});
    window.setTimeout(function(){
      mischiefWidget.css({
        'right': $('body').width() / 2 - (mischiefWidget.width()/2),
        'top': $('body').scrollTop() + mischiefWidget.height()/2,
        'z-index' : 5000
      });
      mischiefWidget.addClass('loaded');
      startAnimation();
      $(document).on('scroll', debounceScroll);
    }, config.interval);

    $('.mischief-widget__close-btn').on('click', function(){
      mischiefWidget.remove();
    });
  }

  var debounceScroll = debounce(function() {
      mischiefWidget.css({'top': $('body').scrollTop() + (mischiefWidget.height()/2) + 'px'});
  }, 10);

  function storageAvailable(type) {
    try {
      var storage = window[type],
      x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    }
    catch(e) {
      return false;
    }
  }

  function getLocalStorage(key) {
    var data = localStorage.getItem(key);
    if (data) {
      return data;
    }
    return false;
  }

  function setLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }

  function removeTimeouts(){
    timeouts.forEach(function(timeout){
      window.clearTimeout(timeout);
    })
    timeouts = [];
  }

  function setHasRun() {
    var today = new Date();
    setLocalStorage('mis_run', 1);
    setLocalStorage('mis_run_date', today.toISOString());
  }

  return {
    init: init
  }


};

var jQuery = jQuery || undefined;

if (jQuery !== undefined ){
  var mcp = new MischiefPopup(jQuery);
}
