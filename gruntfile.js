module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.initConfig({
    connect: {
      server : {
        options: {
          port: 8000,
          hostname: '*',
          open: true,
          livereload: true,
          protocol: 'http'
        }
      }
    },
    sass: {
      options: {
        sourceMap: false,
        outputStyle: 'expanded',
        sourceComments: true,
        precision: 4
      },
      dist: {
        files: {
          'css/styles.css': 'sass/styles.scss',
          'css/ie.css': 'sass/ie.scss',
          'css/shop.css': 'sass/shop.scss',
          'css/shop-ie.css': 'sass/shop-ie.scss'
        }
      }
    }, // sass
    cssmin: {
      options: {
        shorthandCompacting: true,
        roundingPrecision: -1,
        report: 'min',
        level: 2,
      },
      target: {
        files: {
          'css/styles.min.css': ['css/styles.css'],
          'css/ie.min.css': ['css/ie.css'],
          'css/shop.min.css': ['css/shop.css'],
          'css/shop-ie.min.css': ['css/shop-ie.css']
        }
      }
    },
    uglify: {
      script_expanded: {
        options: {
          beautify: true,
          mangle: false
        }, // options
        files: {
          'js/script.js': ['js/comps/*.js']
        } // files
      }, // script.js expanded
      script_mind: {
        options: {
          compress: {
            drop_console: true
          }
        }, // options
        files: {
          'js/script.min.js': ['js/comps/*.js']
        } // files
      }, // script.js min'd
      script_min_individual: {
        options: {
          compress: {
            drop_console: true
          }
        }, // options
        files: {
          'js/vendor/photoswipe.min.js': ['js/vendor/photoswipe.js'],
          'js/vendor/photoswipe-ui-rsc.min.js': ['js/vendor/photoswipe-ui-rsc.js']
        } // files
      }, // script.js min'd
      shop_mind: {
        options: {
          compress: {
            drop_console: true
          }
        }, // options
        files: {
          'js/syos.min.js': ['js/syos.js']
        } // files
      } // shop min'd
    }, // uglify
    watch: {
      options: {
        livereload: true
      }, // options
      scripts: {
        files: ['js/comps/*.js', 'js/syos.js'],
        tasks: ['uglify']
      }, // script
      css: {
        files: ['sass/*/*.scss', 'sass/*.scss'],
        tasks: ['sass']
      },
      html: {
        files: ['*.html']
      }
    } // watch
  }); // initConfig

  grunt.registerTask('default', ['connect','watch']);
  grunt.registerTask('build', ['sass', 'uglify', 'cssmin']);
} // exports
